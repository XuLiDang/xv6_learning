### 1. 向常规文件写入数据
- 应用程序首先会`write`系统调用，随后系统便会调用`sys_write`函数进行处理，该函数首先根据传入的文件描述符获取对应的file结构体以及获取传入的写入数据大小和缓冲区虚拟地址。随后将获取的内容作为参数，调用`filewrite(file *f, uint64 addr, int n)`函数。`filewrite`函数的代码如下：

```
// Write to file f.addr is a user virtual address.
int filewrite(struct file *f, uint64 addr, int n)
{
  int r, ret = 0;
  // 文件结构体不可写则返回-1
  if(f->writable == 0)
    return -1;

  // 若文件结构体type属性的值为FD_INODE
  else if(f->type == FD_INODE)
  {
    // write a few blocks at a time to avoid exceeding
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * BSIZE;
    int i = 0;
    // 写入数据
    while(i < n)
    { 
      // n1表示每次实际写入的数据量
      int n1 = n - i;
      if(n1 > max)
        n1 = max;

      // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
      begin_op();
      // 申请睡眠锁，防止多个线程同时访问同一个inode
      ilock(f->ip);
      // 从 addr + i 地址开始，将其存放的n1个字节的数据写入到f->ip的f->off偏移处
      // 参数 1 是指 addr + i 代表的是用户空间的虚拟地址
      if ((r = writei(f->ip, 1, addr + i, f->off, n1)) > 0)
        f->off += r;
      // 释放睡眠锁
      iunlock(f->ip);
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();

      // 所返回的写入文件的数据量与n1不相等则代表发生了错误
      if(r != n1){
        // error from writei
        break;
      }
      // 更新已写入的数据量
      i += r;
    }
    // 返回成功写入的数据量，若i == n 则代表成功写入n个字节的数据
    // 两者不相等则代表发生了错误, 因此返回-1
    ret = (i == n ? n : -1);
  }
  // 文件结构体type属性不属于上述任何一个则直接报错 
  else 
  {
    panic("filewrite");
  }

  return ret;
}
```
可以看到对于类型为`FD_INODE`的file结构体，filewrite函数会循环调用`writei`函数写入数据，那么接下来我们继续看一看`writei`函数的实现代码：

```
// Write data to inode.
// Caller must hold ip->lock.
// If user_src==1, then src is a user virtual address;
// otherwise, src is a kernel address.
// Returns the number of bytes successfully written.
// If the return value is less than the requested n,
// there was an error of some kind.
int writei(struct inode *ip, int user_src, uint64 src, uint off, uint n)
{
  uint tot, m;
  // 指向buf结构体的指针
  struct buf *bp;
  // 确保文件偏移量小于文件的大小以及写入量大于0
  if(off > ip->size || off + n < off)
    return -1;
  // 确保文件偏移量+要写入的字节数小于MAXFILE*BSIZE
  if(off + n > MAXFILE*BSIZE)
    return -1;

  // 循环遍历文件的每一个块, tot代表每次writei函数所写入的数据量
  // m则代表每次循环写入到buffer数据区的数据量, tot = (n - 1) * m 
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
    // 从磁盘中读取数据块,并存放在buffer中
    // bmap根据inode以及inode中addres数组的索引值找到对应的数据块号
    // 若数组索引对应的数据块号不存在，则需要先分配一个数据块，并将其
    // 块号放入数组中。
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
     // m代表每次循环写入到buffer数据区的数据量 
    m = min(n - tot, BSIZE - off%BSIZE);
    // 将m个字节的数据写入到buffer数据区中
    if(either_copyin(bp->data + (off % BSIZE), user_src, src, m) == -1) {
      brelse(bp);
      break;
    }
    // 在日志中记录对磁盘数据块的修改(实际是对buffer的修改)
    log_write(bp);
    brelse(bp);
  }

  if(off > ip->size)
    ip->size = off;

  // write the i-node back to disk even if the size didn't change
  // because the loop above might have called bmap() and added a new
  // block to ip->addrs[].
  iupdate(ip);

  // 返回此次writei函数所写入的数据量
  return tot;
}
```
`writei`函数同样也是会通过循环向磁盘块对应的buffer写入数据。在每次循环中，其首先会调用`bread`函数获取磁盘块对应的buffer，并且调用`either_copyin`函数将数据写入buffer中，并在每次循环的末尾使用日志来记录队buffer的修改，最后返回成功写入的字节数。

抛开代码的实现细节，常规文件的写入流程如下：
- 应用程序调用`write`函数，并传入文件描述符，用户写缓冲区首地址以及写入字节数一共三个参数，随后系统会根据文件描述符找到对应的file结构体，并通过file结构体找到对应的inode结构体。

- 随后根据file结构体的off(文件偏移量)成员以及写入的数据量计算出inode结构体中addrs数组的索引值，该数组用于记录存放文件数据的数据块号。因此我们就能获得要写入的数据块的块号，然后再根据数据块号以及inode结构体的dev成员获取数据块对应buffer，如果没有对应的buffer则重新分配一个，并将数据块的内容复制到新创建的buffer中。

- 最后将用户缓冲区中的数据写入到数据块对应的buffer中，并且每次写入结束后都要在日志区域中记录所修改的buffer。当事务结束时，则将所修改的buffer的内容写入到日志区域的logged blocks中，最后再将logged blocks中的数据写到对应的数据块。

### 2. 从常规文件中读取数据
- 应用程序首先会`read`系统调用，随后系统便会调用`sys_read`函数进行处理，该函数首先根据传入的文件描述符获取对应的file结构体以及获取传入的读取数据大小和缓冲区虚拟地址。随后将获取的内容作为参数，调用`fileread(file *f, uint64 addr, int n)`函数。`fileread`函数的代码如下：

```
// Read from file f. addr is a user virtual address.
int fileread(struct file *f, uint64 addr, int n)
{
  int r = 0;
  // 文件结构体f不可读则返回-1
  if(f->readable == 0)
    return -1;
    
  ......
  ......
  ......
  
  // 若文件结构体type属性的值为FD_INODE
  else if(f->type == FD_INODE){
    // 锁定文件结构体中的inode
    ilock(f->ip);
    // 读取n个字节的数据到addr中
    if((r = readi(f->ip, 1, addr, f->off, n)) > 0)
      f->off += r;
    // 释放文件结构体中的inode
    iunlock(f->ip);
  } 
  
  // 文件结构体type属性不属于上述任何一个则直接报错
  else {
    panic("fileread");
  }

  return r;
}

```
可以看到对于类型为`FD_INODE`的file结构体，fileread函数会循环调用`readi`函数读取数据，那么接下来我们继续看一看`readi`函数的实现代码：

```
// Read data from inode.
// Caller must hold ip->lock.
// If user_dst==1, then dst is a user virtual address;
// otherwise, dst is a kernel address.
int readi(struct inode *ip, int user_dst, uint64 dst, uint off, uint n)
{
  uint tot, m;
  struct buf *bp;
  // 确保文件偏移量小于文件的大小
  if(off > ip->size || off + n < off)
    return 0;
  // 若文件偏移量+要读取的字节数大于文件的大小
  // 则将要读取修改为 ip->size - off
  if(off + n > ip->size)
    n = ip->size - off;

  // 循环遍历文件的每一个块,tot代表每次readi函数已复制到用户缓冲区的字节数
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    // 从磁盘中读取数据块,并存放在buffer中
    // bmap根据inode以及inode中addres数组的索引值找到对应的数据块号
    // 若数组索引对应的数据块号不存在，则需要先分配一个数据块，并将其
    // 块号放入数组中。
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    // m代表每次循环复制到用户缓冲区的数据量
    m = min(n - tot, BSIZE - off%BSIZE);
    // 将m个字节的数据从buffer复制到用户缓冲区
    if(either_copyout(user_dst, dst, bp->data + (off % BSIZE), m) == -1) {
      brelse(bp);
      tot = -1;
      break;
    }
    brelse(bp);
  }
  return tot;
}
```
`readi`会通过循环从磁盘块对应的buffer读取数据。在每次循环中，其首先会调用`bread`函数获取磁盘块对应的buffer，并且调用`either_copyout`函数从buffer中读取数据。注意readi函数不需要开启事务，因此其只是从磁盘块中读取数据，而并不需要修改磁盘块的内容。

抛开代码的实现细节，常规文件的写入流程如下：
- 应用程序调用`read`函数，并传入文件描述符，用户写缓冲区首地址以及写入字节数一共三个参数，随后系统会根据文件描述符找到对应的file结构体，并通过file结构体找到对应的inode结构体。

- 随后根据file结构体的off(文件偏移量)成员计算出inode结构体中addrs数组的索引值，该数组用于记录存放文件数据的数据块号。因此我们就能获得要读取的数据块的块号，然后再根据数据块号以及inode结构体的dev成员获取数据块对应buffer，如果没有对应的buffer则重新分配一个，并将数据块的内容复制到新创建的buffer中。

- 最后将buffer中的数据复制到用户空间缓冲区，复制过程结束后便返回成功读取的字节数。