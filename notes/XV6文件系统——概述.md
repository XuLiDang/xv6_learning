### 1. 文件系统整体布局
文件系统中必须要有一个方案，该方案能够确定将索引节点和内容块存储在磁盘上哪些位置。为此，xv6的文件系统将磁盘划分为几个部分，如图8.2所示。
![image](https://gitlab.com/XuLiDang/xv6_learning/-/raw/master/lab_docs/file_system_overview.png)
- 块0是**引导扇区**，里面存放的是操作系统启动时需要写入内存中的数据(如何内核源代码等)。只有引导分区才会向块0写入数据，文件系统不适用块0.
- 块1称为**超级块**，它包含有关文件系统的元数据，如文件系统大小（以块为单位）、数据块数、索引节点数和日志区域中的块数。
- 从块2开始的块便是**日志区域**。日志区域之后便是索引结点区域，该区域用于存放索引结点结构体。
- 随后是**位图块**，一个块便能表示512*8个比特位，每个比特位都用来标记对应的数据块是否空闲
- 最后就是**数据块**，其用来保存保存文件或目录的内容。最后需要注意的是：mkfs程序可用于构建并初始化文件系统，其主要向磁盘中的块1写入超级块的内容，而超级块又保存着有关文件系统的元数据，因此便会采用对应文件系统的格式来管理磁盘中的块。

### 2. Buffer cache
Buffer cache的作用主要有两个：
1. 同步对磁盘块的访问，以确保磁盘块在内存中只有一个副本，并且一次只有一个内核线程使用该副本
2. 缓存常用的磁盘块，从而不需要每次都需要从磁盘中读取它们。

Buffer cache由多个buffer构成，而每个buffer均由下面buf结构体(定义在`buf.h`文件中)来表示：

```
// buffer结构体
struct buf {
  int valid;   // 是否已从磁盘中读取数据
  int disk;    // does disk "own" buf?
  uint dev;    // buffer对应的磁盘块的设备号
  uint blockno; // buffer对应的磁盘块的号码
  struct sleeplock lock; // 用于确保同一时间只有一个线程使用该缓冲区
  uint refcnt; // 引用计数
  struct buf *prev; // LRU cache list
  struct buf *next;
  uchar data[BSIZE]; // buffer数据区域，用于存放磁盘块的数据
};
```
除此之外，XV6还定义了一个bcache结构体(定义在`bio.c`文件中)来统一管理所有buffer：

```
// buffer缓冲区管理结构体
struct {
  // 自旋锁，确保同一时刻只能有一个线程修改buffer。实际上，这个自旋锁的粒度过大
  // 会导致当一个线程修改buffer1的内容，另一个线程无法修改buffer2内容的情况发生
  // 因此，后面有关lock的实验中就需要实现更加细粒度的锁
  struct spinlock lock;
  // buf结构体数组
  struct buf buf[NBUF];

  // Linked list of all buffers, through prev/next.
  // Sorted by how recently the buffer was used.
  // head.next is most recent, head.prev is least.
  // buf结构体链表头结点
  struct buf head;
} bcache;
```
系统在启动时，会通过调用`binit`函数对buffer进行初始化：

```
// 初始化buffer缓冲区
void binit(void)
{
  struct buf *b;
  // spinklock *lk = &bcache;
  // lk->name = bcache; lk->locked = 0; lk->cpu = 0
  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  // bcache.head是双向链表的头结点
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  // buffer结构体已用数组的方式组织起来了
  // 现在就是要用链表的形式组织buffer结构体，并采用头插法的形式创建链表
  for(b = bcache.buf; b < bcache.buf + NBUF; b++)
  {
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
```
可以看到，该函数主要的内容是：采用链表的形式将数组中的每个buf结构体链接起来。并且这还是一个LRU链表，头结点next指针指向的buf结构体是访问频率最高的节点，而头结点prev指针指向的则是访问频率最低的节点。

### 3. 索引节点
术语inode(索引结点)具有两种相关含义。第一个含义是指存放在磁盘中的数据结构，其包含了文件大小，主次设备号以及存放文件内容的数据块编号等内容。第二个含义则是内存中的数据结构，它除了包含磁盘上的inode的数据之外，还具有内核所需的额外信息。在XV6中，将第一种数据结构命名为`struct dinode`(位于fs.h文件)，而将第二种数据结构命名为`struct inode`(位于file.h文件)。

**inode结构体：**

```
// in-memory copy of an inode
struct inode {
  uint dev;           // Device number
  uint inum;          // Inode number
  int ref;            // Reference count
  struct sleeplock lock; // protects everything below here
  int valid;          // inode has been read from disk?

  // 文件类型(常规文件,目录文件或者设备文件)
  short type;       
  // 主设备号(只对设备文件有效)
  short major;
  // 次设备号(只对设备文件有效)
  short minor;
  // 引用到此inode的目录项数
  short nlink;
  uint size;
  uint addrs[NDIRECT+1];
};

```
**dinode结构体：**

```
// On-disk inode structure
struct dinode {
  short type;           // File type
  short major;          // Major device number (T_DEVICE only)
  short minor;          // Minor device number (T_DEVICE only)
  short nlink;          // Number of links to inode in file system
  uint size;            // Size of file (bytes)
  uint addrs[NDIRECT+1];   // Data block addresses
};

```
此外，XV6还定义了一个itable结构体来管理inode结构体(注意不是dinode)，其定义如下：

```
// 索引结点表
struct {
  // 自旋锁，确保同一时刻只能有一个线程修改itable中任一inode。实际上，这个自旋锁的粒度过大
  // 会导致当一个线程修改inode1的内容，而另一个线程无法修改inode2内容的情况发生。
  struct spinlock lock;
  // inode结构体数组
  struct inode inode[NINODE];
} itable;
```
同样地，系统在启动时，会通过调用`iinit`函数对inode进行初始化：

```
void iinit()
{
  int i = 0;
  // spinklock *lk = &itable.lock;
  // lk->name = itable; lk->locked = 0; lk->cpu = 0
  initlock(&itable.lock, "itable");
  // 初始化所有inode结构体的睡眠锁
  for(i = 0; i < NINODE; i++) {
    initsleeplock(&itable.inode[i].lock, "inode");
  }
}
```
### 4. 日志
- 文件系统设计中最有趣的问题之一是**崩溃恢复**。出现此问题的原因是，许多文件系统操作都涉及到对磁盘的多次写入，并且在完成写操作的部分子集后，崩溃可能会使磁盘上的文件系统处于不一致的状态。例如，假设在文件截断（将文件长度设置为零并释放其内容块）期间发生崩溃。那么根据磁盘写入的顺序，崩溃可能会导致索引节点引用了标记为空的磁盘块，也有可能丢失了对标记为已分配的磁盘块的引用。

- xv6通过简单的日志记录形式解决了文件系统操作期间的崩溃问题。xv6的系统调用不会直接向磁盘的数据区域写入数据。相反，它首先会向磁盘的数据区域写入数据，随后它就会向磁盘写入一条特殊的 **commit（提交）** 记录，表明日志区域中包含着一个完整的操作。最后，系统会将日志区域中的数据写入到数据区域中，完成这些写入后，系统便会擦除日志区域中的数据。

- 为什么xv6的日志解决了文件系统操作期间的崩溃问题？如果崩溃发生在操作提交之前，那么磁盘上的数据就不会被标记为已完成，那么重新启动时，恢复代码将会擦除日志区域中的数据，并将状态恢复到系统调用发生之前。如果崩溃发生在 **commit（提交）** 之后，则恢复代码会将日志区域上的数据写入到数据区域中。因此，在任何情况下，日志都能使操作在崩溃时成为原子操作，即恢复代码执行完毕后，要么操作的所有写入都显示在磁盘上，要么都不显示。

从图8.2中可以看出，从块2开始的块便是**日志区域**。它由一个头块(header block)和一系列的logged block所组成。header block包含了一个扇区号数组以及用于记录有多少数据块存放在日志区域的计数值N。当计数值N为0时，表示日志区域中的logged blocks没有存放任何内容；而计数值N为非0时，则表示日志区域的logged blocks存放了N个数据块。

需要注意的是，只有在事务正式提交(commit)之后，XV6才会向header block写入数据(记录日志区域的logged blocks存放了多少数据块，以及更新扇区号数组)，并在将logged blocks复制到文件系统的数据区域后将计数值N设置为0。因此，header block用于记录每一次事务操作，而logged blocks则用于存放此次事务所写入的数据。日志系统有关的结构体定义如下：
- **logheader结构体**

```
// 位于磁盘中的log header结构体用于记录日志区的相关信息，而logheader是log header在内存中的映射
struct logheader {
  // n用来记录此时有多少数据块存放在日志区域中,日志空间的总大小记录在超级块中(单位为块)
  int n;
  // block数组是一个int型数组,元素最多为LOGSIZE,该数组用来记录位置关系
  // 写入磁盘是先写入日志区，再写到磁盘的其他区域。因此日志区的磁盘块和其他
  // 区域的磁盘块之间需要有一个映射关系，这个关系就记录在block数组中
  // 例如：block[1] = 1024 代表日志块1记录的数据应该放到1024号磁盘块中
  int block[LOGSIZE];
};
```
- **log结构体**

```
// 该结构体只存在于内存,用来记录当前的日志信息。这个日志信息也是一个公共资源
// 因此需要配备一把锁。start、size、dev这三个属性的值需要从超级块中读取出来
struct log {
  struct spinlock lock; 
  int start; //日志区第一个块的块号(存放log header)
  int size; ///日志区大小,即占据的块数
  int outstanding; // 有多少文件系统调用正在执行
  int committing;  // 用于判断当前是否正处于LOG的提交过程中
  int dev;
  struct logheader lh; // 磁盘log header在内存中的一份映射
};
```
