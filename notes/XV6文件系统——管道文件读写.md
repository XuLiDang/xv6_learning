### 1. 相关结构体及初始化
文件系统中的管道文件是一个较为特殊的文件，特殊之处在于：其并不是存放在硬盘中的文件，而是内存中的一块区域。因此管道文件没有对应的索引结点，也不需要经过日志系统的处理，而与之相关 的结构体分别是file结构体与pipe结构体。

##### 1.1 file结构体
file结构体定义在`file.h`文件中，其具体内容如下：

```
struct file {
  // 当前file结构体文件类型 
  enum { FD_NONE, FD_PIPE, FD_INODE, FD_DEVICE } type;
  // 当前file结构体的引用计数
  int ref; 
  // 是否可以通过该结构体读取数据
  char readable; 
  // 是否可以通过该结构体写入数据
  char writable;
  // 若file结构体的类型为FD_PIPE，那么pipe指针就会指向对应的pipe结构体，否则pipe指针的值为NULL
  struct pipe *pipe; // FD_PIPE
  // 若file结构体的类型为FD_INODE 或 FD_DEVICE，那么ip指针就会指向对应的inode结构体 ，否则ip指针的值为NULL
  struct inode *ip;  // FD_INODE and FD_DEVICE
  // 若file结构体的类型为FD_INODE，那么off则表示文件的读写偏移量
  uint off;          // FD_INODE
  // 若file结构体的类型为FD_DEVICE，那么major则表示设备文件的主设备号
  short major;       // FD_DEVICE
};

```
在`file.c`文件中会定义一个`ftable`(文件表)结构体，定义如下：

```
// 系统中所有打开的文件都保存在全局文件表中
struct {
  // 用于处理并发访问
  struct spinlock lock;
  // 名称为"file"的文件结构体数组
  struct file file[NFILE];
} ftable;
```
该结构体主要定义了一个file结构体数组，每次分配新的file结构体时都需要从数组中查找空闲的file结构体。而在系统启动期间，会调用`fileinit`函数初始化`ftable`结构体。

```
void fileinit(void)，
{
  // spinklock *lk = &ftable.lock;
  // lk->name = ftable; lk->locked = 0; lk->cpu = 0
  initlock(&ftable.lock, "ftable");
}
```

##### 1.2 pipe结构体
pipe结构体定义在`pipe.c`文件中，其具体定义如下：

```
struct pipe {
  // 每个pipe结构体都有一个spinlock结构体，用于处理并发访问
  struct spinlock lock;
  // 环形数据区域
  char data[PIPESIZE];
  // 已从数据区域读取的字节数, nread == nwrite 代表数据区域已全部被读取
  // 此时进程若再从管道文件读取数据便会陷入阻塞态(睡眠态)
  uint nread;     
  // 已向数据区域写入的字节数, nwrite == nread + PIPESIZE 代表数据区域已满
  // 此时进程若再向管道文件写入数据便会陷入阻塞态(睡眠态)
  uint nwrite;    
  int readopen;   // read fd is still open
  int writeopen;  // write fd is still open
};

```

---

### 2. 创建管道文件
应用程序可通过`pipe`系统调用创建管道文件，并返回两个文件描述符。一个用于从管道文件中读取数，一个用于向管道文件写入数据。具体流程如下：

- 应用程序首先执行系统调用`pipe(int *p)`，p为具有两个元素的整形数组的首地址。随后系统会处理由该系统调用所引起的`trap`，并执行`sysfile.c`文件中的`sys_pipe`函数，该函数的代码如下：

```
uint64 sys_pipe(void)
{ 
  // 用于存放应用程序传入的整型数组首地址
  uint64 fdarray; 
  // 读写文件结构体
  struct file *rf, *wf;
  // 读写文件结构体对应的文件描述符
  int fd0, fd1;
  // 获取当前进程的结构体
  struct proc *p = myproc();

  // 获取用户进程传入的整型数组首地址
  if(argaddr(0, &fdarray) < 0)
    return -1;
  // 通过pipealloc函数申请两个file结构体(分别用rf和wf指向), 并且创建一个pipe结构体
  // 随后将两个file结构体的分别设置为可读/可写, 且内部的pipe指针均指向新创建的pipe结构体
  if(pipealloc(&rf, &wf) < 0)
    return -1;

  fd0 = -1;
  // myproc()->ofile[fd0] = rf, myproc()->ofile[fd1] = wf
  // ofile[fd0]指向file结构体rf，而ofile[fd1]指向file结构体wf
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0)
  {
    if(fd0 >= 0)
      p->ofile[fd0] = 0;
    // 减少file结构体的引用计数, 当引用计数为0时则释放file结构体
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  // 将文件描述符返回到用户空间进程
  if(copyout(p->pagetable, fdarray, (char*)&fd0, sizeof(fd0)) < 0 ||
     copyout(p->pagetable, fdarray+sizeof(fd0), (char *)&fd1, sizeof(fd1)) < 0)
  {
    p->ofile[fd0] = 0;
    p->ofile[fd1] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  return 0;
}
```
- 在上面的代码中，`pipealloc`函数执行了较为关键的pipe和file结构体分配的任务，因此我们继续往下查看该函数的定义：

```
int pipealloc(struct file **f0, struct file **f1)
{
  struct pipe *pi;
  pi = 0;
  *f0 = *f1 = 0;
  
  // 通过filealloc函数申请file结构体, 该函数会遍历ftable中的file数组
  // 找到当前第一个引用计数为0的file结构体, 并将其的引用计数加1以及标记
  // 为已使用, 最后返回指向file结构体的指针。
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  // 动态创建pipe结构体, 并用pi指针指向新创建的pipe结构体
  if((pi = (struct pipe*)kalloc()) == 0)
    goto bad;
  // 初始化pipe结构体的内容
  pi->readopen = 1;
  pi->writeopen = 1;
  pi->nwrite = 0;
  pi->nread = 0;
  // 初始化pipe结构体的自旋锁
  initlock(&pi->lock, "pipe");

  // 下面这段代码会将两个不同的file结构体指向同一个pipe结构体
  // 唯一的区别在于一个只允许读，而另一个只允许写
  // 初始化文件结构体f0
  (*f0)->type = FD_PIPE;
  (*f0)->readable = 1;
  (*f0)->writable = 0;
  (*f0)->pipe = pi;
  // 初始化文件结构体f1
  (*f1)->type = FD_PIPE;
  (*f1)->readable = 0;
  (*f1)->writable = 1;
  (*f1)->pipe = pi;
  return 0;

 bad:
  if(pi)
    kfree((char*)pi);
  if(*f0)
    fileclose(*f0);
  if(*f1)
    fileclose(*f1);
  return -1;
}

```

---

### 3. 向管道文件写入数据
应用程序执行`pipe(int *p)`函数后，p所指向的整型数组中，第一个元素便是只读file结构体的文件描述符，而第二个元素则是只写file结构体的文件描述符。那么要向管道文件写入数据，下一步便是执行`write(int fd, char* buf, int size)`系统调用，其中fd参数是只写file结构体的文件描述符。

应用程序执行`write`系统调用后，系统便会调用`sys_write`函数进行处理，该函数首先根据传入的文件描述符获取对应的file结构体以及获取传入的写入数据大小和缓冲区虚拟地址。随后将获取的内容作为参数，调用`filewrite(file *f, uint64 addr, int n)`函数。`filewrite`函数的代码如下：
```
// Write to file f.addr is a user virtual address.
int filewrite(struct file *f, uint64 addr, int n)
{
  int r, ret = 0;
  // 文件结构体不可写则返回-1
  if(f->writable == 0)
    return -1;

  // 若文件结构体type属性的值为FD_PIPE
  if(f->type == FD_PIPE)
  {
    // 调用piperead函数,将n个字节的数据从addr复制到管道文件中
    ret = pipewrite(f->pipe, addr, n);
  } 
  
  // 若文件结构体的类型为FD_DEVICE或FD_INODE
  ...
  ...
  ...
  
  // 文件结构体type属性不属于上述任何一个则直接报错 
  else 
  {
    panic("filewrite");
  }

  return ret;
}
```
可以看到对于类型为`FD_PIPE`的file结构体，`filewrite`函数会调用`pipewrite`函数进行处理，那么接下来我们继续看一看`pipewrite`函数的实现代码：

```
int pipewrite(struct pipe *pi, uint64 addr, int n)
{
  int i = 0;
  struct proc *pr = myproc();

  // 申请自旋锁，防止多个进程同时访问一个pipe结构体
  acquire(&pi->lock);
  // 循环n次，每次向管道文件写入一个字节的数据
  while(i < n){
    // 管道文件已不允许写入数据或当前进程已处于killed状态则返回-1
    if(pi->readopen == 0 || pr->killed){
      release(&pi->lock);
      return -1;
    }
    // 管道文件已满
    if(pi->nwrite == pi->nread + PIPESIZE){ //DOC: pipewrite-full
      // 唤醒等待的channel均为&pi->nread的进程
      wakeup(&pi->nread);
      // 释放自旋锁, 并在&pi->nwrite的channel中睡眠
      sleep(&pi->nwrite, &pi->lock);
    }else {
      char ch;
      // 从用户空间缓冲区读取一个字节的数据
      if(copyin(pr->pagetable, &ch, addr + i, 1) == -1)
        break;
      // 将数据写入管道文件
      pi->data[pi->nwrite++ % PIPESIZE] = ch;
      i++;
    }
  }

  // 唤醒等待的channel均为&pi->nread的进程 
  wakeup(&pi->nread);
  // 释放自旋锁
  release(&pi->lock);
  // 返回成功写入的字节数
  return i;
}
```

---

### 4. 从管道文件读取数据
应用程序执行`pipe(int *p)`函数后，p所指向的整型数组中，第二个元素便是只读file结构体的文件描述符，而第二个元素则是只写file结构体的文件描述符。那么要向管道文件写入数据，下一步便是执行`read(int fd, char* buf, int size)`系统调用，其中fd参数是只读file结构体的文件描述符。

应用程序执行`read`系统调用后，系统便会调用`sys_read`函数进行处理，该函数首先根据传入的文件描述符获取对应的file结构体以及获取传入的读取数据大小和缓冲区虚拟地址。随后将获取的内容作为参数，调用`fileread(file *f, uint64 addr, int n)`函数。`fileread`函数的代码如下：

```
// Read from file f. addr is a user virtual address.
int fileread(struct file *f, uint64 addr, int n)
{
  int r = 0;
  // 文件结构体f不可读则返回-1
  if(f->readable == 0)
    return -1;
  // 若文件结构体type属性的值为FD_PIPE
  if(f->type == FD_PIPE)
  { 
    // 调用piperead函数,读取n个字节的数据到addr中
    r = piperead(f->pipe, addr, n);
  } 
  
  // 若文件结构体的类型为FD_DEVICE或FD_INODE
  ...
  ...
  ...
  
  // 文件结构体type属性不属于上述任何一个则直接报错
  else {
    panic("fileread");
  }

  return r;
}
```
可以看到对于类型为`FD_PIPE`的file结构体，`fileread`函数会调用`piperead`函数进行处理，那么接下来我们继续看一看`piperead`函数的实现代码：

```
int piperead(struct pipe *pi, uint64 addr, int n)
{
  int i;
  struct proc *pr = myproc();
  char ch;

  // 申请自旋锁，防止多个进程同时访问一个pipe结构体
  acquire(&pi->lock);
  // 管道文件内容为空并且管道文件允许写入
  while(pi->nread == pi->nwrite && pi->writeopen){  //DOC: pipe-empty
    // 当前进程的状态为killed
    if(pr->killed){
      // 释放自旋锁
      release(&pi->lock);
      return -1;
    }
    // 释放自旋锁, 并在&pi->nread的channel中睡眠
    sleep(&pi->nread, &pi->lock); //DOC: piperead-sleep
  }
  // 循环n次，每次都从管道文件中读取一个字节的数据
  for(i = 0; i < n; i++){  //DOC: piperead-copy
    // 管道文件数据为空则退出循环
    if(pi->nread == pi->nwrite)
      break;
    // 从管道文件中读取数据
    ch = pi->data[pi->nread++ % PIPESIZE];
    // 将数据放入应用程序的缓冲区
    if(copyout(pr->pagetable, addr + i, &ch, 1) == -1)
      break;
  }
  // 唤醒等待的channel均为&pi->nwrite的进程 
  wakeup(&pi->nwrite);  //DOC: piperead-wakeup
  // 释放自旋锁
  release(&pi->lock);
  // 返回已读取的字节数
  return i;
}
```
