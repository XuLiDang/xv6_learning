### 1. open函数
- 应用程序可通过`open(const char *, int)`函数来打开/创建一个函数，第一个参数是文件的路径，第二个参数则是操作模式（只读，只写，可读可写，创建）。系统随后会调用`sys_open`函数进行处理，该函数首先会判断操作模式是否为"**创建**"，如果是的话，其会调用`create(char *path, short type, short major, short minor)`函数创建`T_FILE`类型的文件(常规文件)，并返回新分配的inode。如果操作模式为其他的话，其便会根据文件的路径找到对应的inode(包括常规文件，设备文件，目录文件等)。

- 经过前面的处理之后，无论是创建还是打开文件，我们都已获取了传入文件路径对应的inode，而`sys_open`函数随后还会做一些条件判断，如打开的文件是否为目录文件且操作模式是否为非只读，打开的文件是否为设备文件且主设备号是否符合要求等。随后该函数便会申请分配一个file结构体并在当前进程的ofile数组(file结构体指针数组)中记录新分配的file结构体，同时将对应的数组下表作为文件描述符(fd)。

- 最后的内容便是填充新分配的file结构体的内容，如将file结构体的type成员设置为对应的文件类型(`FD_DEVICE`, `FD_INODE`等)，设备文件便将type成员设置为`FD_DEVICE`，常规文件和目录文件则将type成员设置为`FD_INODE`，随后便是设置file结构体的ip成员，使其指向当前的inode并设置设置file结构体的读写权限，最后返回文件描述符。需要注意的是：管道文件是不需要通过open函数打开或者创建的。sys_open函数的实现代码如下所示：

```
uint64 sys_open(void)
{
  char path[MAXPATH],target[MAXPATH];
  int fd, omode;
  struct file *f;
  struct inode *ip;
  int n,cycle;

  // 获取用户空间传入的参数
  if((n = argstr(0, path, MAXPATH)) < 0 || argint(1, &omode) < 0)
    return -1;
  // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
  begin_op();

  if(omode & O_CREATE)
  {
    // create函数会对返回的inode进行加锁
    ip = create(path, T_FILE, 0, 0);
    if(ip == 0){
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();
      return -1;
    }
  }
  else 
  {
    // 根据path找到对应的inode,找不到则报错
    if((ip = namei(path)) == 0){
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();
      return -1;
    }
    // namei函数不会对返回的inode进行加锁,因此这里需要锁定path的inode
    ilock(ip);
    // 不允许以非只读模式打开一个目录文件
    if(ip->type == T_DIR && omode != O_RDONLY){
      iunlockput(ip);
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();
      return -1;
    }
  }

  // 设备文件的主设备号必须大于0并小于NDEV
  // 需要注意,运行到这里时,ip指向的inode已被锁定
  if(ip->type == T_DEVICE && (ip->major < 0 || ip->major >= NDEV)){
    iunlockput(ip);
    end_op();
    return -1;
  }

  // 调用filealloc函数申请分配一个file结构体,随后再继续调用
  // fdalloc函数将分配的file结构体存入当前进程的ofile数组中
  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
    if(f)
      fileclose(f);
    iunlockput(ip);
    end_op();
    return -1;
  }

  if(ip->type == T_DEVICE){
    f->type = FD_DEVICE;
    f->major = ip->major;
  }else {
    f->type = FD_INODE;
    f->off = 0;
  }
  f->ip = ip;
  f->readable = !(omode & O_WRONLY);
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);

  if((omode & O_TRUNC) && ip->type == T_FILE){
    itrunc(ip);
  }

  iunlock(ip);
  end_op();

  return fd;
}
```
### 2. sys_mkdir函数
- 上面的open函数除了可以打开文件以外，还可以创建`T_FILE`类型的文件，但目录文件只能通过`sys_mkdir`函数来创建。该函数的定义如下：

```
// 创建目录文件
uint64 sys_mkdir(void)
{
  char path[MAXPATH];
  struct inode *ip;

  // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
  begin_op();
  // 获取用户传入的文件路径，随后调用create函数创建目录文件并返回inode
  if(argstr(0, path, MAXPATH) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
    // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
    end_op();
    return -1;
  }
  iunlockput(ip);
  // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
  end_op();
  return 0;
}
```

### 3. sys_mknod函数
- 同样地，sys_mknod函数可用于创建设备文件(`T_DEVICE`)，该函数的定义如下：

```
// 创建设备文件
uint64 sys_mknod(void)
{
  struct inode *ip;
  char path[MAXPATH];
  int major, minor;
  // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
  begin_op();
  // 获取用户传入的文件路径以及主次设备号，随后调用create函数创建设备文件并返回inode
  if((argstr(0, path, MAXPATH)) < 0 ||
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEVICE, major, minor)) == 0){
    // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
    end_op();
    return -1;
  }
  iunlockput(ip);
  // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
  end_op();
  return 0;
}
```
### 4. create函数
- 从上面的代码中可以看到，无论是`open`函数，`sys_mkdir`函数还是`sys_mknod`函数，其最终都要调用`create`函数创建对应类型的文件，只是传入的文件类型参数不同。那么下面来看看`create`函数的实现代码：

```
// create函数为新的inode创建一个新的名称
static struct inode* create(char *path, short type, short major, short minor)
{
  struct inode *ip, *dp;
  char name[DIRSIZ];

  // 获得路径为path的父目录的inode，name则是路径中最后文件的文件名
  // 例如路径为：/root/123，那么dp则是/root目录文件的inode，而name
  // 代表的是文件名 "123"
  if((dp = nameiparent(path, name)) == 0)
    return 0;

  // 锁定父目录inode，并且若当前inode还未拥有对应dinode的副本
  // 该函数还会将对应dinode的内容复制到该inode中
  ilock(dp);

  // 与普通文件的inode一样，目录文件的inode记录着存放文件数据的数据块号
  // 但不用的地方在于，这些数据块里面的内容是一堆目录项的集合，而每条目录
  // 项的内容为: 子文件的名称和子文件对应的inode号。因此dirlookup函数的作用
  // 则是根据子文件的文件名，找到目录文件中对应的目录项，便能获得子文件对应的
  // inode号。最后根据inode号，从itable中获取对应的inode结构体。这里的ip便是
  // 子文件的inode结构体。 而如果返回的ip为0，则代表子文件目录项并不在目录文件中
  if((ip = dirlookup(dp, name, 0)) != 0){
    iunlockput(dp);
    ilock(ip);
    if(type == T_FILE && (ip->type == T_FILE || ip->type == T_DEVICE))
      return ip;
    iunlockput(ip);
    return 0;
  }

  // 分配新的inode，首先是分配磁盘中的dinode，即将空闲的dinode设置为已使用并将其type成员修改为type参数的值。
  // 随后将新分配的dinode在inode磁盘块中的序号作为inum，然后从itable中寻找
  // 空闲的inode并将第一个空闲的inode设置为已分配，且将inode的dev成员设置为
  // dp->dev，而将inum成员设置为新分配的dinode在inode磁盘块中的序号。这样一
  // 来，dinode便可以通过这两个成员与对应的inode进行绑定。
  if((ip = ialloc(dp->dev, type)) == 0)
    panic("create: ialloc");

  ilock(ip);
  ip->major = major;
  ip->minor = minor;
  // 将引用该inode的目录项设置为1，即父目录中有一个目录项引用到该inode
  ip->nlink = 1;
  iupdate(ip);

  // 新创建的文件为目录文件
  if(type == T_DIR){  // Create . and .. entries.

    dp->nlink++;  // for ".."
    iupdate(dp);
    // No ip->nlink++ for ".": avoid cyclic ref count.
    // 在新创建的目录文件中，一开始就有两个目录项。1. '.'，当前目录文件的inum
    // 2. '..'，父目录文件的inum
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
      panic("create dots");
  }

  // 在父目录文件中创建一个目录项，其包含了子文件的inode序号(inum)和文件名
  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
```
`create`函数的实现代码比较长，那么其整体的实现逻辑：
- . 通过参入的path参数，找到要创建文件的父目录，并返回父目录的inode，找不到则直接返回0。这个过程通过`nameiparent`函数来完成。如果传入的路径是从"**/**"符号开始的，那么`nameiparent`函数就会从根目录开始一级一级地查找，否则便从进程所保存的当前目录开始查找（进程结构体的cwd成员指向的是该进程当前目录的inode，不同进程的cwd成员值并不一定相同）。

- 从父目录文件的目录表中查找是否有包含要创建文件名的目录项，如果有的话则从目录项中获取对应的inum，并根据inum从itable中找到对应的inode，随后根据判断条件返回要创建文件的inode或者直接返回0；若不存在对应目录项，则继续往下处理。这部分的工作由`dirlookup`函数完成。

- 父目录的目录表中不存在关于要创建文件名的目录项，`create`函数随后便会调用`ialloc`函数来分配一个新的inode，该函数需要传入两个参数，分别是"**设备号**"以及"**文件类型**"。

- 分配完inode后，接下来便是一些初始化inode节点的操作，例如设置inode中的major和minor成员的值，而对于T_DIR类型的文件还会有进一步的处理。最后便是在父目录文件中创建一个目录项，其包含了新创建子文件的inode序号(inum)和文件名。最后返回新创建文件的inode。