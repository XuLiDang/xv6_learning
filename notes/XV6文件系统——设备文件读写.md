### 1. 向设备文件写入数据
应用程序首先会`write`系统调用，随后系统便会调用`sys_write`函数进行处理，该函数首先根据传入的文件描述符获取对应的file结构体以及获取传入的写入数据大小和缓冲区虚拟地址。随后将获取的内容作为参数，调用`filewrite(file *f, uint64 addr, int n)`函数。`filewrite`函数的代码如下：

```
// Write to file f.addr is a user virtual address.
int filewrite(struct file *f, uint64 addr, int n)
{
  int r, ret = 0;
  // 文件结构体不可写则返回-1
  if(f->writable == 0)
    return -1;
  
  ......
  ......
  
  // 若文件结构体type属性的值为FD_DEVICE
  else if(f->type == FD_DEVICE)
  { 
    // 满足下列条件中的任意一个都会返回-1
    if(f->major < 0 || f->major >= NDEV || !devsw[f->major].write)
      return -1;
    // 将n个字节的数据从addr复制到设备文件中
    // devsw数组的每一项都是一个devsw结构体，而数组的索引则是设备的主设备号
    // 因此，devsw结构体中定义了对应设备的读写函数。系统启动阶段便会初始化
    // devsw数组中的每一个devsw结构体。
    ret = devsw[f->major].write(1, addr, n);
  } 
  
  ......
  ......
  
  // 文件结构体type属性不属于上述任何一个则直接报错 
  else 
  {
    panic("filewrite");
  }

  return ret;
}
```
可以看到对于类型为`FD_DEVICE`的file结构体，`filewrite`函数首先会判断file结构体保存的主设备号是否符合条件，不符合则直接返回-1。随后便将主设备号作为devsw数组的索引，获取对应的devsw结构体，该结构体中记录着对应设备文件的读写函数，然后调用devsw结构体中`write`函数指针所指向的处理函数。但在XV6中没有找到初始化设备文件读写函数的代码，即没有对devsw结构体进行初始化，因此无法得知下一步的操作该如何进行。

### 2. 从设备文件中读取函数
应用程序首先会`read`系统调用，随后系统便会调用`sys_read`函数进行处理，该函数首先根据传入的文件描述符获取对应的file结构体以及获取传入的写入数据大小和缓冲区虚拟地址。随后将获取的内容作为参数，调用`fileread(file *f, uint64 addr, int n)`函数。`fileread`函数的代码如下：

```
// Read from file f. addr is a user virtual address.
int fileread(struct file *f, uint64 addr, int n)
{
  int r = 0;
  // 文件结构体f不可读则返回-1
  if(f->readable == 0)
    return -1;
    
  ......
  ......
  ......
  
  // 若文件结构体type属性的值为FD_DEVICE
  else if(f->type == FD_DEVICE)
  { 
    // 满足下列条件中的任意一个都会返回-1
    if(f->major < 0 || f->major >= NDEV || !devsw[f->major].read)
      return -1;
    // 读取n个字节的数据到addr中
    // devsw数组的每一项都是一个devsw结构体，而数组的索引则是设备的主设备号
    // 因此，devsw结构体中定义了对应设备的读写函数。系统启动阶段便会初始化
    // devsw数组中的每一个devsw结构体。
    r = devsw[f->major].read(1, addr, n);
  }
  
  ......
  ......
  ......
  
  // 文件结构体type属性不属于上述任何一个则直接报错
  else {
    panic("fileread");
  }

  return r;
}
```
可以看到对于类型为`FD_DEVICE`的file结构体，`fileread`函数首先会判断file结构体保存的主设备号是否符合条件，不符合则直接返回-1。随后便将主设备号作为devsw数组的索引，获取对应的devsw结构体，该结构体中记录着对应设备文件的读写函数，然后调用devsw结构体中`read`函数指针所指向的处理函数。但在XV6中没有找到初始化设备文件读写函数的代码，即没有对devsw结构体进行初始化，因此无法得知下一步的操作该如何进行。