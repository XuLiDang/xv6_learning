#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "spinlock.h"
#include "proc.h"
#include "defs.h"


struct spinlock tickslock;
uint ticks;

extern char trampoline[], uservec[], userret[];

// in kernelvec.S, calls kerneltrap().
void kernelvec();

extern int devintr();

void
trapinit(void)
{
  initlock(&tickslock, "time");
}

// set up to take exceptions and traps while in the kernel.
void
trapinithart(void)
{
  w_stvec((uint64)kernelvec);
}

//
// handle an interrupt, exception, or system call from user space.
// called from trampoline.S
//
void usertrap(void)
{ 
  int which_dev = 0;

  // 查看sstatus寄存器的SPP位，检查trap是否来自用户空间
  // 在处理trap最开始的阶段，RISC-V硬件会在sstatus寄存器
  // 的SPP位中保存当前的模式
  if((r_sstatus() & SSTATUS_SPP) != 0)
    panic("usertrap: not from user mode");

  // send interrupts and exceptions to kerneltrap(),
  // since we're now in the kernel.
  // 将kernelvec的地址保存到stvec寄存器中。这样的话，
  // 在内核中的trap，将交由kernelvec进行处理，而不是uservec。
  w_stvec((uint64)kernelvec);

  struct proc *p = myproc();
  
  // save user program counter.
  // 将sepc寄存器的内容保存到trapframe中
  p->trapframe->epc = r_sepc();
  
  // 检查scause寄存器，判断此次trap的来源，并调用对应的处理函数
  // trap来自系统调用
  if(r_scause() == 8)
  {
    // system call

    if(p->killed)
      exit(-1);

    // sepc points to the ecall instruction,
    // but we want to return to the next instruction.
    p->trapframe->epc += 4;

    // an interrupt will change sstatus &c registers,
    // so don't enable until done with those registers.
    intr_on();

    syscall();
  }
  // trap来自设备中断 
  else if((which_dev = devintr()) != 0)
  { 
   // ok
  } 
  // trap来自异常
  else 
  {   
    printf("usertrap(): unexpected scause %p pid=%d\n", r_scause(), p->pid);
    printf("            sepc=%p stval=%p\n", r_sepc(), r_stval());
    // 代表将要终止当前进程
    p->killed = 1;
  }

  // p->killed则终止当前进程
  if(p->killed)
    exit(-1);

  // give up the CPU if this is a timer interrupt.
  // 若trap来自设备中断，且是时钟中断的话就让出CPU
  if(which_dev == 2)
    yield();

  // 调用usertrapret()函数
  usertrapret();
}

//
// return to user space
//
void usertrapret(void)
{
  struct proc *p = myproc();

  // we're about to switch the destination of traps from
  // kerneltrap() to usertrap(), so turn off interrupts until
  // we're back in user space, where usertrap() is correct.
  intr_off();

  // send syscalls, interrupts, and exceptions to trampoline.S
  w_stvec(TRAMPOLINE + (uservec - trampoline));

  // set up trapframe values that uservec will need when
  // the process next re-enters the kernel.
  // 设置当前进程的trapframe页中的内容，该进程下次进入内核时需要使用
  p->trapframe->kernel_satp = r_satp();         // kernel page table
  p->trapframe->kernel_sp = p->kstack + PGSIZE; // process's kernel stack
  p->trapframe->kernel_trap = (uint64)usertrap;
  p->trapframe->kernel_hartid = r_tp();         // hartid for cpuid()

  // set up the registers that trampoline.S's sret will use
  // to get to user space.
  
  // set S Previous Privilege mode to User.
  unsigned long x = r_sstatus();
  // 将sstatus寄存器的SPP位设置为0，将SIE位设置为1
  // 代表设置当前模式为用户模式，并启用设备中断
  x &= ~SSTATUS_SPP; // clear SPP to 0 for user mode
  x |= SSTATUS_SPIE; // enable interrupts in user mode
  w_sstatus(x);

  // set S Exception Program Counter to the saved user pc.
  // 将sepc寄存器的值设置为当前进程trapframe中保存的值
  w_sepc(p->trapframe->epc);

  // tell trampoline.S the user page table to switch to.
  uint64 satp = MAKE_SATP(p->pagetable);

  // jump to trampoline.S at the top of memory, which 
  // switches to the user page table, restores user registers,
  // and switches to user mode with sret.
  // 这里获得的fn是userret函数的虚拟地址,同样地
  // 在用户页表和内核页表中，该虚拟地址都是指向trampoline页中的某个地址
  uint64 fn = TRAMPOLINE + (userret - trampoline);
  // 调用userret函数,并将TRAPFRAME和satp作为参数传递
  // TRAPFRAME会传递到寄存器a0,satp的值会传送到寄存器a1
  ((void (*)(uint64,uint64))fn)(TRAPFRAME, satp);
}

// interrupts and exceptions from kernel code go here via kernelvec,
// on whatever the current kernel stack is.
void kerneltrap()
{
  int which_dev = 0;
  // 因为yield函数会使得当前进程让出CPU, 而新运行的进程有可能会修改sepc
  // sstatus以及scause寄存器的值, 因此在执行之前需要保存这些寄存器的值
  uint64 sepc = r_sepc();
  uint64 sstatus = r_sstatus();
  uint64 scause = r_scause();
  
  // trap需来自内核空间 
  if((sstatus & SSTATUS_SPP) == 0)
    panic("kerneltrap: not from supervisor mode");
  // 执行该函数时需要先关闭设备中断
  if(intr_get() != 0)
    panic("kerneltrap: interrupts enabled");

  // 调用devintr函数处理设备中断, 若该函数返回0则代表
  // 当前的trap是由异常引起的, 因此需要发出警告信息
  if((which_dev = devintr()) == 0)
  { 
  
    printf("scause %p\n", scause);
    printf("sepc=%p stval=%p\n", r_sepc(), r_stval());
    panic("kerneltrap");
  }

  // give up the CPU if this is a timer interrupt.
  // 如果设备中断属于时钟中断且当前进程处于运行状态, 那么调用yield函数
  // 挂起当前进程, 从而让其他进程有机会运行
  if(which_dev == 2 && myproc() != 0 && myproc()->state == RUNNING)
    yield();

  // the yield() may have caused some traps to occur,
  // so restore trap registers for use by kernelvec.S's sepc instruction.
  // 将sepc以及sstatus寄存器的状态恢复为函数执行之前的状态
  w_sepc(sepc);
  w_sstatus(sstatus);
}

void
clockintr()
{
  acquire(&tickslock);
  ticks++;
  wakeup(&ticks);
  release(&tickslock);
}

// check if it's an external interrupt or software interrupt,
// and handle it.
// returns 2 if timer interrupt,
// 1 if other device,
// 0 if not recognized.
int devintr()
{
  uint64 scause = r_scause();

  if((scause & 0x8000000000000000L) &&(scause & 0xff) == 9)
  {
    // this is a supervisor external interrupt, via PLIC.

    // irq indicates which device interrupted.
    int irq = plic_claim();

    if(irq == UART0_IRQ)
    {
      uartintr();
    } 
    else if(irq == VIRTIO0_IRQ)
    {
      virtio_disk_intr();
    } 
    else if(irq)
    {
      printf("unexpected interrupt irq=%d\n", irq);
    }

    // the PLIC allows each device to raise at most one
    // interrupt at a time; tell the PLIC the device is
    // now allowed to interrupt again.
    if(irq)
      plic_complete(irq);

    return 1;
  } 

  // 
  else if(scause == 0x8000000000000001L)
  {
    // software interrupt from a machine-mode timer interrupt,
    // forwarded by timervec in kernelvec.S.

    if(cpuid() == 0){
      clockintr();
    }
    
    // acknowledge the software interrupt by clearing
    // the SSIP bit in sip.
    w_sip(r_sip() & ~2);

    return 2;
  } 
  else {
    return 0;
  }
}

