// Sleeping locks

#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "proc.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
  initlock(&lk->lk, "sleep lock");
  lk->name = name;
  lk->locked = 0;
  lk->pid = 0;
}

void
acquiresleep(struct sleeplock *lk)
{ 
  // 申请自旋锁
  acquire(&lk->lk);
  // lk->locked代表申请睡眠锁失败
  while (lk->locked) {
    // 释放自旋锁，将进程的状态改为睡眠并且睡眠的channel为lk
    sleep(lk, &lk->lk);
  }
  // 申请锁成功
  lk->locked = 1;
  // 记录持有锁的进程
  lk->pid = myproc()->pid;
  // 释放自旋锁
  release(&lk->lk);
}

void
releasesleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  lk->locked = 0;
  lk->pid = 0;
  // 唤醒睡眠通道为lk的进程
  wakeup(lk);
  release(&lk->lk);
}

int
holdingsleep(struct sleeplock *lk)
{
  int r;
  
  acquire(&lk->lk);
  r = lk->locked && (lk->pid == myproc()->pid);
  release(&lk->lk);
  return r;
}



