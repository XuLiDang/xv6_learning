// File system implementation.  Five layers:
//   + Blocks: allocator for raw disk blocks.
//   + Log: crash recovery for multi-step updates.
//   + Files: inode allocator, reading, writing, metadata.
//   + Directories: inode with special contents (list of other inodes!)
//   + Names: paths like /usr/rtm/xv6/fs.c for convenient naming.
//
// This file contains the low-level file system manipulation
// routines.  The (higher-level) system call implementations
// are in sysfile.c.

#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "stat.h"
#include "spinlock.h"
#include "proc.h"
#include "sleeplock.h"
#include "fs.h"
#include "buf.h"
#include "file.h"

#define min(a, b) ((a) < (b) ? (a) : (b))
// there should be one superblock per disk device, but we run with
// only one device
struct superblock sb; 

// Read the super block.
static void readsb(int dev, struct superblock *sb)
{
  struct buf *bp;

  bp = bread(dev, 1);
  memmove(sb, bp->data, sizeof(*sb));
  brelse(bp);
}

// Init fs
void fsinit(int dev) {
  readsb(dev, &sb);
  if(sb.magic != FSMAGIC)
    panic("invalid file system");
  initlog(dev, &sb);
}

// Zero a block.
static void bzero(int dev, int bno)
{
  struct buf *bp;

  bp = bread(dev, bno);
  memset(bp->data, 0, BSIZE);
  log_write(bp);
  brelse(bp);
}

// Blocks.

// Allocate a zeroed disk block.
static uint balloc(uint dev)
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  // 遍历磁盘中的bitmap块,BPB = BIZE*8
  for(b = 0; b < sb.size; b += BPB)
  {
    // 读取当前的bitmap块
    bp = bread(dev, BBLOCK(b, sb));
    // 遍历bitmap块中的每一个bit,每个bit代表数据区中的一个数据块
    // bit为0代表该数据块的状态为空闲,为1则代表正在使用
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++)
    {
      m = 1 << (bi % 8);
      // bit为0则代表其对应的数据块处于空闲状态
      if((bp->data[bi/8] & m) == 0)
      {  // Is block free?
        // 将bit改为1,代表其对应的数据块正在使用
        bp->data[bi/8] |= m;  // Mark block in use.
        // 在日志中记录对bitmap块的修改
        log_write(bp);
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}

// Free a disk block.
static void bfree(int dev, uint b)
{
  struct buf *bp;
  int bi, m;

  // 找到对应的bitmap块
  bp = bread(dev, BBLOCK(b, sb));
  bi = b % BPB;
  m = 1 << (bi % 8);
  if((bp->data[bi/8] & m) == 0)
    panic("freeing free block");
  // 清除bitmap块中对应的bit
  bp->data[bi/8] &= ~m;
  // 在日志中记录对bitmap块的修改
  log_write(bp);
  brelse(bp);
}

// Inodes.
//
// An inode describes a single unnamed file.
// The inode disk structure holds metadata: the file's type,
// its size, the number of links referring to it, and the
// list of blocks holding the file's content.
//
// The inodes are laid out sequentially on disk at
// sb.startinode. Each inode has a number, indicating its
// position on the disk.
//
// The kernel keeps a table of in-use inodes in memory
// to provide a place for synchronizing access
// to inodes used by multiple processes. The in-memory
// inodes include book-keeping information that is
// not stored on disk: ip->ref and ip->valid.
//
// An inode and its in-memory representation go through a
// sequence of states before they can be used by the
// rest of the file system code.
//
// * Allocation: an inode is allocated if its type (on disk)
//   is non-zero. ialloc() allocates, and iput() frees if
//   the reference and link counts have fallen to zero.
//
// * Referencing in table: an entry in the inode table
//   is free if ip->ref is zero. Otherwise ip->ref tracks
//   the number of in-memory pointers to the entry (open
//   files and current directories). iget() finds or
//   creates a table entry and increments its ref; iput()
//   decrements ref.
//
// * Valid: the information (type, size, &c) in an inode
//   table entry is only correct when ip->valid is 1.
//   ilock() reads the inode from
//   the disk and sets ip->valid, while iput() clears
//   ip->valid if ip->ref has fallen to zero.
//
// * Locked: file system code may only examine and modify
//   the information in an inode and its content if it
//   has first locked the inode.
//
// Thus a typical sequence is:
//   ip = iget(dev, inum)
//   ilock(ip)
//   ... examine and modify ip->xxx ...
//   iunlock(ip)
//   iput(ip)
//
// ilock() is separate from iget() so that system calls can
// get a long-term reference to an inode (as for an open file)
// and only lock it for short periods (e.g., in read()).
// The separation also helps avoid deadlock and races during
// pathname lookup. iget() increments ip->ref so that the inode
// stays in the table and pointers to it remain valid.
//
// Many internal file system functions expect the caller to
// have locked the inodes involved; this lets callers create
// multi-step atomic operations.
//
// The itable.lock spin-lock protects the allocation of itable
// entries. Since ip->ref indicates whether an entry is free,
// and ip->dev and ip->inum indicate which i-node an entry
// holds, one must hold itable.lock while using any of those fields.
//
// An ip->lock sleep-lock protects all ip-> fields other than ref,
// dev, and inum.  One must hold ip->lock in order to
// read or write that inode's ip->valid, ip->size, ip->type, &c.

// 索引结点表
struct {
  // 自旋锁，确保同一时刻只能有一个线程修改buffer。
  struct spinlock lock;
  // inode结构体数组
  struct inode inode[NINODE];
} itable;

void iinit()
{
  int i = 0;
  // spinklock *lk = &itable.lock;
  // lk->name = itable; lk->locked = 0; lk->cpu = 0
  initlock(&itable.lock, "itable");
  // 初始化所有inode结构体的睡眠锁
  for(i = 0; i < NINODE; i++) {
    initsleeplock(&itable.inode[i].lock, "inode");
  }
}

static struct inode* iget(uint dev, uint inum);

// Allocate an inode on device dev.
// Mark it as allocated by  giving it type type.
// Returns an unlocked but allocated and referenced inode.
struct inode* ialloc(uint dev, short type)
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  // 遍历磁盘中的inode
  for(inum = 1; inum < sb.ninodes; inum++){
    // 从磁盘中获取inode块
    bp = bread(dev, IBLOCK(inum, sb));
    // 找到对应的dinode
    dip = (struct dinode*)bp->data + inum%IPB;
    // 当前dinode结点为空闲状态,则对其进行修改
    if(dip->type == 0){  // a free inode
      memset(dip, 0, sizeof(*dip));
      // 将参数type赋值给dinode的type域,意味着将该inode标记为已分配
      dip->type = type;
      // 正式将修改过后的磁盘inode写回磁盘前,需要在日志中记录对磁盘inode的修改
      log_write(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}

// Copy a modified in-memory inode to disk.
// Must be called after every change to an ip->xxx field
// that lives on disk.
// Caller must hold ip->lock.
void iupdate(struct inode *ip)
{
  struct buf *bp;
  struct dinode *dip;
  // 从磁盘中读取存放对应磁盘inode的块
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
  // 获取对应的磁盘inode
  dip = (struct dinode*)bp->data + ip->inum%IPB;
  // 更新磁盘inode
  dip->type = ip->type;
  dip->major = ip->major;
  dip->minor = ip->minor;
  dip->nlink = ip->nlink;
  dip->size = ip->size;
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
  // 正式将修改过后的磁盘inode写回磁盘前,需要在日志中记录对磁盘inode的修改
  log_write(bp);
  // 释放存放磁盘inode块的buffer,这里的实际操作是引用计数减1
  brelse(bp);
}

// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode* iget(uint dev, uint inum)
{
  struct inode *ip, *empty;

  // 接下来的代码有可能需要修改itable,因此需要上锁
  acquire(&itable.lock);

  // Is the inode already in the table?
  empty = 0;
  for(ip = &itable.inode[0]; ip < &itable.inode[NINODE]; ip++){
    // 若在itable中找到对应inode,那么便增加该inode结点的引用计数并返回该结点
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
      ip->ref++;
      release(&itable.lock);
      return ip;
    }
    // 记录第一个引用计数为0的inode
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode entry.
  // empty == 0则代表itable已满
  if(empty == 0)
    panic("iget: no inodes");

  // 代码运行到这里便代码未能从itable中找到对应的inode
  // 因此需要在itable中分配一个新的inode,需要注意的是：
  // 这里并没有从磁盘中读取inode结点,而且直接从itable中分配
  ip = empty;
  // 保存设备号以及inode号
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  // valid代表并没有从磁盘中读取inode
  ip->valid = 0;
  release(&itable.lock);

  return ip;
}

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode* idup(struct inode *ip)
{
  acquire(&itable.lock);
  ip->ref++;
  release(&itable.lock);
  return ip;
}

// Lock the given inode.
// Reads the inode from disk if necessary.
void ilock(struct inode *ip)
{
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  // 申请睡眠锁
  acquiresleep(&ip->lock);

  // valid == 0则代表还未从磁盘中读取inode信息
  if(ip->valid == 0){
    // 读取磁盘中存放dinode的块
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
    // 从块的数据中读取对应的dinode
    dip = (struct dinode*)bp->data + ip->inum%IPB;
    // 将dinode的数据复制到内存inode中
    ip->type = dip->type;
    ip->major = dip->major;
    ip->minor = dip->minor;
    ip->nlink = dip->nlink;
    ip->size = dip->size;
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
    // 释放存放磁盘inode块的buffer
    brelse(bp);
    // valid = 1代表已从磁盘中读取inode信息
    ip->valid = 1;
    if(ip->type == 0)
      panic("ilock: no type");
  }
}

// Unlock the given inode.
void iunlock(struct inode *ip)
{
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
    panic("iunlock");

  releasesleep(&ip->lock);
}

// Drop a reference to an in-memory inode.
// If that was the last reference, the inode table entry can
// be recycled.
// If that was the last reference and the inode has no links
// to it, free the inode (and its content) on disk.
// All calls to iput() must be inside a transaction in
// case it has to free the inode.
void iput(struct inode *ip)
{
  // 接下来的代码有可能需要修改itable,因此需要上锁
  acquire(&itable.lock);

  // 如果只有当前进程引用该inode
  if(ip->ref == 1 && ip->valid && ip->nlink == 0){
    // inode has no links and no other references: truncate and free.

    // ip->ref == 1 means no other process can have ip locked,
    // so this acquiresleep() won't block (or deadlock).
    acquiresleep(&ip->lock);

    release(&itable.lock);
    // 因为nlink = 0,因此需要释放磁盘inode所记录的数据块等信息
    itrunc(ip);
    // type = 0代表该inode(内存及其对应的磁盘inode)是空闲的
    ip->type = 0;
    // 修改磁盘inode,并将其写回磁盘
    iupdate(ip);
    // valid = 0代表该内存inode还未从磁盘inode中读取数据
    ip->valid = 0;

    releasesleep(&ip->lock);

    acquire(&itable.lock);
  }

  // 减少该inode的引用计数
  ip->ref--;
  release(&itable.lock);
}

// Common idiom: unlock, then put.
void iunlockput(struct inode *ip)
{
  iunlock(ip);
  iput(ip);
}

// Inode content
//
// The content (data) associated with each inode is stored
// in blocks on the disk. The first NDIRECT block numbers
// are listed in ip->addrs[].  The next NINDIRECT blocks are
// listed in block ip->addrs[NDIRECT].

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint bmap(struct inode *ip, uint bn)
{
  uint addr, *a;
  struct buf *bp;

  // dinode的addrs数组中,0-11存放的是直接块的块号
  // bn是逻辑块号,即相对于文件内部的块号
  if(bn < NDIRECT){
    // addrs数组对应项的值为0,即bn没有对应的物理块号
    if((addr = ip->addrs[bn]) == 0)
      // 调用balloc分配一个新的数据块,并将其块号存放在addr中
      // 随后将addrs[bn]的值设置为addr中存储的块号
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;

  // bn < NDIRECT + NINDIRECT 且 bn > NDIRECT
  if(bn < NINDIRECT){
    // 在addrs数组中查找间接块对应的物理块号
    // addrs数组对应项的值为0,即bn没有对应的物理块号
    if((addr = ip->addrs[NDIRECT]) == 0)
      // 调用balloc分配一个新的间接块,并将其块号存放在addr中
      // 随后在addrs数组中记录间接块的物理块号
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    // 读取间接块的内容
    bp = bread(ip->dev, addr);
    // a指向buffer的数据区
    a = (uint*)bp->data;
    // 间接块数据区中存放的内容都是对应真正存放数据的数据块号
    // 从间接块中获取对应数据块的块号,为0则代表该数据块未分配使用
    if((addr = a[bn]) == 0){
      // 调用balloc分配一个新的数据块,并将其块号存放在addr中 
      a[bn] = addr = balloc(ip->dev);
      // 在日志中记录对间接块的修改
      log_write(bp);
    }
    brelse(bp);
    // 返回bn对应的数据块号
    return addr;
  }

  // bn > NDIRECT + NINDIRECT则报错
  panic("bmap: out of range");
}

// Truncate inode (discard contents).
// Caller must hold ip->lock.
void itrunc(struct inode *ip)
{
  int i, j;
  struct buf *bp;
  uint *a;

  // 释放inode中的直接块
  for(i = 0; i < NDIRECT; i++){
    if(ip->addrs[i]){
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }

  // 释放inode间接块中的数据块
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
    // 释放inode间接块
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  // 将inode的数据大小设置为0
  ip->size = 0;
  iupdate(ip);
}

// Copy stat information from inode.
// Caller must hold ip->lock.
// 将inode结点的信息复制到stat结构体中
void stati(struct inode *ip, struct stat *st)
{
  st->dev = ip->dev;
  st->ino = ip->inum;
  st->type = ip->type;
  st->nlink = ip->nlink;
  st->size = ip->size;
}

// Read data from inode.
// Caller must hold ip->lock.
// If user_dst==1, then dst is a user virtual address;
// otherwise, dst is a kernel address.
int readi(struct inode *ip, int user_dst, uint64 dst, uint off, uint n)
{
  uint tot, m;
  struct buf *bp;
  // 确保文件偏移量小于文件的大小
  if(off > ip->size || off + n < off)
    return 0;
  // 若文件偏移量+要读取的字节数大于文件的大小
  // 则将要读取修改为 ip->size - off
  if(off + n > ip->size)
    n = ip->size - off;

  // 循环遍历文件的每一个块,tot代表每次readi函数已复制到用户缓冲区的字节数
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    // 从磁盘中读取数据块,并存放在buffer中
    // bmap根据inode以及inode中addres数组的索引值找到对应的数据块号
    // 若数组索引对应的数据块号不存在，则需要先分配一个数据块，并将其
    // 块号放入数组中。
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    // m代表每次循环复制到用户缓冲区的数据量
    m = min(n - tot, BSIZE - off%BSIZE);
    // 将m个字节的数据从buffer复制到用户缓冲区
    if(either_copyout(user_dst, dst, bp->data + (off % BSIZE), m) == -1) {
      brelse(bp);
      tot = -1;
      break;
    }
    brelse(bp);
  }
  return tot;
}

// Write data to inode.
// Caller must hold ip->lock.
// If user_src==1, then src is a user virtual address;
// otherwise, src is a kernel address.
// Returns the number of bytes successfully written.
// If the return value is less than the requested n,
// there was an error of some kind.
int writei(struct inode *ip, int user_src, uint64 src, uint off, uint n)
{
  uint tot, m;
  // 指向buf结构体的指针
  struct buf *bp;
  // 确保文件偏移量小于文件的大小以及写入量大于0
  if(off > ip->size || off + n < off)
    return -1;
  // 确保文件偏移量+要写入的字节数小于MAXFILE*BSIZE
  if(off + n > MAXFILE*BSIZE)
    return -1;

  // 循环遍历文件的每一个块, tot代表每次writei函数所写入的数据量
  // m则代表每次循环写入到buffer数据区的数据量, tot = (n - 1) * m 
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
    // 从磁盘中读取数据块,并存放在buffer中
    // bmap根据inode以及inode中addres数组的索引值找到对应的数据块号
    // 若数组索引对应的数据块号不存在，则需要先分配一个数据块，并将其
    // 块号放入数组中。
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
     // m代表每次循环写入到buffer数据区的数据量 
    m = min(n - tot, BSIZE - off%BSIZE);
    // 将m个字节的数据写入到buffer数据区中
    if(either_copyin(bp->data + (off % BSIZE), user_src, src, m) == -1) {
      brelse(bp);
      break;
    }
    // 在日志中记录对磁盘数据块的修改(实际是对buffer的修改)
    log_write(bp);
    brelse(bp);
  }

  if(off > ip->size)
    ip->size = off;

  // write the i-node back to disk even if the size didn't change
  // because the loop above might have called bmap() and added a new
  // block to ip->addrs[].
  iupdate(ip);

  // 返回此次writei函数所写入的数据量
  return tot;
}

// Directories

int namecmp(const char *s, const char *t)
{
  return strncmp(s, t, DIRSIZ);
}

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode* dirlookup(struct inode *dp, char *name, uint *poff)
{
  uint off, inum;
  struct dirent de;
  // 目录文件inode的type属性必须为T_DIR
  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  // 遍历读取目录文件中的内容
  for(off = 0; off < dp->size; off += sizeof(de)){
    // 从inode中读取目录项,并用de接收
    if(readi(dp, 0, (uint64)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlookup read");、
    // 目录项中的索引结点号为0则代表当前目录项空闲
    if(de.inum == 0)
      continue;
    // 若参数传过来的文件名与当前目录项记录的文件名相同
    if(namecmp(name, de.name) == 0){
      // entry matches path element
      // 更新*poff
      if(poff)
        *poff = off;
      // 获取当前目录项中的索引结点号
      inum = de.inum;
      // 根据设备号以及索引结点号来获取对应的inode
      return iget(dp->dev, inum);
    }
  }

  return 0;
}

// Write a new directory entry (name, inum) into the directory dp.
// 将一个目录项数据写入目录文件inode
int dirlink(struct inode *dp, char *name, uint inum)
{
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  // 首先确保参数传入的文件名不存在于当前目录文件的inode中
  if((ip = dirlookup(dp, name, 0)) != 0){
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  // 遍历读取目录文件中的内容,查找一个空的目录项
  for(off = 0; off < dp->size; off += sizeof(de)){
     // 从目录文件inode中读取目录项,并用de接收
    if(readi(dp, 0, (uint64)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
     // 目录项中的索引结点号为0则代表当前目录项空闲
    if(de.inum == 0)
      break;
  }
  // 将参数传入的文件名复制到空闲目录项的name属性
  strncpy(de.name, name, DIRSIZ);
  // 将参数传入的索引节点号复制到空闲目录项的inum属性
  de.inum = inum;
  // 将修改过的目录项写回磁盘中(实际上是先写入到buffer中,在日志中进行记录)
  if(writei(dp, 0, (uint64)&de, off, sizeof(de)) != sizeof(de))
    panic("dirlink");

  return 0;
}

// Paths

// Copy the next path element from path into name.
// Return a pointer to the element following the copied one.
// The returned path has no leading slashes,
// so the caller can check *path=='\0' to see if the name is the last one.
// If no name to remove, return 0.
//
// Examples:
//   skipelem("a/bb/c", name) = "bb/c", setting name = "a"
//   skipelem("///a//bb", name) = "bb", setting name = "a"
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
// 解析路径名,将传入的路径中最顶层的文件名复制到name指向的地址
// 而path则指向剩下的路径
static char* skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
    path++;
  len = path - s;
  if(len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
    path++;
  return path;
}

// Look up and return the inode for a path name.
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode* namex(char *path, int nameiparent, char *name)
{
  struct inode *ip, *next;

  // 路径以'/'开始代表从根目录开始解析路径
  if(*path == '/')
    // iget函数用于根据设备号和索引节点号来获取对应的inode，若当前没有
    // 对应设备号和索引节点号的inode，则需要从itable中分配一个新的inode
    ip = iget(ROOTDEV, ROOTINO);
  // 否则从当前目录开始解析路径
  else
    // 增加myproc()->cwd的引用计数，即增加当前父目录的inode的引用数
    ip = idup(myproc()->cwd);

  // 调用skipelem函数解析路径,path指向此次解析之后剩下的路径
  // name则指向传入的路径中最顶层的文件名
  while((path = skipelem(path, name)) != 0)
  { 
    // 锁定inode
    ilock(ip);
    // 判断inode的类型是否为T_DIR
    if(ip->type != T_DIR){
      iunlockput(ip);
      return 0;
    }
    // nameiparent ！= 0 且 当前path指向的是路径中最后一个元素
    // 例如:path = skipelem("a",name),那么path = "",而name = 'a'
    // 而这里返回的inode就是文件a上一级的目录文件的inode
    if(nameiparent && *path == '\0'){
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    // 查找下一级目录文件的inode
    if((next = dirlookup(ip, name, 0)) == 0){
      iunlockput(ip);
      return 0;
    }
    iunlockput(ip);
    // ip指向下一级目录文件的inode
    ip = next;
  }
  // nameiparent != 0 且 path = 0
  if(nameiparent){
    iput(ip);
    return 0;
  }
  return ip;
}

struct inode* namei(char *path)
{
  char name[DIRSIZ];
  return namex(path, 0, name);
}

struct inode* nameiparent(char *path, char *name)
{
  return namex(path, 1, name);
}
