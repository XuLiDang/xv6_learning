#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "spinlock.h"
#include "proc.h"
#include "fs.h"
#include "sleeplock.h"
#include "file.h"

#define PIPESIZE 512

struct pipe {
  struct spinlock lock;
  char data[PIPESIZE];
  uint nread;     // number of bytes read
  uint nwrite;    // number of bytes written
  int readopen;   // read fd is still open
  int writeopen;  // write fd is still open
};

int pipealloc(struct file **f0, struct file **f1)
{
  struct pipe *pi;
  pi = 0;
  *f0 = *f1 = 0;

  // 通过filealloc函数申请file结构体, 该函数会遍历ftable中的file数组
  // 找到当前第一个引用计数为0的file结构体, 并将其的引用计数加1以及标记
  // 为已使用, 最后返回指向file结构体的指针。
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  // 动态创建pipe结构体, 并用pi指针指向新创建的pipe结构体
  if((pi = (struct pipe*)kalloc()) == 0)
    goto bad;
  // 初始化pipe结构体的内容
  pi->readopen = 1;
  pi->writeopen = 1;
  pi->nwrite = 0;
  pi->nread = 0;
  // 初始化pipe结构体的自旋锁
  initlock(&pi->lock, "pipe");

  // 下面这段代码会将两个不同的file结构体指向同一个pipe结构体
  // 唯一的区别在于一个只允许读，而另一个只允许写
  // 初始化文件结构体f0
  (*f0)->type = FD_PIPE;
  (*f0)->readable = 1;
  (*f0)->writable = 0;
  (*f0)->pipe = pi;
  // 初始化文件结构体f1
  (*f1)->type = FD_PIPE;
  (*f1)->readable = 0;
  (*f1)->writable = 1;
  (*f1)->pipe = pi;
  return 0;

 bad:
  if(pi)
    kfree((char*)pi);
  if(*f0)
    fileclose(*f0);
  if(*f1)
    fileclose(*f1);
  return -1;
}

void pipeclose(struct pipe *pi, int writable)
{
  acquire(&pi->lock);
  // 管道文件是可读的
  if(writable){
    pi->writeopen = 0;
    // 唤醒所有等待读取管道文件内容的进程
    wakeup(&pi->nread);
  } else {
    pi->readopen = 0;
    // 唤醒所有等待向管道文件写入内容的进程。
    wakeup(&pi->nwrite);
  }
  if(pi->readopen == 0 && pi->writeopen == 0){
    release(&pi->lock);
    // 释放管道文件所占据的内容
    kfree((char*)pi);
  } else
    release(&pi->lock);
}

int pipewrite(struct pipe *pi, uint64 addr, int n)
{
  int i = 0;
  struct proc *pr = myproc();

  // 申请自旋锁，防止多个进程同时访问一个pipe结构体
  acquire(&pi->lock);
  // 循环n次，每次向管道文件写入一个字节的数据
  while(i < n){
    // 管道文件已不允许写入数据或当前进程已处于killed状态则返回-1
    if(pi->readopen == 0 || pr->killed){
      release(&pi->lock);
      return -1;
    }
    // 管道文件已满
    if(pi->nwrite == pi->nread + PIPESIZE){ //DOC: pipewrite-full
      // 唤醒等待的channel均为&pi->nread的进程
      wakeup(&pi->nread);
      // 释放自旋锁, 并在&pi->nwrite的channel中睡眠
      sleep(&pi->nwrite, &pi->lock);
    }else {
      char ch;
      // 从用户空间缓冲区读取一个字节的数据
      if(copyin(pr->pagetable, &ch, addr + i, 1) == -1)
        break;
      // 将数据写入管道文件
      pi->data[pi->nwrite++ % PIPESIZE] = ch;
      i++;
    }
  }

  // 唤醒等待的channel均为&pi->nread的进程 
  wakeup(&pi->nread);
  // 释放自旋锁
  release(&pi->lock);
  // 返回成功写入的字节数
  return i;
}

int piperead(struct pipe *pi, uint64 addr, int n)
{
  int i;
  struct proc *pr = myproc();
  char ch;

  // 申请自旋锁，防止多个进程同时访问一个pipe结构体
  acquire(&pi->lock);
  // 管道文件内容为空并且管道文件允许写入
  while(pi->nread == pi->nwrite && pi->writeopen){  //DOC: pipe-empty
    // 当前进程的状态为killed
    if(pr->killed){
      // 释放自旋锁
      release(&pi->lock);
      return -1;
    }
    // 释放自旋锁, 并在&pi->nread的channel中睡眠
    sleep(&pi->nread, &pi->lock); //DOC: piperead-sleep
  }
  // 循环n次，每次都从管道文件中读取一个字节的数据
  for(i = 0; i < n; i++){  //DOC: piperead-copy
    // 管道文件数据为空则退出循环
    if(pi->nread == pi->nwrite)
      break;
    // 从管道文件中读取数据
    ch = pi->data[pi->nread++ % PIPESIZE];
    // 将数据放入应用程序的缓冲区
    if(copyout(pr->pagetable, addr + i, &ch, 1) == -1)
      break;
  }
  // 唤醒等待的channel均为&pi->nwrite的进程 
  wakeup(&pi->nwrite);  //DOC: piperead-wakeup
  // 释放自旋锁
  release(&pi->lock);
  // 返回已读取的字节数
  return i;
}
