#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "fs.h"
#include "buf.h"

// Simple logging that allows concurrent FS system calls.
//
// A log transaction contains the updates of multiple FS system
// calls. The logging system only commits when there are
// no FS system calls active. Thus there is never
// any reasoning required about whether a commit might
// write an uncommitted system call's updates to disk.
//
// A system call should call begin_op()/end_op() to mark
// its start and end. Usually begin_op() just increments
// the count of in-progress FS system calls and returns.
// But if it thinks the log is close to running out, it
// sleeps until the last outstanding end_op() commits.
//
// The log is a physical re-do log containing disk blocks.
// The on-disk log format:
//   header block, containing block #s for block A, B, C, ...
//   block A
//   block B
//   block C
//   ...
// Log appends are synchronous.


// 位于磁盘中的log header结构体用于记录日志区的相关信息，而logheader是log header在内存中的映射
struct logheader {
  // n用来记录此时有多少数据块存放在日志区域中,日志空间的总大小记录在超级块中(单位为块)
  int n;
  // block数组是一个int型数组,元素最多为LOGSIZE,该数组用来记录位置关系
  // 写入磁盘是先写入日志区，再写到磁盘的其他区域。因此日志区的磁盘块和其他
  // 区域的磁盘块之间需要有一个映射关系，这个关系就记录在block数组中
  // 例如：block[1] = 1024 代表日志块1记录的数据应该放到1024号磁盘块中
  int block[LOGSIZE];
};

// 该结构体只存在于内存,用来记录当前的日志信息。这个日志信息也是一个公共资源
// 因此需要配备一把锁。start、size、dev这三个属性的值需要从超级块中读取出来
struct log {
  struct spinlock lock; 
  int start; //日志区第一个块的块号(存放log header)
  int size; ///日志区大小,即占据的块数
  int outstanding; // 有多少文件系统调用正在执行
  int committing;  // 用于判断当前是否正处于LOG的提交过程中
  int dev;
  struct logheader lh; // 磁盘log header在内存中的一份映射
};
struct log log;

static void recover_from_log(void);
static void commit();

void initlog(int dev, struct superblock *sb)
{
  if (sizeof(struct logheader) >= BSIZE)
    panic("initlog: too big logheader");

  // 初始化日志锁
  initlock(&log.lock, "log");
  /*根据超级块的信息设置日志的一些信息*/
  log.start = sb->logstart; // 日志区第一个块的块号(存放log header)
  log.size = sb->nlog; // log block的个数
  log.dev = dev; // 日志所在设备的设备号
  // 从日志中恢复,每次启动调用初始化函数它都会执行这个函数来保证文件系统的一致性
  recover_from_log();
}

// Copy committed blocks from log to their home location
static void install_trans(int recovering)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk 将buffer的数据写入到磁盘中

    // 1代表是在恢复阶段调用该函数,0则是在commit函数中调用该函数
    if(recovering == 0)
      // 将dbuf的引用计数减1
      bunpin(dbuf);
    // 释放缓存块
    brelse(lbuf);
    brelse(dbuf);
  }
}

// Read the log header from disk into the in-memory log header
// 从磁盘读取log header,并用内存中的logheader接收
static void read_head(void)
{ 
  // 读取日志区第一个块的内容
  struct buf *buf = bread(log.dev, log.start);
  // 用logheader接收
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  // 最后将相关信息赋值给log结构体中的lh属性
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
    log.lh.block[i] = lh->block[i];
  }
  // 释放buffer
  brelse(buf);
}

// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
// 将内存中logheader的数据写回到磁盘中的log header
static void write_head(void)
{ 
  // 读取日志区第一个块的内容
  struct buf *buf = bread(log.dev, log.start);
   // 用logheader接收
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  // 将log结构体中lh属性的内容赋值给hb
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
    hb->block[i] = log.lh.block[i];
  }
  // 将buffer的内容写回到磁盘中
  bwrite(buf);
  brelse(buf);
}

static void recover_from_log(void)
{ 
  // 从磁盘读取log header,并用内存中的logheader接收
  read_head();
  // 将磁盘中的日志块数据复制到应在的磁盘块中
  install_trans(1); // if committed, copy from log to disk
  log.lh.n = 0;
  // 将内存中logheader的数据写回到磁盘中的log header
  write_head(); // clear the log
}

// called at the start of each FS system call.
// begin_op()表明一个文件系统调用开始，它将一直等待直到日志处于未提交状态
// 直到有足够的日志空间保存当前所有调用的写入。
void begin_op(void)
{
  acquire(&log.lock);
  while(1){
    //如果日志正在提交，休眠
    if(log.committing){
      sleep(&log, &log.lock);
    } 
    // xv6假设每个系统调用可能写入 MAXOPBLOCKS 个块，outstanding 则表示当前正在执行的系统调用个数，
    // outstanding+1就表示加上自身这个系统调用，这个数乘以 MAXOPBLOCKS 就表示当前所有的系统调用可能写入的最大块数
    // log.lh.n表示当前的日志空间已经使用的块数，它们两者之和如果小于日志空间，则可以继续下一步，否则等待。
    else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } 
    else {
      // 正在执行的文件系统调用数+1
      log.outstanding += 1;
      // 释放锁
      release(&log.lock);
      break;
    }
  }
}

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void end_op(void)
{
  int do_commit = 0;

  acquire(&log.lock);
  // 正在执行的文件系统系统调用数减1
  log.outstanding -= 1;
  // 如果日志正在提交则发出警告
  if(log.committing)
    panic("log.committing");
  // 若正在执行的文件系统系统调用数为0，则提交事务
  if(log.outstanding == 0){
    do_commit = 1;
    log.committing = 1;
  } else {
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    // 唤醒休眠在log上的进程
    wakeup(&log);
  }
  release(&log.lock);

  // do_commit不为0则代表接下来需要提交事务
  if(do_commit){
    // call commit w/o holding locks, since not allowed
    // to sleep with locks.
    commit();
    acquire(&log.lock);
    log.committing = 0;
    wakeup(&log);
    release(&log.lock);
  }
}

// Copy modified blocks from cache to log.
static void write_log(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
    memmove(to->data, from->data, BSIZE);
    bwrite(to);  // write the log
    brelse(from);
    brelse(to);
  }
}

static void commit()
{
  if (log.lh.n > 0) {
    write_log();     // Write modified blocks from cache to log
    // 崩溃在write_head之前发生，则在恢复时不会重复之前的写操做。反之，在恢复时是重复执行写操作
    write_head();    // Write header to disk -- the real commit
    install_trans(0); // Now install writes to home locations
    log.lh.n = 0;
    write_head();    // Erase the transaction from the log
  }
}

// Caller has modified b->data and is done with the buffer.
// Record the block number and pin in the cache by increasing refcnt.
// commit()/write_log() will do the disk write.
//
// log_write() replaces bwrite(); a typical use is:
//   bp = bread(...)
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)

// 记录日志区块号与数据区块号的映射关系
void log_write(struct buf *b)
{
  int i;

  acquire(&log.lock);
  //当前已使用的日志空间不能大于规定的大小
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
    panic("too big a transaction");
  //当前正执行的系统调用不能小于1
  if (log.outstanding < 1)
    panic("log_write outside of trans");
  // 同一个块在单个事务中多次写入的时候，会先在block数组中查找是否记录了当前缓存块
  for (i = 0; i < log.lh.n; i++) {
    if (log.lh.block[i] == b->blockno)   // log absorption
      break;
  }
  // 如果block数组中记录了当前缓存块，那么该行代码并无实际作用
  // 但如果block数组中没有记录，那么该行代码会更新block数组
  // 这样操作即使一个块在单个事务中多次写入，也只会占用一个日志块，节省了日志空间，这种优化操作就叫做吸收。
  log.lh.block[i] = b->blockno;
  // i == log.lh.n 代表block数组中没有记录当前缓存块
  if (i == log.lh.n) {  // Add new block to log?
    // 增加缓存块的引用计数,防止被释放
    bpin(b);
    log.lh.n++;
  }
  release(&log.lock);
}

