//
// Support functions for system calls that involve file descriptors.
// 该文件的函数与变量均与文件结构体file有关。

#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "fs.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "file.h"
#include "stat.h"
#include "proc.h"

// struct spinlock {
//   uint locked;       // Is the lock held?
//   char *name;        // Name of lock.
//   struct cpu *cpu;   // The cpu holding the lock.
// };

// // map major device number to device functions.
// struct devsw {
//   // 函数指针
//   int (*read)(int, uint64, int);
//   int (*write)(int, uint64, int);
// };

struct devsw devsw[NDEV];
// 系统中所有打开的文件都保存在全局文件表中
struct {
  struct spinlock lock;
  // 名称为"file"的文件结构体数组
  struct file file[NFILE];
} ftable;

void fileinit(void)，
{
  // spinklock *lk = &ftable.lock;
  // lk->name = ftable; lk->locked = 0; lk->cpu = 0
  initlock(&ftable.lock, "ftable");
}

// Allocate a file structure.
struct file* filealloc(void)
{
  struct file *f;
  // 申请ftable的自旋锁
  // spinklock *lk = &ftable.lock; lk->cpu = mycpu();
  // while(__sync_lock_test_and_set(&lk->locked, 1) != 0); 
  acquire(&ftable.lock);
  // 遍历全局文件表ftable中file数组,找到第一个引用计数为0的文件结构体
  for(f = ftable.file; f < ftable.file + NFILE; f++){
    if(f->ref == 0){
      // 将其引用计数加1,将该文件结构体标记为已使用
      f->ref = 1;
      // 释放ftable的自旋锁
      // spinklock *lk = &ftable.lock; lk->locked = 0; lk->cpu = 0;
      release(&ftable.lock);
      // 返回所分配的文件结构体
      return f;
    }
  }
  // spinklock *lk = &ftable.lock; lk->locked = 0; lk->cpu = 0;
  release(&ftable.lock);
  return 0;
}

// Increment ref count for file f.
struct file* filedup(struct file *f)
{
  // 申请ftable的自旋锁
  acquire(&ftable.lock);
  // 传入的文件结构体的引用计数小于1则报错
  if(f->ref < 1)
    panic("filedup");
  // 增加传入的文件结构体的引用计数
  f->ref++;
  // 释放ftable的自旋锁
  release(&ftable.lock);
  return f;
}

// Close file f.  (Decrement ref count, close when reaches 0.)
void fileclose(struct file *f)
{
  struct file ff;
  // 申请ftable的自旋锁
  acquire(&ftable.lock);
  // 传入的文件结构体的引用计数小于1则报错
  if(f->ref < 1)
    panic("fileclose");
  // 减少传入的文件结构体的引用计数
  // 减少后引用计数若仍大于0则直接返回
  if(--f->ref > 0){
    release(&ftable.lock);
    return;
  }
  // 减少后引用计数若小于0则释放传入的文件结构体，ff是f所指向的文件结构体的副本
  // f指向的结构体不会被释放，但里面的成员的值会被清除，最后释放的是ff所占的内存
  ff = *f;
  // 将引用计数设置为0
  f->ref = 0;
  // 将类型设置为FD_NONE
  f->type = FD_NONE;
  // 释放ftable的自旋锁
  release(&ftable.lock);
  // 文件结构体指向的文件的类型为管道
  if(ff.type == FD_PIPE)
  { 
    // 释放管道文件所占据的内存
    pipeclose(ff.pipe, ff.writable);
  } 
  // 文件结构体指向的文件的类型为普通文件或设备文件
  else if(ff.type == FD_INODE || ff.type == FD_DEVICE)
  {
    begin_op();
    // 减少文件结构体中inode的引用计数,在满足特定条件下,甚至会释放该inode
    iput(ff.ip);
    end_op();
  }
}

// Get metadata about file f.
// addr is a user virtual address, pointing to a struct stat.
int filestat(struct file *f, uint64 addr)
{
  struct proc *p = myproc();
  // struct stat {
  //   int dev;     // File system's disk device
  //   uint ino;    // Inode number
  //   short type;  // Type of file
  //   short nlink; // Number of links to file
  //   uint64 size; // Size of file in bytes
  // };
  struct stat st;
  
  // 要求文件结构体type属性的值必须为FD_INODE或FD_DEVICE
  if(f->type == FD_INODE || f->type == FD_DEVICE)
  { 
    // 锁定file结构体中的inode
    ilock(f->ip);
    // 将inode结点的信息复制到stat结构体中
    stati(f->ip, &st);
    // 释放file结构体中的inode
    iunlock(f->ip);
    // 将数据复制到用户虚拟地址addr所指的地址中
    if(copyout(p->pagetable, addr, (char *)&st, sizeof(st)) < 0)
      return -1;
    return 0;
  }
  return -1;
}

// Read from file f. addr is a user virtual address.
int fileread(struct file *f, uint64 addr, int n)
{
  int r = 0;
  // 文件结构体f不可读则返回-1
  if(f->readable == 0)
    return -1;
  // 若文件结构体type属性的值为FD_PIPE
  if(f->type == FD_PIPE)
  { 
    // 调用piperead函数,读取n个字节的数据到addr中
    r = piperead(f->pipe, addr, n);
  } 
  // 若文件结构体type属性的值为FD_DEVICE
  else if(f->type == FD_DEVICE)
  { 
    // 满足下列条件中的任意一个都会返回-1
    if(f->major < 0 || f->major >= NDEV || !devsw[f->major].read)
      return -1;
    // 读取n个字节的数据到addr中
    // devsw数组的每一项都是一个devsw结构体，而数组的索引则是设备的主设备号
    // 因此，devsw结构体中定义了对应设备的读写函数。系统启动阶段便会初始化
    // devsw数组中的每一个devsw结构体。
    r = devsw[f->major].read(1, addr, n);
  }
  // 若文件结构体type属性的值为FD_INODE
  else if(f->type == FD_INODE){
    // 锁定文件结构体中的inode
    ilock(f->ip);
    // 从偏移量f->off开始, 读取n个字节的数据到addr中
    if((r = readi(f->ip, 1, addr, f->off, n)) > 0)
      f->off += r;
    // 释放文件结构体中的inode
    iunlock(f->ip);
  } 
  // 文件结构体type属性不属于上述任何一个则直接报错
  else {
    panic("fileread");
  }

  return r;
}

// Write to file f.addr is a user virtual address.
int filewrite(struct file *f, uint64 addr, int n)
{
  int r, ret = 0;
  // 文件结构体不可写则返回-1
  if(f->writable == 0)
    return -1;

  // 若文件结构体type属性的值为FD_PIPE
  if(f->type == FD_PIPE)
  {
    // 调用piperead函数,将n个字节的数据从addr复制到管道文件中
    ret = pipewrite(f->pipe, addr, n);
  } 
  // 若文件结构体type属性的值为FD_DEVICE
  else if(f->type == FD_DEVICE)
  { 
    // 满足下列条件中的任意一个都会返回-1
    if(f->major < 0 || f->major >= NDEV || !devsw[f->major].write)
      return -1;
    // 将n个字节的数据从addr复制到设备文件中
    ret = devsw[f->major].write(1, addr, n);
  } 
  // 若文件结构体type属性的值为FD_INODE
  else if(f->type == FD_INODE)
  {
    // write a few blocks at a time to avoid exceeding
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * BSIZE;
    int i = 0;
    // 写入数据
    while(i < n)
    { 
      // n1表示每次实际写入的数据量
      int n1 = n - i;
      if(n1 > max)
        n1 = max;

      // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
      begin_op();
      // 申请睡眠锁，防止多个线程同时访问同一个inode
      ilock(f->ip);
      // 从 addr + i 地址开始，将其存放的n1个字节的数据写入到f->ip的f->off偏移处
      // 参数 1 是指 addr + i 代表的是用户空间的虚拟地址
      if ((r = writei(f->ip, 1, addr + i, f->off, n1)) > 0)
        f->off += r;
      // 释放睡眠锁
      iunlock(f->ip);
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();

      // 所返回的写入文件的数据量与n1不相等则代表发生了错误
      if(r != n1){
        // error from writei
        break;
      }
      // 更新已写入的数据量
      i += r;
    }
    // 返回成功写入的数据量，若i == n 则代表成功写入n个字节的数据
    // 两者不相等则代表发生了错误, 因此返回-1
    ret = (i == n ? n : -1);
  }
  // 文件结构体type属性不属于上述任何一个则直接报错 
  else 
  {
    panic("filewrite");
  }

  return ret;
}

