// Buffer cache.
//
// The buffer cache is a linked list of buf structures holding
// cached copies of disk block contents.  Caching disk blocks
// in memory reduces the number of disk reads and also provides
// a synchronization point for disk blocks used by multiple processes.
//
// Interface:
// * To get a buffer for a particular disk block, call bread.
// * After changing buffer data, call bwrite to write it to disk.
// * When done with the buffer, call brelse.
// * Do not use the buffer after calling brelse.
// * Only one process at a time can use a buffer,
//     so do not keep them longer than necessary.


#include "types.h"
#include "param.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "riscv.h"
#include "defs.h"
#include "fs.h"
#include "buf.h"

// buffer缓冲区管理结构体
struct {
  struct spinlock lock;
  struct buf buf[NBUF];

  // Linked list of all buffers, through prev/next.
  // Sorted by how recently the buffer was used.
  // head.next is most recent, head.prev is least.
  // buffer结构体链表头结点
  struct buf head;
} bcache;

// 初始化buffer缓冲区
void binit(void)
{
  struct buf *b;
  // spinklock *lk = &bcache;
  // lk->name = bcache; lk->locked = 0; lk->cpu = 0
  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  // bcache.head是双向链表的头结点
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  // buffer结构体已用数组的方式组织起来了
  // 现在就是要用链表的形式组织buffer结构体，并采用头插法的形式创建链表
  for(b = bcache.buf; b < bcache.buf + NBUF; b++)
  {
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}

// scans the buffer list for a buffer with the given device and sector numbers.
// If not found, allocate a buffer.
// In either case, return locked buffer.
static struct buf* bget(uint dev, uint blockno)
{
  struct buf *b;

  // 接下来要遍历buffer缓冲区,因此需要对bcache上锁
  // bcache结构体包含着buffer缓冲区的头结点
  acquire(&bcache.lock);

  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
    if(b->dev == dev && b->blockno == blockno){
      // 增加该buffer的引用计数
      b->refcnt++; 
      // 释放对bcache结构体的锁
      release(&bcache.lock);
      // 为该buffer申请sleep-lock,确保同一时间只有一个线程在使用该buffer
      acquiresleep(&b->lock);
      return b;
    }
  }

  // Not cached.
  // Recycle the least recently used (LRU) unused buffer.
  // 再次遍历buffer缓冲区,查找引用计数为0的buffer,优先从链表尾部开始
  // 所谓buffer缓冲区就是一个buffer的双向循环链表
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
    // 修改buffer的元数据来记录新的设备号和块号(扇区号)
    if(b->refcnt == 0) {
      b->dev = dev;
      b->blockno = blockno;
      b->valid = 0;
      b->refcnt = 1;
      // 释放对bcache结构体的锁
      release(&bcache.lock);
      // 为该buffer申请sleep-lock
      acquiresleep(&b->lock);
      return b;
    }
  }
  panic("bget: no buffers");
}

// Return a locked buf with the contents of the indicated block.
struct buf* bread(uint dev, uint blockno)
{
  struct buf *b;

  // 获得指定扇区的buffer,没有则返回一个新的buffer
  b = bget(dev, blockno);
  // 判断返回的buffer是否拥有对应块(扇区)的数据
  if(!b->valid) {
    // 从对应的磁盘块中读取数据，并存放到buffer中
    // 第二个参数为1则代表将buffer中的数据写入到磁盘块
    virtio_disk_rw(b, 0);
    b->valid = 1;
  }
  return b;
}

// Write b's contents to disk.  Must be locked.
void bwrite(struct buf *b)
{
  // buffer必须持有sleep-lock才能将修改的数据写入磁盘中
  if(!holdingsleep(&b->lock))
    panic("bwrite");
  virtio_disk_rw(b, 1);
}

// Release a locked buffer.
// Move to the head of the most-recently-used list.
void brelse(struct buf *b)
{
  if(!holdingsleep(&b->lock))
    panic("brelse");

  // 释放sleep-lock
  releasesleep(&b->lock);

  // 下面的代码要修改buffer的元数据信息,因此要申请bcache.lock
  acquire(&bcache.lock);
  b->refcnt--;
  // 若该buffer的引用计数为0,则将其放在buffer缓冲区的头部
  if (b->refcnt == 0) {
    // no one is waiting for it.
    b->next->prev = b->prev;
    b->prev->next = b->next;
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
  
  release(&bcache.lock);
}

void bpin(struct buf *b) {
  // 修改buffer元数据时要持有cache.lock
  acquire(&bcache.lock);
  b->refcnt++;
  release(&bcache.lock);
}

void bunpin(struct buf *b) {
  // 修改buffer元数据时要持有cache.lock
  acquire(&bcache.lock);
  b->refcnt--;
  release(&bcache.lock);
}


