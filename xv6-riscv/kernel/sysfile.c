//
// File-system system calls.
// Mostly argument checking, since we don't trust
// user code, and calls into file.c and fs.c.
//

#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "stat.h"
#include "spinlock.h"
#include "proc.h"
#include "fs.h"
#include "sleeplock.h"
#include "file.h"
#include "fcntl.h"

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
    return -1;
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
    return -1;
  if(pfd)
    *pfd = fd;
  if(pf)
    *pf = f;
  return 0;
}

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
  int fd;
  struct proc *p = myproc();

  for(fd = 0; fd < NOFILE; fd++){
    if(p->ofile[fd] == 0){
      p->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
}

uint64
sys_dup(void)
{
  struct file *f;
  int fd;

  if(argfd(0, 0, &f) < 0)
    return -1;
  if((fd=fdalloc(f)) < 0)
    return -1;
  filedup(f);
  return fd;
}

uint64
sys_read(void)
{
  struct file *f;
  int n;
  uint64 p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argaddr(1, &p) < 0)
    return -1;
  return fileread(f, p, n);
}

uint64
sys_write(void)
{
  struct file *f;
  int n;
  uint64 p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argaddr(1, &p) < 0)
    return -1;

  return filewrite(f, p, n);
}

uint64
sys_close(void)
{
  int fd;
  struct file *f;

  if(argfd(0, &fd, &f) < 0)
    return -1;
  myproc()->ofile[fd] = 0;
  fileclose(f);
  return 0;
}

uint64
sys_fstat(void)
{
  struct file *f;
  uint64 st; // user pointer to struct stat

  if(argfd(0, 0, &f) < 0 || argaddr(1, &st) < 0)
    return -1;
  return filestat(f, st);
}

// Create the path new as a link to the same inode as old.
// 为一个现有的inode添加一个新的文件名
uint64 sys_link(void)
{
  char name[DIRSIZ], new[MAXPATH], old[MAXPATH];
  struct inode *dp, *ip;

  // 从用户空间获得参数,old是被链接文件的路径,new是新建立链接文件的路径
  if(argstr(0, old, MAXPATH) < 0 || argstr(1, new, MAXPATH) < 0)
    return -1;

  // 开始事务
  begin_op();
  // 获得被链接文件的inode
  if((ip = namei(old)) == 0){
    end_op();
    return -1;
  }
  // 锁定被链接文件的inode
  ilock(ip);
  // 被链接文件的类型不能为目录文件
  if(ip->type == T_DIR){
    iunlockput(ip);
    end_op();
    return -1;
  }
  // 被链接文件inode的引用数加1(默认链接成功)
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  // 获得在new路径中,文件名为name的父目录的inode结点
  if((dp = nameiparent(new, name)) == 0)
    goto bad;
  // 锁定name父目录的inode结点
  ilock(dp);
  // dirlink函数的作用是将一个目录项数据(name,ip->num)写入目录文件inode(dp)中
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
    iunlockput(dp);
    goto bad;
  }
  iunlockput(dp);
  iput(ip);

  end_op();

  return 0;

bad:
  ilock(ip);
  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);
  end_op();
  return -1;
}

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
    if(readi(dp, 0, (uint64)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
}

// unlink操作核心的步骤是：获取要unlink的文件的inode(ip)以及其父目录的inode(dp)
// 随后在dp所对应的数据块中将ip相关的目录项信息清除,最后将ip的nlink值减1
uint64 sys_unlink(void)
{
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], path[MAXPATH];
  uint off;

  // 从用户空间获得参数
  if(argstr(0, path, MAXPATH) < 0)
    return -1;
  // 开启事务
  begin_op();
  // 获得路径为path的父目录的inode·
  if((dp = nameiparent(path, name)) == 0){
    end_op();
    return -1;
  }
  // 锁定该父目录的inode
  ilock(dp);

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  // 获得path对应的inode
  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
  ilock(ip);
  // inode的nlink属性小于1则报错
  if(ip->nlink < 1)
    panic("unlink: nlink < 1");
  // path为目录文件且该目录文件中包含着其他文件
  if(ip->type == T_DIR && !isdirempty(ip)){
    iunlockput(ip);
    goto bad;
  }
  // 将用0初始化de
  memset(&de, 0, sizeof(de));
  if(writei(dp, 0, (uint64)&de, off, sizeof(de)) != sizeof(de))
    panic("unlink: writei");
  if(ip->type == T_DIR){
    dp->nlink--;
    iupdate(dp);
  }
  iunlockput(dp);

  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);

  end_op();

  return 0;

bad:
  iunlockput(dp);
  end_op();
  return -1;
}

// create函数为新的inode创建一个新的名称
static struct inode* create(char *path, short type, short major, short minor)
{
  struct inode *ip, *dp;
  char name[DIRSIZ];

  // 获得路径为path的父目录的inode，name则是路径中最后文件的文件名
  // 例如路径为：/root/123，那么dp则是/root目录文件的inode，而name
  // 代表的是文件名 "123"
  if((dp = nameiparent(path, name)) == 0)
    return 0;

  // 锁定父目录inode，并且若当前inode还未拥有对应dinode的副本
  // 该函数还会将对应dinode的内容复制到该inode中
  ilock(dp);

  // 与普通文件的inode一样，目录文件的inode记录着存放文件数据的数据块号
  // 但不用的地方在于，这些数据块里面的内容是一堆目录项的集合，而每条目录
  // 项的内容为: 子文件的名称和子文件对应的inode号。因此dirlookup函数的作用
  // 则是根据子文件的文件名，找到目录文件中对应的目录项，便能获得子文件对应的
  // inode号。最后根据inode号，从itable中获取对应的inode结构体。这里的ip便是
  // 子文件的inode结构体。 而如果返回的ip为0，则代表子文件目录项并不在目录文件中
  if((ip = dirlookup(dp, name, 0)) != 0){
    iunlockput(dp);
    ilock(ip);
    if(type == T_FILE && (ip->type == T_FILE || ip->type == T_DEVICE))
      return ip;
    iunlockput(ip);
    return 0;
  }

  // 首先会读取磁盘中的inode块信息，并且分配一个空闲的dinode(即将dinode的type成员从0改为参数type的值)
  // 随后将新分配的dinode在inode磁盘块中的序号作为inum，然后从itable中寻找
  // 空闲的inode并将第一个空闲的inode设置为已分配，且将inode的dev成员设置为
  // dp->dev，而将inum成员设置为新分配的dinode在inode磁盘块中的序号。这样一
  // 来，dinode便可以通过这两个成员与对应的inode进行绑定。
  if((ip = ialloc(dp->dev, type)) == 0)
    panic("create: ialloc");

  ilock(ip);
  ip->major = major;
  ip->minor = minor;
  // 将引用该inode的目录项设置为1，即父目录中有一个目录项引用到该inode
  ip->nlink = 1;
  iupdate(ip);

  // 新创建的文件为目录文件
  if(type == T_DIR){  // Create . and .. entries.

    dp->nlink++;  // for ".."
    iupdate(dp);
    // No ip->nlink++ for ".": avoid cyclic ref count.
    // 在新创建的目录文件中，一开始就有两个目录项。1. '.'，当前目录文件的inum
    // 2. '..'，父目录文件的inum
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
      panic("create dots");
  }

  // 在父目录文件中创建一个目录项，其包含了子文件的inode序号(inum)和文件名
  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}

uint64 sys_open(void)
{
  char path[MAXPATH],target[MAXPATH];
  int fd, omode;
  struct file *f;
  struct inode *ip;
  int n,cycle;

  // 获取用户空间传入的参数
  if((n = argstr(0, path, MAXPATH)) < 0 || argint(1, &omode) < 0)
    return -1;
  // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
  begin_op();

  if(omode & O_CREATE)
  {
    // create函数会对返回的inode进行加锁
    ip = create(path, T_FILE, 0, 0);
    if(ip == 0){
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();
      return -1;
    }
  }
  else 
  {
    // 根据path找到对应的inode,找不到则报错
    if((ip = namei(path)) == 0){
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();
      return -1;
    }
    // namei函数不会对返回的inode进行加锁,因此这里需要锁定path的inode
    ilock(ip);
    // 不允许以非只读模式打开一个目录文件
    if(ip->type == T_DIR && omode != O_RDONLY){
      iunlockput(ip);
      // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
      end_op();
      return -1;
    }
  }

  // 设备文件的主设备号必须大于0并小于NDEV
  // 需要注意,运行到这里时,ip指向的inode已被锁定
  if(ip->type == T_DEVICE && (ip->major < 0 || ip->major >= NDEV)){
    iunlockput(ip);
    end_op();
    return -1;
  }

  // path的inode中type属性为T_SYMLINK
  if(ip->type == T_SYMLINK)
  { 
    // 若打开模式不是NOFOLLOW
    if(!(omode & O_NOFOLLOW))
    { 
      // 若当前ip指向的inode是属于符号链接文件且打开模式不是NOFOLLOW
      // 那么此时需要进行循环处理,循环次数超过10便返回-1
      while(ip->type == T_SYMLINK)
      { 
        // 最大循环次数为10
        if(cycle == 10){
            iunlockput(ip);
            end_op();
            return -1;
        }
        cycle ++;
        // 从ip指向的inode中复制路径到target中
        if(readi(ip, 0, (uint64)target, 0, MAXPATH) != MAXPATH)
        { 
          // 释放对path的inode的锁,并将其引用计数减1
          iunlockput(ip);
          end_op();
          return -1;
        }
        // 解锁当前ip指向的inode
        iunlockput(ip);

        // 根据目标路径获得对应的inode,ip指向该inode
        if((ip = namei(target)) == 0)
        {
          end_op();
          return -1;
        }
        // namei函数不会对返回的inode进行加锁,因此这里需要锁定新获得的inode
        ilock(ip);
      } // while
    
    } // if(!(omode & O_NOFOLLOW))
    
  } // if(ip->type == T_SYMLINK)

  // 调用filealloc函数申请分配一个file结构体,随后再继续调用
  // fdalloc函数将分配的file结构体存入当前进程的ofile数组中
  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
    if(f)
      fileclose(f);
    iunlockput(ip);
    end_op();
    return -1;
  }

  if(ip->type == T_DEVICE){
    f->type = FD_DEVICE;
    f->major = ip->major;
  }else {
    f->type = FD_INODE;
    f->off = 0;
  }
  f->ip = ip;
  f->readable = !(omode & O_WRONLY);
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);

  if((omode & O_TRUNC) && ip->type == T_FILE){
    itrunc(ip);
  }

  iunlock(ip);
  end_op();

  return fd;
}

// 创建目录文件
uint64 sys_mkdir(void)
{
  char path[MAXPATH];
  struct inode *ip;

  // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
  begin_op();
  // 获取用户传入的文件路径，随后调用create函数创建目录文件并返回inode
  if(argstr(0, path, MAXPATH) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
    // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
    end_op();
    return -1;
  }
  iunlockput(ip);
  // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
  end_op();
  return 0;
}

// 创建设备文件
uint64 sys_mknod(void)
{
  struct inode *ip;
  char path[MAXPATH];
  int major, minor;
  // 执行文件系统相关的系统调用时都需要开启事务，将正在执行的文件系统相关的系统调用数加一
  begin_op();
  // 获取用户传入的文件路径以及主次设备号，随后调用create函数创建设备文件并返回inode
  if((argstr(0, path, MAXPATH)) < 0 ||
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEVICE, major, minor)) == 0){
    // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
    end_op();
    return -1;
  }
  iunlockput(ip);
  // 将正在执行的文件系统相关的系统调用数减一，减完为零则进行提交(commit)操作
  end_op();
  return 0;
}

uint64 sys_chdir(void)
{
  char path[MAXPATH];
  struct inode *ip;
  struct proc *p = myproc();
  
  begin_op();
  if(argstr(0, path, MAXPATH) < 0 || (ip = namei(path)) == 0){
    end_op();
    return -1;
  }
  ilock(ip);
  if(ip->type != T_DIR){
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
  iput(p->cwd);
  end_op();
  p->cwd = ip;
  return 0;
}

uint64 sys_exec(void)
{
  char path[MAXPATH], *argv[MAXARG];
  int i;
  uint64 uargv, uarg;

  if(argstr(0, path, MAXPATH) < 0 || argaddr(1, &uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
    if(i >= NELEM(argv)){
      goto bad;
    }
    if(fetchaddr(uargv+sizeof(uint64)*i, (uint64*)&uarg) < 0){
      goto bad;
    }
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    argv[i] = kalloc();
    if(argv[i] == 0)
      goto bad;
    if(fetchstr(uarg, argv[i], PGSIZE) < 0)
      goto bad;
  }

  int ret = exec(path, argv);

  for(i = 0; i < NELEM(argv) && argv[i] != 0; i++)
    kfree(argv[i]);

  return ret;

 bad:
  for(i = 0; i < NELEM(argv) && argv[i] != 0; i++)
    kfree(argv[i]);
  return -1;
}

uint64 sys_pipe(void)
{ 
  // 用于存放应用程序传入的整型数组首地址
  uint64 fdarray; 
  // 读写文件结构体
  struct file *rf, *wf;
  // 读写文件结构体对应的文件描述符
  int fd0, fd1;
  // 获取当前进程的结构体
  struct proc *p = myproc();

  // 获取用户进程传入的整型数组首地址
  if(argaddr(0, &fdarray) < 0)
    return -1;
  // 通过pipealloc函数申请两个file结构体(分别用rf和wf指向), 并且创建一个pipe结构体
  // 随后将两个file结构体的分别设置为可读/可写, 且内部的pipe指针均指向新创建的pipe结构体
  if(pipealloc(&rf, &wf) < 0)
    return -1;

  fd0 = -1;
  // myproc()->ofile[fd0] = rf, myproc()->ofile[fd1] = wf
  // ofile[fd0]指向file结构体rf，而ofile[fd1]指向file结构体wf
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0)
  {
    if(fd0 >= 0)
      p->ofile[fd0] = 0;
    // 减少file结构体的引用计数, 当引用计数为0时则释放file结构体
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  // 将文件描述符返回到用户空间进程
  if(copyout(p->pagetable, fdarray, (char*)&fd0, sizeof(fd0)) < 0 ||
     copyout(p->pagetable, fdarray+sizeof(fd0), (char *)&fd1, sizeof(fd1)) < 0)
  {
    p->ofile[fd0] = 0;
    p->ofile[fd1] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  return 0;
}

uint64 sys_symlink(void)
{ 
  char target[MAXPATH],path[MAXPATH];
  struct inode *ip;
  struct buf *bp;
 
  // 从用户空间获得参数
  if(argstr(0, target, MAXPATH) < 0 || argstr(1, path, MAXPATH) < 0)
    return -1;

  // 开启事务
  begin_op();

  // 为路径path创建对应的inode,create返回的inode已被当前进程加锁
  if((ip = create(path, T_SYMLINK, 0, 0)) == 0 ){
    end_op();
    return -1;
  }

  // 无论target是否存在,都直接将路径target存放到路径path对应的数据块中
  if(writei(ip, 0, (uint64)target, 0, MAXPATH) != MAXPATH){
    iunlockput(ip);
    end_op();
    return -1;
  }

  iunlockput(ip);
  end_op();
  return 0;

}
