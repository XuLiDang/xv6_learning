#include "param.h"
#include "types.h"
#include "memlayout.h"
#include "elf.h"
#include "riscv.h"
#include "defs.h"
#include "fs.h"

/*
 * the kernel's page table.
 */
// pagetable_t是指向risc-v根页表页的指针
pagetable_t kernel_pagetable;

extern char etext[];  // kernel.ld sets this to end of kernel code.

extern char trampoline[]; // trampoline.S
/*
* kvm开头的函数用于操作内核页表,uvm开头的函数则用于操作用户页表
* 其他的函数则两者都适用
*/
// Make a direct-map page table for the kernel.
pagetable_t kvmmake(void)
{
  pagetable_t kpgtbl;

  // 申请一页内存用来存放根内核页表
  kpgtbl = (pagetable_t) kalloc();
  // 将该页内存的数据全部初始化为0
  memset(kpgtbl, 0, PGSIZE);

  // uart registers
  // 通过kvmmap建立内核所需要的地址转换映射,这里建立的是直接映射
  // void kvmmap(pagetable_t kpgtbl, uint64 va, uint64 pa, uint64 sz, int perm)
  kvmmap(kpgtbl, UART0, UART0, PGSIZE, PTE_R | PTE_W);

  // virtio mmio disk interface
  kvmmap(kpgtbl, VIRTIO0, VIRTIO0, PGSIZE, PTE_R | PTE_W);

  // PLIC
  kvmmap(kpgtbl, PLIC, PLIC, 0x400000, PTE_R | PTE_W);

  // map kernel text executable and read-only.
  kvmmap(kpgtbl, KERNBASE, KERNBASE, (uint64)etext-KERNBASE, PTE_R | PTE_X);

  // map kernel data and the physical RAM we'll make use of.
  // 执行完该函数之后,在内核页表中,虚拟地址空间(0x80000000-0x86400000)
  // 直接映射到RAM的物理地址空间(0x80000000-0x86400000)
  kvmmap(kpgtbl, (uint64)etext, (uint64)etext, PHYSTOP-(uint64)etext, PTE_R | PTE_W);

  // map the trampoline for trap entry/exit to
  // the highest virtual address in the kernel.
  // 在内核页表中建立起TRAMPOLINE(虚拟地址)到trampoline(物理地址)的映射
  kvmmap(kpgtbl, TRAMPOLINE, (uint64)trampoline, PGSIZE, PTE_R | PTE_X);

  // map kernel stacks
  // 为每个进程都分配一个内核栈,并且在内核页表中用不同的虚拟地址映射到对应的内核栈
  // 
  proc_mapstacks(kpgtbl);
  
  return kpgtbl;
}

// Initialize the one kernel_pagetable
void kvminit(void)
{
  kernel_pagetable = kvmmake();
}

// Switch h/w page table register to the kernel's page table,
// and enable paging.
void kvminithart()
{
  w_satp(MAKE_SATP(kernel_pagetable));
  // 刷新TLB
  sfence_vma();
}

// Return the address of the PTE in page table pagetable
// that corresponds to virtual address va.  If alloc!=0,
// create any required page-table pages.
//
// The risc-v Sv39 scheme has three levels of page-table
// pages. A page-table page contains 512 64-bit PTEs.
// A 64-bit virtual address is split into five fields:
//   39..63 -- must be zero.
//   30..38 -- 9 bits of level-2 index.
//   21..29 -- 9 bits of level-1 index.
//   12..20 -- 9 bits of level-0 index.
//    0..11 -- 12 bits of byte offset within the page.
// 返回逻辑地址va所对应的PTE的指针
// 一般都是对整个page做操作,很少有对某个特定地址进行操作,所以是获得该va所在page的PTE,主要操作包括对其标志位进行处理
pte_t * walk(pagetable_t pagetable, uint64 va, int alloc)
{ 
  // 虚拟地址必须小于VA
  if(va >= MAXVA)
    panic("walk");

  for(int level = 2; level > 0; level--) 
  {
    // PX(level, va)会返回第level级页表的索引值
    // 随后便可获得第level级页表中对应PTE的地址,用指针pte存放
    pte_t *pte = &pagetable[PX(level, va)];

    // 若之前找到的页表项合法
    if(*pte & PTE_V) 
    { 
      // PTE2PA会将PTE转换成实际的物理地址
      // 该物理地址为下一级页表页的起始地址
      // 在启用了分页硬件之后,这里获得的物理地址均会被CPU看作虚拟地址
      // 从而传入MMU进行转换,但在系统启动阶段早期并没有启用分页硬件
      // 并且在此期间,内核页表将虚拟地址空间(0x80000000-0x86400000)
      // 直接映射到RAM的物理地址空间(0x80000000-0x86400000)
      pagetable = (pagetable_t)PTE2PA(*pte);
    } 
    else 
    { 
      // 若找到的PTE无效,则说明下一级页表不存在,因此需要分配一个页面来创建并存放下一级页表
      // pagetable则指向新创建页表页的起始地址,alloc != 0 代表创建所需的页表页
      if(!alloc || (pagetable = (pde_t*)kalloc()) == 0)
        return 0;
      //用0填充新分配的页面
      memset(pagetable, 0, PGSIZE);
      // 新创建的页表一开始便有512个PTE,每个8字节,因此一个页表需要用一个4096B的页面存放
      // 但每个PTE中的PTE_V位均为0,即代表当前PTE无效
      // 创建完毕后,使pte指向下一级页表页的起始地址,同时将PTE_V置 1
      *pte = PA2PTE(pagetable) | PTE_V;
    }
  }
  // 返回最后一级页表中对应PTE的地址
  return &pagetable[PX(0, va)];
}

// Look up a virtual address, return the physical address,
// or 0 if not mapped.
// Can only be used to look up user pages.
uint64 walkaddr(pagetable_t pagetable, uint64 va)
{
  pte_t *pte;
  uint64 pa;

  if(va >= MAXVA)
    return 0;

  // 寻找虚拟地址a的最后一级页表中对应PTE的指针
  pte = walk(pagetable, va, 0);
  if(pte == 0)
    return 0;
  // PTE无效则直接返回
  if((*pte & PTE_V) == 0)
    return 0;
  // 用户模式下的代码无法访问则直接返回
  if((*pte & PTE_U) == 0)
    return 0;
  // 根据PTE获取物理地址
  pa = PTE2PA(*pte);
  return pa;
}

// add a mapping to the kernel page table.
// only used when booting.
// does not flush TLB or enable paging.
void kvmmap(pagetable_t kpgtbl, uint64 va, uint64 pa, uint64 sz, int perm)
{
  if(mappages(kpgtbl, va, sz, pa, perm) != 0)
    panic("kvmmap");
}

// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned. Returns 0 on success, -1 if walk() couldn't
// allocate a needed page-table page.
int mappages(pagetable_t pagetable, uint64 va, uint64 size, uint64 pa, int perm)
{
  uint64 a, last;
  pte_t *pte;

  if(size == 0)
    panic("mappages: size");
  // 令地址 va 向下对齐为PGSIZE的整数倍
  a = PGROUNDDOWN(va);
  // 令地址 va + size - 1 向下对齐为PGSIZE的整数倍
  last = PGROUNDDOWN(va + size - 1);
  for(;;)
  {
    // 寻找虚拟地址a的最后一级页表中对应PTE的指针
    // 第三个参数为1则代表如有需要,会请求分配一个页面用于存放新页表
    // 新创建的页表一开始便有512个PTE,每个8字节,因此一个页表需要用大小4096B的页面存放
    // 但该页面中每个PTE中的PTE_V位均为0,即代表当前PTE无效
    if((pte = walk(pagetable, a, 1)) == 0)
      return -1;
    // 若虚拟地址a对应PTE的PTE_V = 1,则代表已存有效的PTE
    if(*pte & PTE_V)
      panic("mappages: remap");
    // 使得虚拟地址a对应PTE指向的地址为pa,并且设置PTE_V = 1以及对应的权限
    *pte = PA2PTE(pa) | perm | PTE_V;
    // 判断a与last是否相同
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}

// Remove npages of mappings starting from va. va must be
// page-aligned. The mappings must exist.
// Optionally free the physical memory.
void uvmunmap(pagetable_t pagetable, uint64 va, uint64 npages, int do_free)
{
  uint64 a;
  pte_t *pte;

  if((va % PGSIZE) != 0)
    panic("uvmunmap: not aligned");

  for(a = va; a < va + npages*PGSIZE; a += PGSIZE){
    if((pte = walk(pagetable, a, 0)) == 0)
      panic("uvmunmap: walk");
    if((*pte & PTE_V) == 0)
      panic("uvmunmap: not mapped");
    if(PTE_FLAGS(*pte) == PTE_V)
      panic("uvmunmap: not a leaf");
    if(do_free){
      uint64 pa = PTE2PA(*pte);
      kfree((void*)pa);
    }
    *pte = 0;
  }
}

// create an empty user page table.
// returns 0 if out of memory.
pagetable_t uvmcreate()
{
  pagetable_t pagetable;
  pagetable = (pagetable_t) kalloc();
  if(pagetable == 0)
    return 0;
  memset(pagetable, 0, PGSIZE);
  return pagetable;
}

// Load the user initcode into address 0 of pagetable,
// for the very first process.
// sz must be less than a page.
void uvminit(pagetable_t pagetable, uchar *src, uint sz)
{
  char *mem;

  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pagetable, 0, PGSIZE, (uint64)mem, PTE_W|PTE_R|PTE_X|PTE_U);
  memmove(mem, src, sz);
}

// Allocate PTEs and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
uint64 uvmalloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz)
{
  char *mem;
  uint64 a;

  if(newsz < oldsz)
    return oldsz;

  oldsz = PGROUNDUP(oldsz);
  for(a = oldsz; a < newsz; a += PGSIZE){
    mem = kalloc();
    if(mem == 0){
      uvmdealloc(pagetable, a, oldsz);
      return 0;
    }
    memset(mem, 0, PGSIZE);
    if(mappages(pagetable, a, PGSIZE, (uint64)mem, PTE_W|PTE_X|PTE_R|PTE_U) != 0){
      kfree(mem);
      uvmdealloc(pagetable, a, oldsz);
      return 0;
    }
  }
  return newsz;
}

// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
uint64 uvmdealloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz)
{
  if(newsz >= oldsz)
    return oldsz;

  if(PGROUNDUP(newsz) < PGROUNDUP(oldsz)){
    int npages = (PGROUNDUP(oldsz) - PGROUNDUP(newsz)) / PGSIZE;
    uvmunmap(pagetable, PGROUNDUP(newsz), npages, 1);
  }

  return newsz;
}

// Recursively free page-table pages.
// All leaf mappings must already have been removed.
// typedef uint64 *pagetable_t
void freewalk(pagetable_t pagetable)
{
  // there are 2^9 = 512 PTEs in a page table.
  for(int i = 0; i < 512; i++){ 
    pte_t pte = pagetable[i];
    if((pte & PTE_V) && (pte & (PTE_R|PTE_W|PTE_X)) == 0){
      // this PTE points to a lower-level page table.
      uint64 child = PTE2PA(pte);
      // pagetable_t p; *p = child;
      freewalk((pagetable_t)child);
      pagetable[i] = 0;
    } else if(pte & PTE_V){
      panic("freewalk: leaf");
    }
  }
  kfree((void*)pagetable);
}

// Free user memory pages,
// then free page-table pages.
void uvmfree(pagetable_t pagetable, uint64 sz)
{
  if(sz > 0)
    uvmunmap(pagetable, 0, PGROUNDUP(sz)/PGSIZE, 1);
  freewalk(pagetable);
}

// Given a parent process's page table, copy
// its memory into a child's page table.
// Copies both the page table and the
// physical memory.
// returns 0 on success, -1 on failure.
// frees any allocated pages on failure.
int uvmcopy(pagetable_t old, pagetable_t new, uint64 sz)
{
  pte_t *pte;
  uint64 pa, i;
  uint flags;
  char *mem;

  for(i = 0; i < sz; i += PGSIZE){
    // 寻找父进程虚拟地址i对应的PTE, 并用指针变量pte存放PTE的地址
    if((pte = walk(old, i, 0)) == 0)
      panic("uvmcopy: pte should exist");
    // PTE不存在则报错
    if((*pte & PTE_V) == 0)
      panic("uvmcopy: page not present");
    // 从PTE中提取物理页的物理基地址
    pa = PTE2PA(*pte);
    // 从父进程页表中获取父进程对该页的权限
    flags = PTE_FLAGS(*pte);
    // 申请分配一页内存
    if((mem = kalloc()) == 0)
      goto err;
    // 将属于父进程的物理页的数据复制到新分配的物理页中
    memmove(mem, (char*)pa, PGSIZE);
    // 将新分配的物理页映射到子进程的页表中
    // 即在子进程用户页表中, 将虚拟地址i映射到物理地址mem
    if(mappages(new, i, PGSIZE, (uint64)mem, flags) != 0){
      kfree(mem);
      goto err;
    }
  }
  return 0;

 err:
  uvmunmap(new, 0, i / PGSIZE, 1);
  return -1;
}

// mark a PTE invalid for user access.
// used by exec for the user stack guard page.
void uvmclear(pagetable_t pagetable, uint64 va)
{
  pte_t *pte;
  
  pte = walk(pagetable, va, 0);
  if(pte == 0)
    panic("uvmclear");
  *pte &= ~PTE_U;
}

// Copy from kernel to user.
// Copy len bytes from src to virtual address dstva in a given page table.
// Return 0 on success, -1 on error.
int copyout(pagetable_t pagetable, uint64 dstva, char *src, uint64 len)
{
  uint64 n, va0, pa0;

  while(len > 0){
    va0 = PGROUNDDOWN(dstva);
    pa0 = walkaddr(pagetable, va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (dstva - va0);
    if(n > len)
      n = len;
    memmove((void *)(pa0 + (dstva - va0)), src, n);

    len -= n;
    src += n;
    dstva = va0 + PGSIZE;
  }
  return 0;
}

// Copy from user to kernel.
// Copy len bytes to dst from virtual address srcva in a given page table.
// Return 0 on success, -1 on error.
int copyin(pagetable_t pagetable, char *dst, uint64 srcva, uint64 len)
{
  uint64 n, va0, pa0;

  while(len > 0){
    va0 = PGROUNDDOWN(srcva);
    pa0 = walkaddr(pagetable, va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (srcva - va0);
    if(n > len)
      n = len;
    memmove(dst, (void *)(pa0 + (srcva - va0)), n);

    len -= n;
    dst += n;
    srcva = va0 + PGSIZE;
  }
  return 0;
}

// Copy a null-terminated string from user to kernel.
// Copy bytes to dst from virtual address srcva in a given page table,
// until a '\0', or max.
// Return 0 on success, -1 on error.
int copyinstr(pagetable_t pagetable, char *dst, uint64 srcva, uint64 max)
{
  uint64 n, va0, pa0;
  int got_null = 0;

  while(got_null == 0 && max > 0){
    va0 = PGROUNDDOWN(srcva);
    pa0 = walkaddr(pagetable, va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (srcva - va0);
    if(n > max)
      n = max;

    char *p = (char *) (pa0 + (srcva - va0));
    while(n > 0){
      if(*p == '\0'){
        *dst = '\0';
        got_null = 1;
        break;
      } else {
        *dst = *p;
      }
      --n;
      --max;
      p++;
      dst++;
    }

    srcva = va0 + PGSIZE;
  }
  if(got_null){
    return 0;
  } else {
    return -1;
  }
}

void vmprint(pagetable_t pagetable,int level)
{
  // there are 2^9 = 512 PTEs in a page table.
  for(int i = 0; i < 512; i++)
  {
    pte_t pte = pagetable[i];
    if(pte & PTE_V)
    { 
      switch(level)
      {
        case 0:
          printf(".. .. .. %d: pte %p pa %p\n",i,pte,PTE2PA(pte));
          break;
        case 1:
          printf(".. .. %d: pte %p pa %p\n",i,pte,PTE2PA(pte));
          break;
        case 2:
          printf(".. %d: pte %p pa %p\n",i,pte,PTE2PA(pte));
          break;
      }

      // this PTE points to a lower-level page table.
      uint64 child = PTE2PA(pte);
      if(level > 0 )
        vmprint((pagetable_t)child,level-1);
    } 
  
  }
}
