#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "proc.h"
#include "sysinfo.h" // struct sysinfo

uint64
sys_exit(void)
{
  int n;
  if(argint(0, &n) < 0)
    return -1;
  exit(n);
  return 0;  // not reached
}

uint64
sys_getpid(void)
{
  return myproc()->pid;
}

uint64
sys_fork(void)
{
  return fork();
}

uint64
sys_wait(void)
{
  uint64 p;
  if(argaddr(0, &p) < 0)
    return -1;
  return wait(p);
}

uint64
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

// sleep系统调用对应实现函数
uint64 sys_sleep(void)
{
  int n;
  // unsigned int
  uint ticks0;

  // 获取整型的系统调用参数,并赋值给n
  if(argint(0, &n) < 0)
    return -1;

  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    backtrace();
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

uint64
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

// return how many clock tick interrupts have occurred
// since start.
uint64
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

uint64 sys_trace(void)
{
  int mask;
  struct proc *p = myproc();

  if(argint(0, &mask) < 0)
    return -1;

  p->mask = mask;

  return 0; 
}

uint64 sys_info(void)
{
  uint64 u_addr;
  struct proc *p = myproc();
  struct sysinfo si;

  if(argaddr(0, &u_addr) < 0)
    return -1;

  si.freemem = collect_freemem_nr();
  si.nproc = collect_proc_nr();
  
  if (copyout(p->pagetable, u_addr, (char *)&si, sizeof(si)) < 0)
    return -1;

  return 0;

}

int sys_pgaccess(void)
{ 
  // base用于存放用户进程传进来的虚拟地址
  // u_mask则是用户空间的缓冲区，用于存放内核传递给用户空间的数据
  uint64 base,u_mask;
  // len代表需要检查的页数
  int len;
  // abits是内核要传递给用户空间的数据，代表用户进程传递给该函数进行检查
  // 的所有页面中，哪些页已被访问，例如000....1代表第一个页面已被访问
  unsigned int abits = 0;
  // 获取当前进程
  struct proc *p = myproc();
  // pte_p为指向PTE的指针
  pte_t *pte_p;

  // 获取用户进程传递的参数
  if(argaddr(0, &base) < 0) 
    return -1;  
  if(argint(1, &len) < 0)
    return -1;
  if(argaddr(2, &u_mask) < 0)
    return -1;

  // 循环检查从虚拟地址base初始值开始的len个页面
  // 每结束一轮循环，base += PGSIZE
  for(int i = 0; i < len; i++)
  { 
    // base的值不能大于或等于MAXVA
    if(base >= MAXVA)
      return -1; 

    // 找到逻辑地址base所对应的PTE的指针
    pte_p = walk(p->pagetable,base,0);
    // 判断以base为首地址的页是否已被访问，主要通过判断
    // 其对应PTE中的访问位是否已被置位(特指最后一级页表的PTE)
    if(*pte_p & PTE_A)
    { 
      // 在abits中记录访问位已被置位的页
      // 例：abits | (1 << 0) 则代表第一个页的访问位已被置位
      abits = abits | (1 << i);
      // 清除其对应PTE中的访问位
      // ~PTE_A = 1...011111
      *pte_p = *pte_p & (~PTE_A);
    }
    // 检查下一个页面
    base += PGSIZE;
  }

  // 将abits传递给用户空间，通过首地址为u_mask的缓冲区来接收
  if (copyout(p->pagetable, u_mask, (char *)&abits, sizeof(unsigned int)) < 0)
    return -1;
  
  return 0;
}

uint64 sys_sigalarm(void)
{
  int interval;
  uint64 handler_addr;
  // 获取当前进程
  struct proc *p = myproc();

  // 获取用户进程传递的参数
  if(argint(0, &interval) < 0) 
    return -1;  
  if(argaddr(1, &handler_addr) < 0)
    return -1;

  if(interval == 0)
    return -1;

  p->handler_addr = handler_addr;
  p->interval = interval;

  return 0;
}

uint64 sys_sigreturn(void)
{ 
  // 获取当前进程
  struct proc *p = myproc();

  if(p->alarm == 0)
    return -1;

  // 恢复执行alarm处理程序前trapframe页中的内容
  memmove(p->trapframe,p->alarm,sizeof(struct trapframe));
  // 代表alarm处理程序已结束
  p->alarm_flag = 0;
  
  return 0;
}