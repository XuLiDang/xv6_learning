#include "user/user.h"

void print_primes(int *p_prev)
{	
	// 缓冲区
	int n,buf;
	// 用于存放当前进程所创建的管道文件的读写描述符,会作为参数传送给下一个进程(子进程)
	int p_next[2];
	// 创建管道文件,并将p_next[0]作为读描述符,p_next[1]作为写描述符
	pipe(p_next);
	// 释放指向由上一个进程(左邻居,也是父进程)所创建的管道文件写端的文件描述符
	close(p_prev[1]);

	// 从上一个进程(左邻居,也是父进程)所创建的管道文件中读取第一个数据
	// 若没有数据则会阻塞
	if (read(p_prev[0], &n, sizeof(int)))
	{	
		// 将读取到的数据打印出来
		printf("prime %d\n", n);

		if(fork() == 0)
		{	
			print_primes(p_next);
			exit(0);
		}
		else
		{	
			// 释放指向当前进程创建的管道文件读端的文件描述符
			close(p_next[0]);
			// 下面几行代码的含义是：1.依次从上一个进程创建的管道文件中读取数据
			// 2. 分别将这些数据除以第一个读取到的数据(用n来存放)
			// 3. 若相除不为0则将其写入到当前进程所创建的管道文件中
			// 4. 而下一个进程会使用到当前进程所创建的管道文件
			while(read(p_prev[0], &buf, sizeof(int)))
			{
				if( buf % n != 0)
					write(p_next[1], &buf, sizeof(int));
			}

			// 释放指向当前进程创建的管道文件写端的文件描述符
			close(p_next[1]);
			// 释放指向上一个进程创建的管道文件读端的文件描述符
			// 至此,指向上一个进程所创建的管道文件的文件描述符已全部释放
			close(p_prev[0]);
			// 等待子进程退出
			wait(0);
		}
	}

}


int main(int argc, char *argv[])
{	
	// 用于存放管道文件的读写描述符
	int p[2];
	// 创建管道文件,并将p[0]作为读描述符,p[1]作为写描述符
	pipe(p);

	if(fork() == 0)
	{	print_primes(p);
		exit(0);
	}
	// 父进程,即第一个进程,则需要将1-35的数据输入到管道中
	else
	{	// 释放指向管道读端的描述符,因为第一个进程无需使用
		close(p[0]);
		// 向管道写入数据
		for(int i = 2; i <= 35; i++)
			write(p[1],&i,sizeof(int));
		// 释放指向管道写端的描述符
		close(p[1]);
		// 等待子进程退出
		wait(0);
	}
	
	exit(0);
}