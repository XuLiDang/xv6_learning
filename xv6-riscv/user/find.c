#include "user/user.h" 
#include "kernel/fs.h" // struct dirent
#include "kernel/stat.h" // struct stat

void find(char *dir_path, char *file_name)
{
 	int fd;
 	char buf[512],*p;
 	struct stat st;
 	struct dirent de;

 	// 根据路径名打开对应的文件
  if((fd = open(dir_path, 0)) < 0)
  {
    printf("find: cannot open %s\n", dir_path);
    return;
  }

  // fstat能够从文件描述符所指向的inode结点中索取信息
  // 并将这些信息存储在stat结构体中,该结构体定义在kernel/stat.h
  if(fstat(fd, &st) < 0)
  {
    printf("find: cannot stat %s\n", dir_path);
    close(fd);
    return;
  }

  // dir_path并不是目录文件路径则输出错误信息
  if(st.type != T_DIR )
  {
  	printf("%s is not a dir\n",dir_path);
    return;
  }
 	
 	// 目录文件路径不能超过系统规定的长度
  if(strlen(dir_path) + 1 + DIRSIZ + 1 > sizeof(buf))
  {
     printf("find: dir_path too long\n");
     return;
  }

  // 将目录文件路径名拷贝到buf缓冲区
  strcpy(buf, dir_path);
  // strlen(buf)计算出buf缓冲区有多少个数据,这里buf代表的是buf缓冲区的首地址
  // 因此此时p指向的是buf的第一个空项。例如,buf中有两个数据,strlen(buf)返回值为2
  // 且分别用buf[0]和buf[1]来存储,而此时p指向的则是buf[2]的地址
  p = buf + strlen(buf);
  //*p = '/', p++
  // 接上面的例子,这行代码的作用是：buf[2] = '/',随后p指向buf[3]的地址
  // 因此p仍然指向buf缓冲区的第一个空项
  *p++ = '/';

	// 读取目录文件的信息,此类文件的每项内容可用dirent(目录项)结构体来存放
	// dirent结构体的内容包括文件名以及对应的索引结点号
	while(read(fd, &de, sizeof(de)) == sizeof(de))
	{ 
		// 当前目录项的索引结点号为0或文件名为"."以及".."时,则直接跳过,处理下一项
		if(de.inum == 0 || !strcmp(de.name,".") || !strcmp(de.name,".."))
	    continue;

	  // 将de.name[DIRSIZ]中存储的文件名复制到p所指向的地址处,de.name指向这个数组的首地址
	  // 注:复制完之后,p所指向的地址是没有改变的,具体可查看memmove的实现
	  memmove(p, de.name, DIRSIZ);
	  p[DIRSIZ] = 0;

	  // stat是对fstat进行了一层封装
	  // 注意,在从p所指的地址开始进行赋值后,此时这里的buf的内容就等于
	  // while循环开始前buf缓冲区的内容加上while循环中所追加的内容
	  if(stat(buf, &st) < 0)
	  {
	  	printf("find: cannot stat %s\n", buf);
      continue;
	  }

	  switch(st.type)
	  {
	  	case T_FILE:
	    	if(!strcmp(de.name,file_name))
	    		printf("%s\n",buf);
	    	break;
	    case T_DIR:
	    	find(buf,file_name);
	    	break;
	  }

	}
	 
}

int main(int argc, char *argv[])
{
	if(argc != 3)
	{
		printf("usage: find dir_name filename\n");
		exit(0);
	}
	
	find(argv[1],argv[2]);
	exit(0);
}

