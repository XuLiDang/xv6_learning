#include "user/user.h"
#include "kernel/param.h" // MAXARG

void get_input(char **args,char *buf,int commandLen_old)
{	
	// 临时缓冲区用于存放从buf缓冲区筛选出来的数据
	char tempBuf[MAXARG]; 
	// 临时缓冲区的长度不能超过MAXARG
  	int tempBufSize = 0;  
  	// commandLen_old为args原始的长度,即没有获取进程输出之前的长度
  	// commandLen_new则为获取了获取进程输出之后的长度
  	int commandLen_new = commandLen_old;
  	// input_size用于记录从上一条命令中读取到了多少数据
  	int input_size;

  	// 一次read能读取的数据有限,因此需要多次调用read才能避免丢失数据
  	while((input_size = read(0, buf, sizeof(buf))) > 0) 
  	{
		for(int i = 0; i < input_size; i++)
		{
			if(buf[i] == '\n')
			{	
				tempBuf[tempBufSize] = 0;
	        	args[commandLen_new++] =  tempBuf;
	        	args[commandLen_new] = 0;

	        	// 创建子进程
	         	if(fork() == 0)
				{
					// 注意exec接收的二维参数数组argv，第一个参数argv[0]必须是该命令本身
					// 最后一个参数argv[size-1]必须为0，否则将执行失败。
					exec(args[0],args);
					exit(0);
				}
				// 父进程
				else
				{
					wait(0);
					commandLen_new = commandLen_old; // 重置参数个数
					tempBufSize = 0;
				}
			}
			else if(buf[i] == '\\' && buf[i+1] == 'n')
			{	
				i++;
				tempBuf[tempBufSize] = 0;
	        	args[commandLen_new++] =  tempBuf;
	        	args[commandLen_new] = 0;
	        	
	        	// 创建子进程
	         	if(fork() == 0)
				{
					// 注意exec接收的二维参数数组argv，第一个参数argv[0]必须是该命令本身
					// 最后一个参数argv[size-1]必须为0，否则将执行失败。
					exec(args[0],args);
					exit(0);
				}
				// 父进程
				else
				{
					wait(0);
					commandLen_new = commandLen_old; // 重置参数个数
					tempBufSize = 0;
				}
			}
			//	遇到空格要进行特殊处理
			else if(buf[i] == ' ')				
				tempBuf[tempBufSize++] =*(" ");			
			else
				tempBuf[tempBufSize++] = buf[i];	

		} // for
	}// while
}

int main(int argc, char *argv[])
{	
	int commandLen = 0;  //参数个数，值不能超过MAXARG
	// buf缓冲区用于从标准输入中获取管道左边进程传入的参数(详情可看user/sh.c中处理pipe的那部分代码)
	// args缓冲区则将所有有关的参数整合起来,最后传入到exec函数中
	char *args[MAXARG],buf[MAXARG];

	if(argc < 2)
	{
		printf("usage: command | xargs command\n");
		exit(0);
	}

	/* 若xargs后面有-n参数,则需要进行特殊处理*/
	if (!strcmp(argv[1], "-n")) // xargs -n 1 ...
	{  	
    	// argv[2] = 1,因此需要从argv[3]开始将n个参数复制到以args[0]开始的数组中 
    	for(int i = 3; i < argc; i++)
    		args[i-3] = argv[i];
    	
    	commandLen = argc - 3;
    	get_input(args,buf,commandLen);

	}
	
	/* 若xargs后面没有-n参数,则不用特殊处理*/
	else
	{
		// 将传入到该程序的所有参数均复制到args缓冲区中	
		for (int i = 1; i < argc; i++)
    		args[i-1] = argv[i];

		commandLen = argc-1;
		get_input(args,buf,commandLen);		
	}

	exit(0);
}	