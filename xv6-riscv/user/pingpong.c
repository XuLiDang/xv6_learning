#include "user/user.h"

int main(int argc, char *argv[])
{	
	// 用于存放管理文件的读写描述符
	int p[2];
	// 用于判断父进程与子进程
	int pid;
	// 缓冲区
	char buf;
	// 创建管道文件,并将p[0]作为读描述符,p[1]作为写描述符
	pipe(p);
	// 父进程先向管道文件写入数据
	write(p[1],&buf,1);

	pid = fork();
	// 子进程
	if(pid == 0)
	{
		read(p[0],&buf,1);
		printf("%d: received ping\n",getpid());
		write(p[1],&buf,1);
		close(p[0]);
		close(p[1]);
		exit(0);
	}
	// 父进程
	else if(pid > 0)
	{	
		// 等待子进程结束
		wait(0);
		read(p[0],&buf,1);
		printf("%d: received pong\n",getpid());
		close(p[0]);
		close(p[1]);
		exit(0);
	}
	// fork出错
	else
	{
		printf("fork error!");
		exit(1);
	}
	
}