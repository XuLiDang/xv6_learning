#include "user/user.h"

int main(int argc, char *argv[])
{	
	int time;

	// 以在shell中输入命令：sleep 10为例
	// 此时会shell会认为argc = 2, 并且argv[0] = sleep, argv[1] = 10
	if(argc != 2){
		fprintf(2, "error argument!\n");
		exit(1);
	}

	time = atoi(argv[1]);

	// 这里的sleep只是一个库函数,在user/user.h中声明
	sleep(time);
	exit(0);
}
