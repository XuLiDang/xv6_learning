#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"
#include "kernel/fs.h"

// 提取路径当中的文件名,并将其进行格式化
char* fmtname(char *path)
{
  static char buf[DIRSIZ+1];
  char *p;

  // p一开始指向的是path存有的数据的末端,这里path代表的是首地址
  // 此次循环的作用是将路径中最末尾的文件名提取出来,例如:路径`a/b/abc`
  // 那么循环完成之后,p指向的是'abc'中的首地址
  for(p=path+strlen(path); p >= path && *p != '/'; p--)
    ;
  p++;

  // Return blank-padded name.
  // 如果从p所指地址处开始的数据量大于或等于前面创建的buf数组的容量,此时直接返回p
  if(strlen(p) >= DIRSIZ)
    return p;
  // 否则首先将p所指地址处开始的全部数据复制到buf数组中
  memmove(buf, p, strlen(p));
  // buf数组中空出来的部分用' '填充,这样就能统一显示出来的文件名占用空间
  memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  return buf;
}

void ls(char *path)
{
  char buf[512], *p;
  int fd;
  struct dirent de;
  struct stat st;

  // 根据路径名打开对应的文件
  if((fd = open(path, 0)) < 0){
    fprintf(2, "ls: cannot open %s\n", path);
    return;
  }

  // fstat能够从文件描述符所指向的inode结点中索取信息
  // 并将这些信息存储在stat结构体中,该结构体定义在kernel/stat.h
  if(fstat(fd, &st) < 0){
    fprintf(2, "ls: cannot stat %s\n", path);
    close(fd);
    return;
  }

  // 根据文件的类型做相应的处理
  switch(st.type)
  {

  // 普通文件则直接输出相关信息
  case T_FILE:
    printf("%s %d %d %l\n", fmtname(path), st.type, st.ino, st.size);
    break;

  case T_DIR:
    if(strlen(path) + 1 + DIRSIZ + 1 > sizeof(buf))
    {
      printf("ls: path too long\n");
      break;
    }
    // 将路径名拷贝到buf缓冲区中
    strcpy(buf, path);
    // strlen(buf)计算出buf缓冲区有多少个char类型的数据
    // p此时指向buffer缓冲区存有的数据的末端,这里buf代表的是buf缓冲区的首地址
    p = buf+strlen(buf);
    // 在缓冲区保存的路径名后面加上一个`/`
    *p++ = '/';

    // 读取目录文件的信息,此类文件的每项内容可用dirent(目录项)结构体来存放
    // dirent结构体的内容包括文件名以及对应的索引结点号
    while(read(fd, &de, sizeof(de)) == sizeof(de))
    { 
      // 当前目录项的索引结点号为0则跳过,继续处理下一个
      if(de.inum == 0)
        continue;
      // 将de.name中存储的文件名复制到p所指向的地址处
      // 注:复制完之后,p所指向的地址是没有改变的,具体可查看memmove的实现
      memmove(p, de.name, DIRSIZ);
      p[DIRSIZ] = 0;

      // stat是对fstat进行了一层封装
      // 注意,在从p所指的地址开始进行赋值后,此时这里的buf的内容就等于
      // while循环开始前buf缓冲区的内容加上while循环中所追加的内容
      if(stat(buf, &st) < 0)
      {
        printf("ls: cannot stat %s\n", buf);
        continue;
      }
      printf("%s %d %d %d\n", fmtname(buf), st.type, st.ino, st.size);
    }
    break;
  }

  close(fd);
}

int main(int argc, char *argv[])
{
  int i;

  // 参数个数小于2则代表查看当前目录的文件,即 ls
  if(argc < 2){
    ls(".");
    exit(0);
  }
  for(i=1; i<argc; i++)
    ls(argv[i]);
  exit(0);
}
