# Lab: networking
- 简单地说，该实验要完成网络驱动中接收与发送packet的两个函数`e1000_recv`与`e1000_transmit`。而驱动初始化与paket处理等实现代码已给出。

**1.** `e1000_transmit`实现函数

```
int e1000_transmit(struct mbuf *m)
{ 
  acquire(&e1000_lock);
  // 获得用于传输下一个packet的描述符索引值index
  // index同时也是传输描述符对应mbuf的索引值
  int index = regs[E1000_TDT];
  // 判断由索引值index索引的传输描述符是否设置了E1000_TXD_STAT_DD标志
  if((tx_ring[index].status & E1000_TXD_STAT_DD) == 0)
  {
    printf("e1000_transmit: status unequals E1000_TXD_STAT_DD!\n");
    release(&e1000_lock);
    return -1;
  }

  if(tx_mbufs[index] != 0)
    // 释放上一个通过该描述符进行传输的mbuf
    mbuffree(tx_mbufs[index]);

  // 更新tx_mbufs[index]，以便重用该描述符时释放对应的mbuf
  tx_mbufs[index] = m;
  // 填充描述符
  tx_ring[index].addr = (uint64) m->head;
  tx_ring[index].length = m->len;
  // 设置cmd标志，有关cmd标志的内容可查看官网给出的intel文档中的3.3.3.1章节
  tx_ring[index].cmd = E1000_TXD_CMD_EOP | E1000_TXD_CMD_RS;
  // 更新寄存器E1000_TDT的值
  regs[E1000_TDT] = (index + 1) % TX_RING_SIZE;

  release(&e1000_lock);
  return 0;
}
```
网络栈(`net.c`文件中的函数)会调用该函数，从而将packet发送出去。该函数的整体逻辑为：
- 申请锁，防止多个进程并发调用该函数。

- 根据`E1000_TDT`寄存器的值，获得用于传输下一个packet的描述符索引值`index`，同时`index`也是对应`mbuf`的索引值。

- 判断由索引值`index`索引的传输描述符是否设置了`E1000_TXD_STAT_DD`标志(该标志表明传输描述符上次的传输已完成)，否则返回-1。

- 判断由`index`索引的传输描述符对应的`mbuf`是否为空，为空则代表该描述符还未使用过，因此可以直接往下执行；不为空则代表该`mbuf`用于传输之前的`pakcet`，且传输已完成，因此需要释放该传输描述符对应的`mbuf`

- 最后将网络栈传下来的`mbuf`与由`index`索引的传输描述符关联起来，并设置对应的`cmd`标志以及更新寄存器`E1000_TDT`的值，最后释放锁。

---


**2.** `e1000_recv`实现函数

```
static void e1000_recv(void)
{
  int index;
  struct mbuf *m;

  while(1)
  { 
    //acquire(&e1000_lock);
    
    // 获取描述下一个等待接收packet的接收描述符的索引值
    index = (regs[E1000_RDT] + 1) % RX_RING_SIZE;
    
    // 判断接收描述符是否设置了E1000_TXD_STAT_DD标志
    if((rx_ring[index].status & E1000_RXD_STAT_DD) == 0)
      break;
    
    // 更新对应mbuf结构体中的len属性 
    rx_mbufs[index]->len = rx_ring[index].length;
    // 用指针变量m存放指向mbuf的指针
    m = rx_mbufs[index];

    // 申请分配新的mbuf
    rx_mbufs[index] = mbufalloc(0);
    if (!rx_mbufs[index])
      panic("mbuf alloc failed");

    // 将新分配的mbuf连接到当前接收描述符
    rx_ring[index].addr = (uint64)rx_mbufs[index]->head;
    // 将当前接收描述符状态设置为0
    rx_ring[index].status = 0;
    // 更新E1000_RDT寄存器的值
    regs[E1000_RDT] = index;
    
    //release(&e1000_lock);

    // 通过net_rx函数将对应的mbuf交由网络栈进行处理
    net_rx(m);
  }
}
```
- 当`E1000`网卡将接收到的数据传输到内存中的缓冲区后(每个接收描述符对应的`mbuf`)，便会生成一个中断，请求进一步的处理。接收到该中断后，系统便会调用`e1000_intr`函数(`trap.c`文件中定义了对该中断的处理流程)，而该函数则会调用我们所实现的`e1000_recv`函数。

- `e1000_recv`函数的作用是一次性获取所有`E1000_TXD_STAT_DD`标志位不为0的接收描述符，然后调用`net_rx`函数将接收描述符对应的`mbuf`发给上一层，

- `net_rx`函数收到包后会调用`net_rx_ip`或`net_rx_arp`函数进入网络层，随后`net_rx_ip`函数会调用`net_rx_udp`函数进入传输层，而net_rx_udp函数会调用`sockrecvudp`，再由socket机制统一根据端口把包分给各个进程。

- 实现代码中有一个遗留的问题，即原先代码的思路是：在调用`net_rx()`函数之前利用自旋锁，从而防止对`rx_ring`，`rx_mbufs`以及`regs[E1000_RDT]`的并发访问。但这样会导致调用`nettests`测试时出现会卡死的现象，而去掉锁后便能顺利地通过测试。

---

**3.** 修改`nettests.c`文件
- 测试程序中用的dns服务器是谷歌的域名服务器8.8.8.8，由于众所周知的原因，不修改域名服务器的地址就会卡死。因此我们需要找到`nettests.c`文件中的`dns()`函数，并将域名服务器修改为国内的dns服务器(例：114.114.114.114)

```
dst = (114 << 24) | (114 << 16) | (114 << 8) | (114 << 0);
```

