# 1.Implement copy-on write
**大致实现思路：**
- 当父进程调用`fork`函数创建子进程时，`fork`函数会通过调用`uvmcopy`函数为子进程创建与父进程同样大小的内存空间，并将父进程内存空间的内容赋值过去。而在实现COW机制则需要修改`uvmcopy`函数，使得父子进程共享同一内存空间，并在父子进程的页表中，将该内存空间中各页面的权限修改为不可写的COW页(原本不可写或用户不可访问的页则无需修改)。另外，可采用PTE中的第8或第9位来表示页面的COW标志。

- 当共享同一内存空间父或子进程需要修改内存内容时，其肯定会向一个不可写的页面写入数据，而这时便会触发写缺页异常(exception code = 15)。因此，我们需要实现一个COW缺页异常处理函数，该函数首先会判断引发写异常的页面是否为COW页。若不是，则杀死当前进程，并返回错误信息；若是，则创建一个新的页面，随后将引发写异常的页面的数据复制到新页面中，将新页面权限修改为可写的非COW页。并在引发写异常的进程页表中，解除出错地址与旧页面的映射关系，创建出错地址与新页面的映射关系，最后释放旧页面。

- 在`copyout`函数中，会将内核中的数据复制到进程虚拟地址对应的物理页面中。如果在进程页表，该页面的权限被标记为不可写，那么此时也会触发写缺页异常。因此，我们也修改`copyout`函数的代码来处理这种情况，处理方式与第二点所提及的方法的一致。

- 最后，我们还需要记录内存中的每个页面的"引用计数"。即当页面被共享时，该页面的引用计数需要加1，而页面被其中一个进程释放时，其引用计数需要减1，并且只有当页面的引用计数为0时才可以真正的释放该页面。此外，还需要使用锁来处理并发访问时的情况

**实现代码**：

**1.** 在`vm.c`文件中修改`uvmcopy`和`copyout`函数，代码如下：

**uvmcopy：**
```
int uvmcopy(pagetable_t old, pagetable_t new, uint64 sz)
{
  pte_t *pte;
  uint64 pa, i;
  uint flags;

  for(i = 0; i < sz; i += PGSIZE)
  {
    // 寻找指向父进程虚拟地址i对应的PTE的指针
    if((pte = walk(old, i, 0)) == 0)
    {
      printf("uvmcopy: pte should exist\n");
      return -1; 
    }
    // 虚拟地址i对应PTE不存在则报错
    if((*pte & PTE_V) == 0)
    {
      printf("uvmcopy: page not present");
      return -1;
    }

    /*------------------------------------------------------------*/
    /*下面的代码使得父子进程共享内存空间,并将符合要求的页面设置为不可写*/
    /*------------------------------------------------------------*/

    // 从PTE中获得父进程虚拟地址i对应物理页的基地址
    pa = PTE2PA(*pte);

    // 仅修改设置了PTE_U和PTE_W标志的页,其他的维持不变
    if((*pte & PTE_U) && (*pte & PTE_W))
    {
      /* clear PTE_W */
      *pte &= (~PTE_W);
      /* set PTE_COW */
      *pte |= PTE_COW;
    }
    
    // 子进程对当前页面的权限与父进程一致
    flags = PTE_FLAGS(*pte);
    
    // 在子进程用户页表中, 将虚拟地址i映射到物理地址pa
    // 这样一来, 父子进程便可以共享同一块只读内存空间
    if(mappages(new, i, PGSIZE, pa, flags) != 0){  
      goto err; 
    }
    // 增加共享页的引用计数
    refcnt_incr(pa, 1);  
  }
  return 0;

 err:
  // 在new所指向的页表中, 从虚拟地址0开始, 移除i / PGSIZE个页面的映射
  // 同时释放对应的物理页
  uvmunmap(new, 0, i / PGSIZE, 1);
  return -1;
}
```

**copyout：**

```
int copyout(pagetable_t pagetable, uint64 dstva, char *src, uint64 len)
{ 
  pte_t *pte;
  uint64 n, va, pa;

  while(len > 0)
  { 
    // dstva必须小于最大虚拟地址
    if (dstva >= MAXVA )
    { 
      printf("copyout: wrong address\n");
      return -1;
    }
    // 向下取整dstva,va是页面对齐的虚拟地址
    va = PGROUNDDOWN(dstva);

    // 寻找指向虚拟地址va对应PTE的指针
    if((pte = walk(pagetable,va,0)) == 0)
    {
      printf("copyout: pte should exist\n");
      return -1; 
    }

    // 调用cow_fork_handler函数处理COW页
    if(*pte & PTE_COW)
    { 
      if(cow_fork_handler(pagetable,va) == -1){
        return -1;
      }
    }
    
    // 获取虚拟地址va对应的物理地址
    if((pa = walkaddr(pagetable, va)) == 0){
      return -1;
    }

    // n代表每次循环需要传输的字节数,而len则是剩余传输字节数
    n = PGSIZE - (dstva - va);
    if(n > len)
      n = len;
    // 传输n个字节的数据
    memmove((void *)(pa + (dstva - va)), src, n);

    // 更新剩余传输字节数,源地址以及目的地址(虚拟地址)
    len -= n;
    src += n;
    dstva = va + PGSIZE;
  }

  return 0;
}

```
**2.** 在`kalloc.c`文件中添加页面引用计数相关结构体与函数，缺页异常处理函数`cow_fork_handler`，并修改`kinit`，`kalloc`以及`kfree`函数，：

**页面引用计数相关结构体与函数：**

```
// 页面引用计数结构体
struct refcnt 
{ 
  // 自旋锁,用于处理并发访问
  struct spinlock lock;
  // 页面引用计数数组
  uint counter[(PHYSTOP - KERNBASE) / PGSIZE];
};

// 页面引用计数初始化函数
void refcnt_init()
{
  initlock(&refcnt.lock, "refcnt");
  acquire(&refcnt.lock);
  // 初始化页面引用计数数组的值, 系统启动时, freerange函数会调用kfree函数
  // 因此, 首先需要将页面引用计数数组中每一项的值都初始化为1
  for (int i = 0; i < (PHYSTOP - KERNBASE) / PGSIZE; i++)
    refcnt.counter[i] = 1;
  release(&refcnt.lock);
}

uint64 pgindex(uint64 pa)
{
  return (pa - KERNBASE) / PGSIZE;
}

void refcnt_setter(uint64 pa, int n)
{ 
  acquire(&refcnt.lock);
  refcnt.counter[pgindex((uint64)pa)] = n;
  release(&refcnt.lock);
}

uint refcnt_getter(uint64 pa)
{
  return refcnt.counter[pgindex(pa)];
}

void refcnt_incr(uint64 pa, int n)
{
  acquire(&refcnt.lock);
  refcnt.counter[pgindex(pa)] += n;
  release(&refcnt.lock);
}

void refcnt_decr(uint64 pa, int n)
{
  acquire(&refcnt.lock);
  refcnt.counter[pgindex(pa)] -= n;
  release(&refcnt.lock);
}

```
**kinit函数：**

```
void kinit()
{ 
  // 初始化页面引用计数
  refcnt_init();
  initlock(&kmem.lock, "kmem");
  freerange(end, (void*)PHYSTOP);
}
```


**kalloc函数：**

```
void * kalloc(void)
{
  struct run *r;

  acquire(&kmem.lock);
  r = kmem.freelist;
  if(r)
    kmem.freelist = r->next;
  release(&kmem.lock);

  if(r)
  {
    memset((char*)r, 5, PGSIZE); // fill with junk
    // 将页面引用计数设置为1
    refcnt_setter((uint64)r, 1);
  }

  return (void*)r;
}
```


**kfree函数：**

```
void kfree(void *pa)
{
  struct run *r;

  if(((uint64)pa % PGSIZE) != 0 || (char*)pa < end || (uint64)pa >= PHYSTOP)
    panic("kfree");

  // 页面引用计数大于1时, 只需要将计数值减1
  if(refcnt_getter((uint64)pa) > 1)
  { 
    refcnt_decr((uint64)pa,1);
    return;
  }
  // 页面引用计数为1时, 需要释放页面
  else if(refcnt_getter((uint64)pa) == 1){
    refcnt_setter((uint64)pa, 0);
  }
  // 页面引用计数小于1时, 则需要报告错误信息
  else{
    printf("kfree: refcnt has already less than 1\n");
    return -1;
  }

  // Fill with junk to catch dangling refs.
  memset(pa, 1, PGSIZE);

  r = (struct run*)pa;

  acquire(&kmem.lock);
  r->next = kmem.freelist;
  kmem.freelist = r;
  release(&kmem.lock);
}
```

**cow_fork_handler函数：**

```
int cow_fork_handler(pagetable_t pagetable, uint64 fault_addr)
{ 
  pte_t *pte;
  uint64 old_pa,flags;
  char *new_pa;

  // cow_fork_handler()对有效地址的要求与usertrap()不同, 后者还要求fault_addr < p->sz
  if (fault_addr < 0 || fault_addr >= MAXVA)
	{	
		printf("cow_fork_handler: wrong address!\n");
    return -1;
  }

  uint64 va = PGROUNDDOWN(fault_addr);
  // 根据va, 从进程页表中获得指向相应PTE的指针
  // 虚拟地址va必须要是页面对齐的,因此需要事先调用PGROUNDDOWN处理出错地址
  if((pte = walk(pagetable,va,0)) == 0)
  {
    printf("cow_fork_handler: pte should exist\n");
    return -1;
  }

  // 只处理COW页
  if(!(*pte & PTE_COW))
  {
    printf("cow_fork_handler: this is not a COW page\n");
    return -1;
  }

  // 优化点：当触发异常的COW页的引用计数只为1时, 只需要直接修改其PTE中的标志位即可
  if(refcnt_getter(PTE2PA(*pte)) == 1)
  {
    *pte &= ~PTE_COW;
    *pte |= PTE_W;
  }
  // 否则便需要重新分配一个新页面,并在页表中与该新页面建立映射关系
  else
  {
    // 申请分配一个新页面, 用于存放虚拟地址va对应旧页面的数据
    if((new_pa = kalloc()) == 0)
    { 
      printf("cow_fork_handler: there'r no free memory for cow page fault\n");
      return -1;
    }
    // 通过PTE获得旧页面的物理地址
    old_pa = PTE2PA(*pte);  
    // 将旧页面的数据复制到新页面中
    memmove(new_pa, (char*)old_pa, PGSIZE);
    // 设置新页面的标志位
    flags = (PTE_FLAGS(*pte) | PTE_W) & (~PTE_COW);

    // 在进程页表中解除虚拟地址va与旧页面原有的映射关系, 注意：该函数会清除va对应PTE的值
    // 所以 flags = (PTE_FLAGS(*pte) | PTE_W) & (~PTE_COW) 要在uvmunmap函数之前执行
    uvmunmap(pagetable, va, 1, 0);
    // 当前进程不再需要使用旧页面,因此需要释放该页
    kfree((void *)old_pa);

    // 在进程页表中将虚拟地址va映射到新页面, 并赋予对应的权限
    mappages(pagetable, va, PGSIZE, (uint64)new_pa, flags);
  }

  return 0;
}
```
**3.** 修改`trap.c`文件中的`usertrap`函数：

```
void usertrap(void)
{ 
  ...
  // trap来自设备中断 
  else if((which_dev = devintr()) != 0)
  { 
   // ok
  } 
  else if(r_scause() == 15)
  { 
    uint64 fault_addr = r_stval();
    // 超过最大地址或者在gurad page中报错
    if (fault_addr >= MAXVA || (fault_addr <= PGROUNDDOWN(p->trapframe->sp) && fault_addr >= PGROUNDDOWN(p->trapframe->sp) - PGSIZE) || fault_addr < 0 || fault_addr >= p->sz)
    { 
      printf("cow_fork_handler: wrong address\n");
      p->killed = 1;
    }
    else
    {
      if(cow_fork_handler(p->pagetable, fault_addr) == -1)
      {
        printf("usertrap(): unexpected scause %p pid=%d\n", r_scause(), p->pid);
        printf("            sepc=%p stval=%p\n", r_sepc(), r_stval());
        // 代表将要终止当前进程
        p->killed = 1;
      }
    }   
  }
 ...
  usertrapret();
}
```



**4.** 修改`defs.h`文件，添加对以下函数的声明

```
...
// kalloc.c
...
uint64 pgindex(uint64 pa);
void refcnt_setter(uint64 pa, int n);
uint refcnt_getter(uint64 pa);
void refcnt_incr(uint64 pa, int n);
void refcnt_decr(uint64 pa, int n)
int cow_fork_handler(pagetable_t pagetable, uint64 fault_addr);
...
// vm.c
...
pte_t*			walk(pagetable_t pagetable, uint64 va, int alloc);
...
```
**5.** 修改`risv.h`文件，添加COW标志位：

```
...
#define PTE_COW (1L << 8)
...
```

**6.** debug时遇到的坑：
- 在缺页异常处理函数`cow_fork_handler`中，`flags = (PTE_FLAGS(*pte) | PTE_W) & (~PTE_COW)`必须要放在`uvmunmap(pagetable, va, 1, 0)`之前，这是因为`uvmunmap`函数会清除虚拟地址va对应的PTE。

- 在`kalloc`函数中，注意要把`refcnt_setter((uint64)r, 1)`放入判断r是否为空的作用范围中，否则就会出现当前已无可用内容分配(r = 0)，但对应页面的引用计数仍被设置为1的情况。

---

# 2.Lazy Allocation
- 简单来讲，Lazy Allocation可以看作是对sbrk系统调用的一种优化。用户进程调用sbrk时，系统并不会真正地为其分配物理内存以及建立映射，而是单纯地修改其可用的虚拟地址范围(即修改p->sz的值)，等到进程实际访问该虚拟地址时，再为其分配物理内存并建立映射。

- 这里的**Lazy Allocation**实验是在完成了上述的**Copy On Write**实验的基础上进行的

**实现细节：**
**1.** 修改`sys_sbrk`函数：

```
uint64 sys_sbrk(void)
{
  int addr;
  int n;
  struct proc *p =  myproc();

  if(argint(0, &n) < 0)
    return -1;

  addr = p->sz;
  
  if(n < 0)
  {
    if ((int)p->sz + n >= (int)PGROUNDUP(p->trapframe->sp)) 
    	uvmdealloc(p->pagetable, p->sz, p->sz + n);
    else 
    	return -1;
  }

  // 修改当前进程可用的虚拟地址空间
  p->sz = addr + n;
  // 返回修改前最大可用的虚拟地址
  return addr;
}
```
**这里要注意一个细节**：sbrk针对的是堆内存，而在xv6中，堆内存的范围在栈之上（即PGROUNDUP(p->trapframe->sp)）。因此，在释放堆内存的时，不能把非堆内存包括在内。

---


**2.** 修改`usertrap`函数：

```
void usertrap(void)
{
	...
	else if(r_scause() == 15 || r_scause() == 13)
  { 
    uint64 fault_addr = r_stval();
    // 检查地址的合法性
    if(fault_addr >= MAXVA || fault_addr < 0 || fault_addr >= p->sz || (fault_addr <= PGROUNDDOWN(p->trapframe->sp) && fault_addr >= PGROUNDDOWN(p->trapframe->sp) - PGSIZE))
    { 
      printf("usertrap(): wrong address!\n");
      p->killed = 1;
      goto end;
    }

    // 首先调用cow_fork_handler函数处理, 检查是否为COW页
    if(cow_fork_handler(p->pagetable, fault_addr) == 0){
    	goto end;
    }

    // 进一步检查地址是否属于堆内存，如果是，则调用lazy_allocation_handler函数处理
    if(PGROUNDUP(p->trapframe->sp) <= fault_addr && fault_addr < p->sz)
    {	
    	if(lazy_allocation_handler(p->pagetable, p->sz, fault_addr) == 0){
    		goto end;
    	}
    }

    p->killed = 1;

  } // r_scause() == 15 || r_scause() == 13
  ...

end:
  // p->killed则终止当前进程
  if(p->killed)
    exit(-1);

  // give up the CPU if this is a timer interrupt.
  if(which_dev == 2)
    yield();

  // 调用usertrapret()函数
  usertrapret();
}
```
由于我们同时维护着COW和LazyAllocation机制，因此处理trap的时候也要分情况进行处理，大致思路是：
- 首先检查地址的合法性

- 随后调用`cow_fork_handler`函数，若该函数返回0，则说明fault_addr有对应的物理页面且为COW页。因此不需要调用`lazy_allocation_handler`函数进行处理。

- 若`lazy_allocation_handler`函数返回的是-1，则进一步检查fault_addr是否属于堆内存。如果是，则调用lazy_allocation_handler函数进行处理。

- 如果以上都不成功，说明是进程的访问权限不够，直接kill掉对应进程。 


---


**3.** 实现`lazy_allocation_handler`函数：
```
int lazy_allocation_handler(pagetable_t pagetable, uint64 sz, uint64 fault_addr)
{	
	pte_t *pte;
	uint64 va;
	char *new_pa;

	if (fault_addr < 0 || fault_addr >= sz || fault_addr >= MAXVA)
	{
    return -1;
  }

  va = PGROUNDDOWN(fault_addr);
  // 指向PTE的指针不为0且PTE设置了PTE_V标志
	if((pte = walk(pagetable,va,0)) != 0 && (*pte & PTE_V))
	{
		printf("lazy_allocation_handler: va already have mapping!\n");
    return -1;
	}
	// 申请分配一个新页面
	if((new_pa = kalloc()) == 0)
  { 
  	printf("lazy_allocation_handler: there's no free memory!\n");
    return -1;
  }

  // 在进程页表中将虚拟地址va映射到新页面, 并赋予对应的权限
  if(mappages(pagetable, va, PGSIZE, (uint64)new_pa, PTE_W|PTE_X|PTE_R|PTE_U) != 0)
  {	
    printf("lazy_allocation_handler: mappages errors!\n");
    kfree(new_pa);
    return -1;
  }

  return 0;
}	
```

---

**4.** 处理出现panic的情况：
- xv6在没有实现Lazy Allocation之前，有一些情形是不可能出现的。因此会直接panic，但是由于我们实现了Lazy Allocation，一个进程的地址空间中完全有可能出现未被映射的虚拟地址，所以我们需要处理这些情况

`uvmunmap`函数：

```
void uvmunmap(pagetable_t pagetable, uint64 va, uint64 npages, int do_free)
{
  uint64 a;
  pte_t *pte;

  if((va % PGSIZE) != 0)
    panic("uvmunmap: not aligned");

  for(a = va; a < va + npages*PGSIZE; a += PGSIZE){
    if((pte = walk(pagetable, a, 0)) == 0)
//      panic("uvmunmap: walk"); // LAZY ALLOCATION
      continue;
    if((*pte & PTE_V) == 0)
//      panic("uvmunmap: not mapped"); // LAZY ALLOCATION
      continue;
    if(PTE_FLAGS(*pte) == PTE_V)
      panic("uvmunmap: not a leaf");
    if(do_free) {
      uint64 pa = PTE2PA(*pte);
      kfree((void *) pa);
    }
    *pte = 0;
  }
}
```
- 实现了Lazy Allocation机制之后，用户进程便可以调用sbrk()增加自己的内存空间。但新增的这些虚拟地址并没有对应的物理地址，即没有创建对应的映射。若此时我们调用`uvmunmap`函数解除这些新增虚拟地址映射关系，系统便会直接panic，因此我们需要修改`uvmunmap`函数，使其忽略虚拟地址对应PTE不存在的情况。

`uvmcopy`函数：

```
int uvmcopy(pagetable_t old, pagetable_t new, uint64 sz)
{
  pte_t *pte;
  uint64 pa, i;
  uint flags;

  for(i = 0; i < sz; i += PGSIZE)
  {
    // 寻找指向父进程虚拟地址i对应的PTE的指针
    if((pte = walk(old, i, 0)) == 0)
    	continue; // panic改为continue
    // 虚拟地址i对应PTE不存在则报错
    if((*pte & PTE_V) == 0)
    	continue; // panic改为continue

    ...
}
```
- 同样地，我们还需要进一步修改Lab cow实验所实现的`uvmcopy`函数，使其忽略父进程中不存在对应PTE的虚拟地址

---

**5.** 处理内核中的Lazy Allocation：
- 不光是usertrap，当进行copyin、copyout等操作时，也需要访问进程虚拟地址对应的物理地址，因此我们也需要进行相应的处理。

`copyout`函数：

```
int copyout(pagetable_t pagetable, uint64 dstva, char *src, uint64 len)
{ 
  pte_t *pte;
  uint64 n, va, pa;
  // 需要在vm.c文件中引用proc.h以及spinlock.h，spinlock.h的引用需要在proc.h的前面
  struct proc *p = myproc();

  while(len > 0)
  { 
    // dstva必须小于最大虚拟地址
    if (dstva >= MAXVA)
    { 
      printf("copyout: wrong address\n");
      return -1;
    }

    // 向下取整dstva,va是页面对齐的虚拟地址
    va = PGROUNDDOWN(dstva);
    // 指向PTE的指针为0或指向PTE的指针不为0，但PTE没有设置PTE_V标志
    if((pte = walk(pagetable,va,0)) == 0 || !(*pte & PTE_V) )
    {
    	// 目的虚拟地址位于当前进程通过sbrk函数向内核申请的地址范围内
   		if(dstva < p->sz && dstva >= PGROUNDUP(p->trapframe->sp))
   		{	
   			if(lazy_allocation_handler(pagetable,p->sz,dstva) == -1)
   			{	
   				printf("copyout: lazy_allocation_handler returns -1!\n");
        	return -1;
   			}
    	}
    	else
    	{
    		printf("copyout: pte doesn't exist and dstva beyond the range of heap\n");
        return -1;
    	}
    }
    
    // 调用cow_fork_handler函数处理COW页
    if(*pte & PTE_COW)
    { 
      if(cow_fork_handler(pagetable,dstva) == -1)
      {
      	printf("copyout: cow_fork_handler returns -1!\n");
        return -1;
      }
    }
    
    ...

  return 0;
}
```

`copyin`函数：

```
int copyin(pagetable_t pagetable, char *dst, uint64 srcva, uint64 len)
{
  uint64 n, va0, pa0;
  pte_t *pte;
  struct proc *p = myproc();

  while(len > 0)
  {
  	// srcva必须小于最大虚拟地址
    if (srcva >= MAXVA)
    { 
      printf("copyin: wrong address\n");
      return -1;
    }

    va0 = PGROUNDDOWN(srcva);

    if((pte = walk(pagetable, va0, 0)) == 0 || !(*pte & PTE_V))
    {
    	// 目的虚拟地址位于当前进程通过sbrk函数向内核申请的地址范围内
   		if(srcva < p->sz && srcva >= PGROUNDUP(p->trapframe->sp))
   		{	
   			if(lazy_allocation_handler(pagetable,p->sz,srcva) == -1)
   			{	
   				printf("copyin: lazy_allocation_handler returns -1!\n");
        	return -1;
   			}
    	}
    	else
    	{
    		printf("copyin: pte doesn't exist and srcva beyond the range of heap\n");
        return -1;
    	}
    }
    ...    
  }
  ...
  return 0;
}
```

---

**6.** 其他问题
- 修改完代码后，系统可以通过cowtest中的所有测试。但无法通过usertest中的sbrkbasic以及sbrkfail测试，即这两个测试会输出FAILED，但不会引起系统宕机。
- 另外还有一个奇怪的现象是：在init进程通过修改过后的sys_sbrk函数所创建的虚拟地址中，将某些地址作为参数调用walk后并不会返回0，即指向该地址对应PTE的指针(pte_t *pte)不为0，但其对应的PTE并不存在(*pte & PTE_V = 0)。

---
**7**. 更新
- 查看`sbrkbasic`函数的代码可以看到，该函数主要测试的是：当应用程序申请的内存过大时，sbrk是否会返回预期的错误值(`0xffffffffffffffffL`)。首先，该函数会调用`sbrk`函数申请1G的内存空间(1024*1024*1024)，并只有当`sbrk`函数返回的地址值为`0xffffffffffffffffL`时才能通过测试。

```
void sbrkbasic(char *s)
{
  enum { TOOMUCH=1024*1024*1024};
  int i, pid, xstatus;
  char *c, *a, *b;

  // does sbrk() return the expected failure value?
  pid = fork();
  if(pid < 0){
    printf("fork failed in sbrkbasic\n");
    exit(1);
  }
  if(pid == 0){
    a = sbrk(TOOMUCH);
    if(a == (char*)0xffffffffffffffffL){
      // it's OK if this fails.
      printf("sbrkbasic test, xstatus = %d\n", xstatus);
      exit(0);
    }
    
    for(b = a; b < a+TOOMUCH; b += 4096){
      *b = 99;
    }
    
    // we should not get here! either sbrk(TOOMUCH)
    // should have failed, or (with lazy allocation)
    // a pagefault should have killed this process.
    exit(1);
  }

  wait(&xstatus);

  printf("sbrkbasic test, xstatus = %d\n", xstatus);

  if(xstatus == 1){
    printf("%s: too much memory allocated!\n", s);
    exit(1);
  }
  ...
  ...
}
```
因此，我们需要将`sys_sbrk`函数修改为：

```
uint64 sys_sbrk(void)
{
  int addr;
  int n;
  struct proc *p =  myproc();

  if(argint(0, &n) < 0)
    return -1;
  
  // 获得修改前的进程堆顶地址
  addr = p->sz;

  // n < 0代表要减少进程堆的地址空间
  if(n < 0)
  {
    // 进程堆顶地址不能小于栈顶地址，具体可查看参考第三章的进程地址空间示意图
    if (p->sz + (uint64)n >= PGROUNDUP(p->trapframe->sp))
        uvmdealloc(p->pagetable, p->sz, p->sz + n);
    else
        return -1;
  }

  // 更新进程堆顶地址
  p->sz = addr + (uint64)n;
  
  // 限制每个进程只能申请小于1G的地址空间
  if(p->sz >= PGROUNDUP(p->trapframe->sp) + 1024*1024*1024)
  {
    // 恢复修改前的进程堆顶地址
    p->sz = addr;
    // 返回错误值
    return 0xffffffffffffffffL;
  }

  // 返回修改前的进程堆顶地址
  return addr;
}
``
```

