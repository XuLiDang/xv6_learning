# 1.Speed up system calls
**实现细节**：
1. 在proc.h文件的proc结构体中加入`struct usyscall *usyscall`, 该结构体定义在memlayout.h文件中
```
struct proc 
{
  ..
  struct usyscall *usyscall;   // page for usyscall
  ...
};
```
2.在proc.c文件的allocproc函数中添加以下代码，这段代码要放在调用proc_pagetable函数之前：

```
 ...
 // Allocate a usyscall page.
  if((p->usyscall = (struct usyscall *)kalloc()) == 0){
    freeproc(p);
    release(&p->lock);
    return 0;
  }
  // 将进程pid放入usyscall页
  p->usyscall->pid = p->pid;
  ....
  
```
3.在proc.c文件的proc_pagetable函数中添加以下代码：

```
  // map the usyscall just below TRAPFRAME
  // 在进程p的用户页表中建立起USYSCALL(va)到p->usyscall(pa)的映射
  // 同时设置权限为PTE_R以及PTE_U
  if(mappages(pagetable, USYSCALL, PGSIZE,
              (uint64)(p->usyscall), PTE_R | PTE_U) < 0){
    uvmunmap(pagetable, USYSCALL, 1, 0);
    uvmfree(pagetable, 0);
    return 0;
  }
```
4.在proc.c文件的freeproc函数中添加以下代码：

```
 // 释放usyscall页
 if(p->usyscall)
    kfree((void*)p->usyscall);
  p->usyscall = 0;
```

5.在proc.c文件的proc_freepagetable函数中添加以下代码：

```
...
// 在用户页表中解除USYSCALL的映射，否则会有freewalk panic
uvmunmap(pagetable, USYSCALL, 1, 0);
...
```
# 2.Print a page table
**实现细节：**
1.在vm.c文件中实现vmprint函数，代码如下：

```
void vmprint(pagetable_t pagetable,int level)
{
  // there are 2^9 = 512 PTEs in a page table.
  for(int i = 0; i < 512; i++)
  {
    pte_t pte = pagetable[i];
    if(pte & PTE_V)
    { 
      switch(level)
      {
        case 0:
          printf(".. .. .. %d: pte %p pa %p\n",i,pte,PTE2PA(pte));
          break;
        case 1:
          printf(".. .. %d: pte %p pa %p\n",i,pte,PTE2PA(pte));
          break;
        case 2:
          printf(".. %d: pte %p pa %p\n",i,pte,PTE2PA(pte));
          break;
      }

      // this PTE points to a lower-level page table.
      uint64 child = PTE2PA(pte);
      if(level > 0 )
        vmprint((pagetable_t)child,level-1);
    } 
  
  }
}
```
2.在exec.c文件中的exec函数（语句`return argc`前）中添加以下代码：

```
 if(p->pid == 1)
    vmprint(p->pagetable,2);
    
```
3.在defs.h文件中声明新实现的vmprint函数，这样exec函数才能调用该函数

```
// vm.c
...
void   vmprint(pagetable_t pagetable,int level);
...
```
# 3.Detecting which pages have been accessed
**实现细节：**
1.修改risc.h，添加`PTE_A`的定义

```
...
#define PTE_V (1L << 0) // valid
#define PTE_R (1L << 1)
#define PTE_W (1L << 2)
#define PTE_X (1L << 3)
#define PTE_U (1L << 4) // 1 -> user can access
// 将0...01(64位)左移6位 --> 0...100000
#define PTE_A (1L << 6)
...
```
2.修改defs.h，添加对vm.c中walk函数的声明：

```
// vm.c
...
pte_t*    walk(pagetable_t pagetable, uint64 va, int alloc);
```
3.在sysproc.c文件中实现`sys_pgaccess`函数：

```
...
int sys_pgaccess(void)
{ 
  // base用于存放用户进程传进来的虚拟地址
  // u_mask则是用户空间的缓冲区，用于存放内核传递给用户空间的数据
  uint64 base,u_mask;
  // len代表需要检查的页数
  int len;
  // abits是内核要传递给用户空间的数据，代表用户进程传递给该函数进行检查
  // 的所有页面中，哪些页已被访问，例如000....1代表第一个页面已被访问
  unsigned int abits = 0;
  // 获取当前进程
  struct proc *p = myproc();
  // pte_p为指向PTE的指针
  pte_t *pte_p;

  // 获取用户进程传递的参数
  if(argaddr(0, &base) < 0)
    return -1;  
  if(argint(1, &len) < 0)
    return -1;
  if(argaddr(2, &u_mask) < 0)
    return -1;

  // 循环检查从虚拟地址base初始值开始的len个页面
  // 每结束一轮循环，base += PGSIZE
  for(int i = 0; i < len; i++)
  { 
    // base的值不能大于或等于MAXVA
    if(base >= MAXVA)
      return -1; 

    // 找到逻辑地址base所对应的PTE的指针
    pte_p = walk(p->pagetable,base,0);
    // 判断以base为首地址的页是否已被访问，主要通过判断
    // 其对应PTE中的访问位是否已被置位(特指最后一级页表的PTE)
    if(*pte_p & PTE_A)
    { 
      // 在abits中记录访问位已被置位的页
      // 例：abits | (1 << 0) 则代表第一个页的访问位已被置位
      abits = abits | (1 << i);
      // 清除其对应PTE中的访问位
      // ~PTE_A = 1...011111
      *pte_p = *pte_p & (~PTE_A);
    }
    // 检查下一个页面
    base += PGSIZE;
  }

  // 将abits传递给用户空间，通过首地址为u_mask的缓冲区来接收
  if (copyout(p->pagetable, u_mask, (char *)&abits, sizeof(unsigned int)) < 0)
    return -1;
  
  return 0;
}

```


