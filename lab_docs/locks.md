# 1.Memory allocator
- 官网的提示写得很清晰，即为每个CPU都维护一个空闲内存队列，而不是所有CPU共用一个队列。除此之外，在初始化内存时，首先将所有的空闲内存都放入到运行`kinit()`函数的CPU的空闲队列中。而当某个CPU的空闲队列无法满足当前的内存分配请求时，可以借用其他CPU的空闲队列来满足此次分配请求。

**实现代码：**
我们仅需要修改`kalloc.c`文件下的代码，修改如下：

**1. 数据结构定义**

```
struct kmem {
  struct spinlock lock;
  struct run *freelist;
};

// 每个CPU均有一个kmem结构体
struct kmem kmems[NCPU];
```
**2. kinit函数**

```
void kinit()
{ 	
	// 初始化每个CPU对应的空闲队列锁
	for(int i = 0; i < NCPU; i++)
		initlock(&kmems[i].lock, "kmem");
	// 初始化空闲链表来保存kernel末端到PHYSTOP之间的内存
	freerange(end, (void*)PHYSTOP);
}
```
**3. kfree函数**

```
void kfree(void *pa)
{ 
  int cpu_id;
  struct run *r;

  // 获取当前CPU的id
  push_off();
  cpu_id = cpuid();
  pop_off();

  if(((uint64)pa % PGSIZE) != 0 || (char*)pa < end || (uint64)pa >= PHYSTOP)
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(pa, 1, PGSIZE);

  r = (struct run*)pa;

  // 获取当前CPU的空闲队列锁
  acquire(&kmems[cpu_id].lock);
  r->next = kmems[cpu_id].freelist;
  kmems[cpu_id].freelist = r;
  // 释放当前CPU的空闲队列锁
  release(&kmems[cpu_id].lock);
}
```

**4. kalloc函数**

```
void * kalloc(void)
{ 
  int cpu_id;
  struct run *r;

  // 获取当前CPU的id
  push_off();
  cpu_id = cpuid();
  pop_off();

  // 获取当前CPU的空闲队列锁
  acquire(&kmems[cpu_id].lock);
  // r此时指向当前CPU的空闲队列头
  r = kmems[cpu_id].freelist;

  // 当前CPU的空闲队列不为空
  if(r)
  { 
  	// 更新当前CPU的空闲队列头
    kmems[cpu_id].freelist = r->next;
    release(&kmems[cpu_id].lock);
  }
  // 当前CPU的空闲队列为空
  else
  { 
  	// 释放当前CPU的空闲队列锁
  	release(&kmems[cpu_id].lock);
  	// 借用其他CPU的空闲队列来满足此次分配请求
  	for(int i = 0; i < NCPU; i++)
  	{	
  		// 第i个CPU的空闲队列不为空
  		if(kmems[i].freelist)
  		{ 
  		  // 获取第i个CPU的空闲队列锁
  		  acquire(&kmems[i].lock);
  		  r = kmems[i].freelist;
  		  // 更新第i个CPU的空闲队列头
  		  kmems[i].freelist = r->next;
  		  // 释放第i个CPU的空闲队列锁
  		  release(&kmems[i].lock);
  		  break;
  		}
  	}
  }

  if(r) 
    memset((char*)r, 5, PGSIZE); // fill with junk

  return (void*)r;
}
```

---

# 2.Buffer cache
- 官网提示我们要采用哈希表(表中每一项为hash bucket，且每个hash bucket都有独立的锁)以及利用时间戳建立LRU队列。不过在实现代码时，这里我们采用了较为简单粗暴的方法，即仅使用了哈希表，并且每个hash bucket均有独立的锁和缓冲区。

**实现代码：**
因没有利用时间戳建立LRU队列，故仅需要修改`bio.c`文件中的代码即可。

**1. 数据结构定义**
```
#define NBUCKETS 13
// 哈希表中每一项的内容(hash bucket)
struct bcache {
  struct spinlock lock;
  struct buf bufs[NBUF];
};
// 哈希表
struct bcache hash_bcache[NBUCKETS];
```

**2. binit函数**
```
// 初始化哈希表中每个hash bucket的锁
void binit(void)
{
  for(int i = 0; i < NBUCKETS; i++)
    initlock(&hash_bcache[i].lock, "bcache.bucket");  
}
```

**3. bget函数**
```
static struct buf* bget(uint dev, uint blockno)
{
  struct buf *b;

  // 获取块号对应hash bucket的锁
  acquire(&hash_bcache[blockno % NBUCKETS].lock);
  // 遍历hash bucket中的缓冲区，寻找符合条件的buffer
  for(b = hash_bcache[blockno % NBUCKETS].bufs; b < hash_bcache[blockno % NBUCKETS].bufs + NBUF; b++){
    if(b->dev == dev && b->blockno == blockno){
      // 增加该buffer的引用计数
      b->refcnt++; 
      // 释放对bcache结构体的锁
      release(&hash_bcache[blockno % NBUCKETS].lock);
      // 为该buffer申请sleep-lock,确保同一时间只有一个线程在使用该buffer
      acquiresleep(&b->lock);
      return b;
    }
  }

  // 再次遍历hash bucket中的缓冲区,查找引用计数为0的buffer
  for(b = hash_bcache[blockno % NBUCKETS].bufs; b < hash_bcache[blockno % NBUCKETS].bufs + NBUF; b++){
    // 修改buffer的元数据来记录新的设备号和块号(扇区号)
    if(b->refcnt == 0) {
      b->dev = dev;
      b->blockno = blockno;
      b->valid = 0;
      b->refcnt = 1;
      // 释放对bcache结构体的锁
      release(&hash_bcache[blockno % NBUCKETS].lock);
      // 为该buffer申请sleep-lock,确保同一时间只有一个线程在使用该buffer
      acquiresleep(&b->lock);
      return b;
    }
  }
  panic("bget: no buffers");
}
```

**4. brelse函数**
```
void brelse(struct buf *b)
{
  if(!holdingsleep(&b->lock))
    panic("brelse");

  // 释放sleep-lock
  releasesleep(&b->lock);

  // 获取块号(b->blockno)对应hash bucket的锁
  acquire(&hash_bcache[b->blockno % NBUCKETS].lock);
  b->refcnt--;
  release(&hash_bcache[b->blockno % NBUCKETS].lock);
}
```
**5. bpin函数**
```
void bpin(struct buf *b) {
  // 获取块号(b->blockno)对应hash bucket的锁
  acquire(&hash_bcache[b->blockno % NBUCKETS].lock);
  b->refcnt++;
  release(&hash_bcache[b->blockno % NBUCKETS].lock);
}
```

**6. bunpin函数**
```
void bunpin(struct buf *b) {
  // 获取块号(b->blockno)对应hash bucket的锁
  acquire(&hash_bcache[b->blockno % NBUCKETS].lock);
  b->refcnt--;
  release(&hash_bcache[b->blockno % NBUCKETS].lock);
}
```
**7.BUG修复**
- 修改param.h中的`FSSZIE 1000`，增加`FSSZIE`的值，否则在执行测试程序时便会因为文件系统磁盘块不足而导致出现`panic: balloc: out of blocks`。

---

# 3. 运行结果

**kalloctest**
```
$ kalloctest
start test1
test1 results:
--- lock kmem/bcache stats
lock: kmem: #test-and-set 0 #acquire() 92682
lock: kmem: #test-and-set 0 #acquire() 159352
lock: kmem: #test-and-set 0 #acquire() 180924
lock: bcache.bucket: #test-and-set 0 #acquire() 10
lock: bcache.bucket: #test-and-set 0 #acquire() 22
lock: bcache.bucket: #test-and-set 0 #acquire() 12
lock: bcache.bucket: #test-and-set 0 #acquire() 12
lock: bcache.bucket: #test-and-set 0 #acquire() 12
lock: bcache.bucket: #test-and-set 0 #acquire() 92
lock: bcache.bucket: #test-and-set 0 #acquire() 1084
lock: bcache.bucket: #test-and-set 0 #acquire() 2
lock: bcache.bucket: #test-and-set 0 #acquire() 2
--- top 5 contended locks:
lock: proc: #test-and-set 171412 #acquire() 628121
lock: proc: #test-and-set 163232 #acquire() 628063
lock: proc: #test-and-set 155011 #acquire() 628121
lock: proc: #test-and-set 146823 #acquire() 628121
lock: proc: #test-and-set 142190 #acquire() 628122
tot= 0
test1 OK
start test2
total free number of pages: 32401 (out of 32768)
.....
test2 OK

```

**bcachetest**

```
$ bcachetest
start test0
test0 results:
--- lock kmem/bcache stats
lock: kmem: #test-and-set 0 #acquire() 738284
lock: kmem: #test-and-set 0 #acquire() 1825129
lock: kmem: #test-and-set 0 #acquire() 1689334
lock: bcache.bucket: #test-and-set 0 #acquire() 4126
lock: bcache.bucket: #test-and-set 0 #acquire() 2120
lock: bcache.bucket: #test-and-set 0 #acquire() 4284
lock: bcache.bucket: #test-and-set 0 #acquire() 2266
lock: bcache.bucket: #test-and-set 0 #acquire() 4272
lock: bcache.bucket: #test-and-set 0 #acquire() 4310
lock: bcache.bucket: #test-and-set 0 #acquire() 6782
lock: bcache.bucket: #test-and-set 0 #acquire() 10092
lock: bcache.bucket: #test-and-set 0 #acquire() 6176
lock: bcache.bucket: #test-and-set 0 #acquire() 6176
lock: bcache.bucket: #test-and-set 0 #acquire() 6182
lock: bcache.bucket: #test-and-set 0 #acquire() 6176
lock: bcache.bucket: #test-and-set 0 #acquire() 4118
--- top 5 contended locks:
lock: uart: #test-and-set 3665932 #acquire() 3491
lock: proc: #test-and-set 3135932 #acquire() 15887687
lock: proc: #test-and-set 3120202 #acquire() 15883639
lock: proc: #test-and-set 3119067 #acquire() 15887687
lock: proc: #test-and-set 3104114 #acquire() 15867155
tot= 0
test0: OK
start test1
test1 OK

```
**usertests**

```
$ usertests
usertests starting
test MAXVAplus: usertrap(): unexpected scause 0x000000000000000f pid=17
            sepc=0x0000000000002228 stval=0x0000004000000000
usertrap(): unexpected scause 0x000000000000000f pid=18
            sepc=0x0000000000002228 stval=0x0000008000000000
usertrap(): unexpected scause 0x000000000000000f pid=19
            sepc=0x0000000000002228 stval=0x0000010000000000
......
......
......
test createtest: OK
test openiput: OK
test exitiput: OK
test iput: OK
test mem: OK
test pipe1: OK
test killstatus: OK
test preempt: kill... wait... OK
test exitwait: OK
test rmdot: OK
test fourteen: OK
test bigfile: OK
test dirfile: OK
test iref: OK
test forktest: OK
test bigdir: OK
ALL TESTS PASSED

```


