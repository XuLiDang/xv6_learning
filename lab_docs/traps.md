# 1. RISC-V assembly
**the assembly code for main:**

```
000000000000001c <main>:

void main(void) {
  1c:	1141                	addi	sp,sp,-16
  1e:	e406                	sd	ra,8(sp)
  20:	e022                	sd	s0,0(sp)
  22:	0800                	addi	s0,sp,16
  printf("%d %d\n", f(8)+1, 13);
  24:	4635                	li	a2,13
  26:	45b1                	li	a1,12
  28:	00000517          	auipc	a0,0x0
  2c:	7b050513          	addi	a0,a0,1968 # 7d8 <malloc+0xea>
  # auipc的作用是把立即数左移12位，低12位补0，和pc相加赋给指定寄存器
  # 这里立即数是0，指定寄存器是ra，即ra=pc+0x0=0x30=48
  30:	00000097          	auipc	ra,0x0
  # jalr 1536(ra)代表1536加上ra寄存器的值，然后赋值给pc
  # 将1536转为16进制再加上0x30即为0x0000000000000630
  34:	600080e7          	jalr	1536(ra) # 630 <printf>
  exit(0);
  38:	4501                	li	a0,0
  3a:	00000097          	auipc	ra,0x0
  3e:	27e080e7          	jalr	638(ra) # 2b8 <exit>
```

**Q1:** Which registers contain arguments to functions? For example, which register holds 13 in main's call to printf?****

**Answer:** register a0-a5 contain the arguments to functions; register a2 holds 13 in main's call to printf.

**Q2:** Where is the call to function f in the assembly code for main? Where is the call to g? (Hint: the compiler may inline functions.)****

**Answer:** the compiler have inlined these two functions.

**Q3:** At what address is the function printf located?
**Answer:** 
According to the following code:

```
# 这里立即数是0，指定寄存器是ra，即ra=pc+0x0=0x30=48
  30:	00000097          	auipc	ra,0x0
  # jalr 1536(ra)代表1536加上ra寄存器的值，然后赋值给pc
  # 并将ra的值+8，使得ra寄存器存放的是从printf返回后在main函数的地址
  # 1536转为16进制再加上0x30即为0x0000000000000630
  34:	600080e7          	jalr	1536(ra) # 630 <printf>
```
we can know that the address of function printf is 0x0000000000000630, and the assembly code of printf function in `call.asm` proves our assumption.

```
0000000000000630 <printf>:

void
printf(const char *fmt, ...)
{
 630:	711d                	addi	sp,sp,-96
 632:	ec06                	sd	ra,24(sp)
 634:	e822                	sd	s0,16(sp)
 ...
```

**Q4:** What value is in the register ra just after the jalr to printf in main?
**Answer:** 
According to the following code:

```
# 这里立即数是0，指定寄存器是ra，即ra=pc+0x0=0x30=48
  30:	00000097          	auipc	ra,0x0
  # jalr 1536(ra)代表1536加上ra寄存器的值，然后赋值给pc
  # 并将ra的值+8，使得ra寄存器存放的是从printf返回后在main函数的地址
  # 1536转为16进制再加上0x30即为0x0000000000000630
  34:	600080e7          	jalr	1536(ra) # 630 <printf>
```
So we can know that the value of register `ra` is 0x30 + 8 = 0x38

**Q5:** Run the following code:

	unsigned int i = 0x00646c72;
	printf("H%x Wo%s", 57616, &i);
      
1.What is the output? 
2.The output depends on that fact that the RISC-V is little-endian. If the RISC-V were instead big-endian what would you set i to in order to yield the same output? Would you need to change 57616 to a different value?

**Answer:**
1. the value of 57616 in the format of %x is `0xe110`, and the value of 0x00646c72 is rld\0 under the format of %s. So, the complete output is "He110, World\0".
2. we don't need to change 57616 to a different value, but the value of i need to be changed to `0x726c6400`

**Q6:** In the following code, what is going to be printed after 'y='? (note: the answer is not a specific value.) Why does this happen?

```
printf("x=%d y=%d", 3)
```
**Answer:** It will directly print out the value of a2.

# 2.Backtrace
下面是一个非常简单的Stack的结构图，其中每一个区域都是一个Stack Frame，每执行一次函数调用就会移动Stack Pointer，从而在当前的Stack中创建一个Stack Frame。

![image.png](https://gitlab.com/XuLiDang/xv6_learning/-/raw/master/lab_docs/xv6_stack_image.png)

对于Stack来说，是从高地址开始向低地址使用。所以Stack总是向下增长。当我们想要创建一个新的Stack Frame的时候，总是对当前的Stack Pointer做减法。

一个函数的Stack Frame包含了保存的寄存器，本地变量，并且，如果函数的参数多于8个，额外的参数会出现在Stack中。

所以Stack Frame大小并不总是一样，即使在这个图里面看起来是一样大的。不同的函数有不同数量的本地变量，不同的寄存器，所以Stack Frame的大小是不一样的。但是有关Stack Frame有两件事情是确定的：
- Return address总是会出现在Stack Frame的第一位
- 指向前一个Stack Frame的指针也会出现在栈中的固定位置

有关Stack Frame中有两个重要的寄存器：
- 第一个是**SP（Stack Pointer）寄存器**，该寄存器存放了一个栈指针，该指针会指向Stack的底部并代表了当前Stack Frame的位置。

- 第二个是**FP（Frame Pointer）寄存器/S0寄存器**，该寄存器存放了一个Frame Pointer，而该寄存器所保存的Frame Pointer会指向当前Stack Frame的顶部。因为Return address和指向前一个Stack Frame的的Frame Pointer都在当前Stack Frame的固定位置，因此可以通过当前的FP/S0寄存器寻址到这两个数据。

我们在当前Stack Frame中保存指向前一个Stack Frame的Frame Pointer的原因是为了让能跳转回去。所以当前函数返回时，我们可以将指向前一个Stack Frame的Frame Pointer存储到FP寄存器中。

**实现代码**：
1.在`riscv.h`文件中添加以下代码：

```
// read FP
static inline uint64
r_fp()
{
  uint64 x;
  asm volatile("mv %0, s0" : "=r" (x) );
  return x;
}
```
2.在`kernel/printf.c`文件中添加实现函数`backtrace`：

```
void backtrace(void)
{
  uint64 fp,stack_bot,stack_top;
  uint64 *r_addrp,*prev_fp;
  // 获得指向当前stack frame顶部的frame pointer
  fp = r_fp();
  // xv6会为每个栈都分配一页内存,通过PGROUNDUP函数能计算出属于当前进程栈页顶部的地址
  // 通过PGROUNDDOWN函数能够计算出属于当前进程栈页底部的地址
  stack_top = PGROUNDUP(fp);
  stack_bot = PGROUNDDOWN(fp);

  printf("backtrace:\n");
  while(fp < stack_top && fp > stack_bot)
  { 
    // 地址“fp - 8”对应存储单元中的值是函数返回地址
    // 指针变量r_addrp对应存储单元存放的是地址“fp - 8”
    r_addrp = (uint64 *)(fp - 8);
    // *r_addrp是获取地址“fp - 8”对应存储单元的值
    printf("%p\n",*r_addrp);

    // 地址“fp - 16”对应存储单元中的值是指向前一个stack frame顶部的frame pointer
    // 指针变量prev_fp对应存储单元存放的是地址“fp - 16”
    prev_fp = (uint64 *)(fp - 16);
    // 将fp更新为指向前一个stack frame顶部的frame pointer
    fp = *prev_fp;

  }

}
```
3.在sysproc.c文件的`sys_sleep`函数中添加对`backtrace`函数的调用：

```
// sleep系统调用对应实现函数
uint64 sys_sleep(void)
{
  ...
    backtrace();
    sleep(&ticks, &tickslock);
  .....
  return 0;
}
```
4.在defs.h文件中添加`backtrace`函数的原型：

```
...
// printf.c
void            printf(char*, ...);
void            panic(char*) __attribute__((noreturn));
void            printfinit(void);
void  			backtrace(void);
...
```
# 3.Alarm
在官网阅读完该实验相关的说明后，实现步骤可概括为以下几点：

**1.** 需要在proc结构体中添加5个新属性，分别为：（1）alarm定时器，用来记录自从上次调用完进程的alarm handler后，经过了个ticks。（2）alarm定时器的响应时间，以tick为单位。（3）记录该进程的alarm handler的地址。（4）在返回用户空间执行alarm handler时，暂时存放trapframe页的内容。（5）判断是否正在执行alarm handler，从而避免对alarm handler的重入调用。

**2.** 创建进程时，需要为该进程分配一个页大小的内存空间，用来暂时上面说的的属性(4)，同时也需要初始化其他属性；同时在释放进程时，也要记得释放为进程所分配的所有页面。

**3.** 实现`sys_sigalarm`和`sys_sigreturn`函数，前者负责为proc结构体的新属性（2）和（3）赋值；而后者则负责恢复trapframe页中的内容，并修改新属性（5）的值，将其表示为当前alarm handler已执行完毕。

**4.** 修改`trap.c`文件的`usertrap`函数，当进程的alarm定时器到达指定响应时间时，便会调用用户空间的alarm handler。

**实现代码：**

**1.** 在`proc`结构体中添加以下属性：

```
// Per-process state
// 相当于linux中的task_struct结构体
struct proc 
{
  ...
  /*lab traps*/
  int passed_ticks;            // passed ticks since last call to a process's alarm handler
  int interval;                // alarm interval
  int alarm_flag;              // Check whether the alarm handler is executing
  uint64 handler_addr;         // alarm handler addr
  struct alarm *alarm;         // page for timer alarm, store the data of trapframe when execute alarm handler
};

```
**2.** 在`proc.h`文件中添加结构体`alarm`，该结构体的内容与`trapframe`结构体一致：

```
// lab alarm
struct alarm {
  /*   0 */ uint64 kernel_satp;   // kernel page table
  /*   8 */ uint64 kernel_sp;     // top of process's kernel stack
  /*  16 */ uint64 kernel_trap;   // usertrap()
  ...
  ...
  /* 280 */ uint64 t6;
};
```

**3.** 修改`proc.c`文件的`allocproc`函数，在创建进程时为其分配一页内存用于存放`alarm`结构体：

```
...
// Allocate a alarm page for lab traps
  if((p->alarm = (struct alarm *)kalloc()) == 0){
    freeproc(p);
    release(&p->lock);
    return 0;
  }

  // An empty user page table.
  p->pagetable = proc_pagetable(p);
  if(p->pagetable == 0){
    freeproc(p);
    release(&p->lock);
    return 0;
  }
...
// 初始化与lab traps有关的属性
  p->passed_ticks = 0;
  p->interval = 0;
  p->handler_addr = 0;
  p->alarm_flag = 0;
  
  return p;
```

**4.** 修改`proc.c`文件的`freeproc`函数，在释放进程时释放为其分配的用于存放`alarm`结构体的一页内存：

```
 if(p->alarm)
    kfree((void*)p->alarm);
 p->alarm = 0;
```

**5.** 在`sysproc.c`文件中添加`sys_sigalarm`和`sys_sigreturn`函数：

```
uint64 sys_sigalarm(void)
{
  int interval;
  uint64 handler_addr;
  // 获取当前进程
  struct proc *p = myproc();

  // 获取用户进程传递的参数
  if(argint(0, &interval) < 0) 
    return -1;  
  if(argaddr(1, &handler_addr) < 0)
    return -1;

  if(interval == 0)
    return -1;

  p->handler_addr = handler_addr;
  p->interval = interval;
  
  return 0;
}

uint64 sys_sigreturn(void)
{ 
  // 获取当前进程
  struct proc *p = myproc();

  if(p->alarm == 0)
    return -1;

  // 恢复执行alarm处理程序前trapframe页中的内容
  memmove(p->trapframe,p->alarm,sizeof(struct trapframe));
  // 代表alarm处理程序已结束
  p->alarm_flag = 0;
  
  return 0;
}
```
**5.** 修改**trap.c**文件中的**usertrap**函数：

```
void usertrap(void)
{
...
...
// trap来自设备中断 
  else if((which_dev = devintr()) != 0)
  { 
    // 引起trap的原因是时钟中断
    if(which_dev == 2)
    { 
      // 将当前进程的passed_ticks属性加1
      p->passed_ticks++;
      // passed_ticks的值等于alarm处理程序规定的时间间隔
      if(p->passed_ticks == p->interval)
      { 
        if(p->alarm_flag == 0)
        { 
          // 代表正在执行alarm处理程序
          p->alarm_flag = 1;
          // 将当前进程的passed_ticks属性清0,为下次调用alarm处理程序做准备
          p->passed_ticks = 0;
          // 将trapframe的内容保存到alarm页,因为从alarm处理程序返回内核时,trapframe中的内容会被覆盖       
          memmove(p->alarm,p->trapframe,sizeof(struct trapframe));

          // 下一次处理trap时会调用uservec
          w_stvec(TRAMPOLINE + (uservec - trampoline));
           // 设置当前进程的trapframe页中的内容，该进程下次进入内核时需要使用
          p->trapframe->kernel_satp = r_satp();         // kernel page table
          p->trapframe->kernel_sp = p->kstack + PGSIZE; // process's kernel stack
          p->trapframe->kernel_trap = (uint64)usertrap;
          p->trapframe->kernel_hartid = r_tp();         // hartid for cpuid()

          // set S Previous Privilege mode to User.
          unsigned long x = r_sstatus();
          x &= ~SSTATUS_SPP; // clear SPP to 0 for user mode
          w_sstatus(x);

          // 将alarm处理程序的地址写入到sepc寄存器中(虚拟地址,需要用进程页表进行解析)
          w_sepc(p->handler_addr);
          // 获得用户进程页表基地址
          uint64 satp = MAKE_SATP(p->pagetable);
          // 这里获得的fn是userret函数的虚拟地址,同样地
          // 在用户页表和内核页表中，该虚拟地址都是指向trampoline页中的某个地址
          uint64 fn = TRAMPOLINE + (userret - trampoline);
          // 调用userret函数,并将TRAPFRAME和satp作为参数传递
          // TRAPFRAME会传递到寄存器a0,satp的值会传送到寄存器a1
          ((void (*)(uint64,uint64))fn)(TRAPFRAME, satp);
        } // p->alarm_flag == 0 

      } // p->passed_ticks == p->interval

    } // which_dev == 2

  } // (which_dev = devintr()) != 0
...
...
}
```










