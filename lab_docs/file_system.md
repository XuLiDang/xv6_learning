# 1.Large files
**实现细节：**

1.修改文件`fs.h`中与块相关的定义
```
#define NDIRECT 11
#define NDINDIRECT (BSIZE / sizeof(uint)) * (BSIZE / sizeof(uint))
#define MAXFILE (NDIRECT + NINDIRECT + NDINDIRECT)


// On-disk inode structure
struct dinode {
  short type;           // File type
  short major;          // Major device number (T_DEVICE only)
  short minor;          // Minor device number (T_DEVICE only)
  short nlink;          // Number of links to inode in file system
  uint size;            // Size of file (bytes)
  uint addrs[NDIRECT+2];   // Data block addresses
};

```
2.修改文件`file.h`中的inode结构体，确保addrs[]数组与dinode中保持一致

```
// in-memory copy of an inode
struct inode {
  uint dev;           // Device number
  uint inum;          // Inode number
  int ref;            // Reference count
  struct sleeplock lock; // protects everything below here
  int valid;          // inode has been read from disk?

  short type;         // copy of disk inode
  short major;
  short minor;
  short nlink;
  uint size;
  uint addrs[NDIRECT+2];
};
```
3.修改文件`fs.c`中bmap以及itrunc函数的定义

**bmap函数**：

```
static uint bmap(struct inode *ip, uint bn)
{
  uint addr, *a, dindirect_index, indirect_index;
  struct buf *bp,*dbp;

  // dinode的addrs数组中,0 - NDIRECT-1存放的是直接块的块号
  // bn是逻辑块号,即相对于文件内部的块号
  if(bn < NDIRECT){
    // addrs数组对应项的值为0,即bn没有对应的物理块号
    if((addr = ip->addrs[bn]) == 0)
      // 调用balloc分配一个新的数据块,并将其块号存放在addr中
      // 随后将addrs[bn]的值设置为addr中存储的块号
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  // bn >= NDIRECT代表接下来要从间接块或双重间接块中找到bn对应的物理块号
  // 执行完bn -= NDIRECT后,bn就仅作为间接块或双重间接块中的索引值
  bn -= NDIRECT;

  // 执行完bn -= NDIRECT后,bn < NINDIRECT则代表bn是间接块中的索引值
  if(bn < NINDIRECT){
    // 在addrs数组中查找间接块对应的物理块号
    // addrs数组对应项的值为0,即bn没有对应的物理块号
    if((addr = ip->addrs[NDIRECT]) == 0)
      // 调用balloc分配一个新的间接块,并将其块号存放在addr中
      // 随后在addrs数组中记录间接块的物理块号
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    // 读取间接块的内容
    bp = bread(ip->dev, addr);
    // a指向buffer的数据区
    a = (uint*)bp->data;
    // 间接块数据区中存放的内容都是对应真正存放数据的数据块号
    // 从间接块中获取对应数据块的块号,为0则代表该数据块未分配使用
    if((addr = a[bn]) == 0){
      // 调用balloc分配一个新的数据块,并将其块号存放在addr中 
      a[bn] = addr = balloc(ip->dev);
      // 在日志中记录对间接块的修改
      log_write(bp);
    }
    brelse(bp);
    // 返回bn对应的数据块号
    return addr;
  }
  // 执行完bn -= NDIRECT后,bn就仅作为双重间接块中的索引值
  bn -= NINDIRECT;

  // 若bn > NDINDIRECT就代表bn的值超过了系统中一个文件能占用的最大块数。
  if(bn < NDINDIRECT){
    // 在addrs数组中查找双重间接块对应的物理块号
    // addrs数组对应项的值为0,即bn没有对应的物理块号
    if((addr = ip->addrs[NDIRECT+1]) == 0)
      // 调用balloc分配一个新的双重间接块,并将其块号存放在addr中
      // 随后在addrs数组中记录双重间接块的物理块号
      ip->addrs[NDIRECT+1] = addr = balloc(ip->dev);
    // 读取双重间接块的内容
    bp = bread(ip->dev, addr);
    // a指向buffer的数据区,双重间接块数据区中存放的内容是间接块的块号
    a = (uint*)bp->data;

    // 获得双重间接块中的索引
    dindirect_index = bn / (BSIZE / sizeof(uint));
    // 从双重间接块中获取对应间接块的块号,为0则代表该间接块未分配使用
    if((addr = a[dindirect_index]) == 0){
      // 调用balloc分配一个新的间接块,并将其块号记录在双重间接块中
      a[dindirect_index] = addr = balloc(ip->dev);
      // 在日志中记录对双重间接块的修改
      log_write(bp);
    }

    // 读取间接块的内容
    dbp = bread(ip->dev, addr);
    // a指向buffer的数据区,间接块数据区中存放的内容是数据块的块号
    a = (uint*)dbp->data;
    // 获得间接块中的索引
    indirect_index = bn % (BSIZE / sizeof(uint));
    // 从间接块中获取对应数据块的块号,为0则代表该数据块未分配使用
    if((addr = a[indirect_index]) == 0){
      // 调用balloc分配一个新的数据块,并将其块号记录在间接块中
      a[indirect_index] = addr = balloc(ip->dev);
      // 在日志中记录对间接块的修改
      log_write(dbp);
    }

    brelse(bp);
    brelse(dbp);
    // 返回bn对应的数据块号
    return addr;
  }


  // bn > NDIRECT + NINDIRECT则报错
  panic("bmap: out of range");
}


```
**itrunc函数：**

```
void itrunc(struct inode *ip)
{
  int i, j;
  struct buf *bp,*dbp;
  uint *a,*b;

  // 释放inode中的直接块
  for(i = 0; i < NDIRECT; i++){
    if(ip->addrs[i]){
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }

  // 释放inode间接块中的数据块
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
    // 释放inode间接块
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  // 释放inode双重间接块
  if(ip->addrs[NDIRECT+1])
  {
    // 读取双重间接块的数据,用bp接收
    bp = bread(ip->dev, ip->addrs[NDIRECT+1]);
    // a指向bp的数据区,双重间接块数据区存放的内容是间接块的块号
    a = (uint*)bp->data;
    
    // 遍历双重间接块
    for(i = 0; i < (BSIZE / sizeof(uint)); i++)
    { 
      // 判断当前间接块是否存在
      if(a[i])
      {
        // 读取间接块,用dbp接收
        dbp = bread(ip->dev, a[i]);
        // b指向dbp的数据区,间接块数据区存放的内容是数据块的块号
        b = (uint*)dbp->data;
        // 遍历间接块
        for(j = 0; j < (BSIZE / sizeof(uint)); j++)
        { 
          if(b[j])
            // 释放数据块
            bfree(ip->dev, b[j]);
        }
        brelse(dbp);
        // 释放间接块
        bfree(ip->dev, a[i]);
      }
      
    }

    brelse(bp);
    // 释放双重间接块
    bfree(ip->dev, ip->addrs[NDIRECT+1]);
    ip->addrs[NDIRECT+1] = 0;
  }

  // 将inode的数据大小设置为0
  ip->size = 0;
  iupdate(ip);
}
```
# 2.Symbolic links
**实现细节：**

1. 在文件`user/usys.pl, user/user.h, kernel/syscall.h, kernel/syscall.c`中增加对函数`symlink`的声明

2. 修改`kernel/fcntl.h`, 添加`O_NOFOLLOW`标志
```
#define O_RDONLY   0x000
#define O_WRONLY   0x001
#define O_RDWR     0x002
#define O_CREATE   0x200
#define O_TRUNC    0x400
#define O_NOFOLLOW 0x100
```

3. 修改kernel/stat.h, 添加类型`T_SYMLINK`
```
#define T_DIR     1   // Directory
#define T_FILE    2   // File
#define T_DEVICE  3   // Device
#define T_SYMLINK 4   // Symbolic link
```

4. 在kernel/sysfile.c文件中添加函数`sys_symlink`

```
uint64 sys_symlink(void)
{ 
  char target[MAXPATH],path[MAXPATH];
  struct inode *ip;
  struct buf *bp;
 
  // 从用户空间获得参数
  if(argstr(0, target, MAXPATH) < 0 || argstr(1, path, MAXPATH) < 0)
    return -1;

  // 开启事务
  begin_op();

  // 为路径path创建对应的inode,create返回的inode已被当前进程加锁
  if((ip = create(path, T_SYMLINK, 0, 0)) == 0 ){
    end_op();
    return -1;
  }

  // 无论target是否存在,都直接将路径target存放到路径path对应的数据块中
  if(writei(ip, 0, (uint64)target, 0, MAXPATH) != MAXPATH){
    iunlockput(ip);
    end_op();
    return -1;
  }

  iunlockput(ip);
  end_op();
  return 0;

}
```

5. 在kernel/sysfile.c文件中修改`sys_open`函数

```
uint64 sys_open(void)
{
  char path[MAXPATH],target[MAXPATH];
  int fd, omode;
  struct file *f;
  struct inode *ip;
  int n,cycle;
  ...
  ...
  / 设备文件的主设备号必须小于0并小于NDEV
  // 需要注意,运行到这里时,ip指向的inode已被锁定
  if(ip->type == T_DEVICE && (ip->major < 0 || ip->major >= NDEV)){
    iunlockput(ip);
    end_op();
    return -1;
  }
  // path的inode中type属性为T_SYMLINK
  if(ip->type == T_SYMLINK)
  { 
    // 若打开模式不是NOFOLLOW
    if(!(omode & O_NOFOLLOW))
    { 
      // 若当前ip指向的inode是属于符号链接文件且打开模式不是NOFOLLOW
      // 那么此时需要进行循环处理,循环次数超过10便返回-1
      while(ip->type == T_SYMLINK)
      { 
        // 最大循环次数为10
        if(cycle == 10){
            iunlockput(ip);
            end_op();
            return -1;
        }
        cycle ++;
        // 从ip指向的inode中复制路径到target中
        if(readi(ip, 0, (uint64)target, 0, MAXPATH) != MAXPATH)
        { 
          // 释放对path的inode的锁,并将其引用计数减1
          iunlockput(ip);
          end_op();
          return -1;
        }
        // 解锁当前ip指向的inode
        iunlockput(ip);

        // 根据目标路径获得对应的inode,ip指向该inode
        if((ip = namei(target)) == 0)
        {
          end_op();
          return -1;
        }
        // namei函数不会对返回的inode进行加锁,因此这里需要锁定新获得的inode
        ilock(ip);
      } // while
    
    } // if(!(omode & O_NOFOLLOW))
    
  } // if(ip->type == T_SYMLINK)
  ...
  ...
  return fd;
}

```
