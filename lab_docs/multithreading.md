# 1.Uthread: switching between threads
- 实验的主体框架代码已经给出，按照提示，我们需要修改`user/uthread.c`文件中的`thread_create()`和`thread_schedule()`函数以及Uthread_switch.S文件中的`thread_switch`函数来完成线程的创建以及线程之间的切换。同时，还需要往`struct thread `结构体添加一个属性，用于存放线程的寄存器信息。


**实现代码：**

**1.数据结构**

```
struct context {
  uint64 ra;
  uint64 sp;

  // callee-saved
  uint64 s0;
  uint64 s1;
  uint64 s2;
  uint64 s3;
  uint64 s4;
  uint64 s5;
  uint64 s6;
  uint64 s7;
  uint64 s8;
  uint64 s9;
  uint64 s10;
  uint64 s11;
};

struct thread{
  char       stack[STACK_SIZE]; /* the thread's stack */
  int        state;             /* FREE, RUNNING, RUNNABLE */
  struct context context;
};
```

**2.thread_create()函数**

```
void thread_create(void (*func)())
{
  struct thread *t;

  for (t = all_thread; t < all_thread + MAX_THREAD; t++) {
    if (t->state == FREE) break;
  }

  // 设置线程的状态为RUNNABLE
  t->state = RUNNABLE;
  // 设置线程运行的函数
  t->context.ra = (uint64)func;
  // 设置线程运行时使用的栈
  t->context.sp = (uint64)t->stack + STACK_SIZE;
}
```
**3.thread_schedule()函数**

```
void thread_schedule(void)
{
  ...
  ...
  if (current_thread != next_thread) 
  {         
    /* switch threads?  */
    next_thread->state = RUNNING;
    t = current_thread;
    current_thread = next_thread;
    
    //Invoke thread_switch to switch from t to next_thread
    thread_switch((uint64)&t->context,(uint64)&next_thread->context);
    
  } 
  else
    next_thread = 0;
}
```

**4.thread_switch函数**

```
.text
.globl thread_switch
thread_switch:
	sd ra, 0(a0)
    sd sp, 8(a0)
    sd s0, 16(a0)
    sd s1, 24(a0)
    sd s2, 32(a0)
    sd s3, 40(a0)
    sd s4, 48(a0)
    sd s5, 56(a0)
    sd s6, 64(a0)
    sd s7, 72(a0)
    sd s8, 80(a0)
    sd s9, 88(a0)
    sd s10, 96(a0)
    sd s11, 104(a0)

    ld ra, 0(a1)
    ld sp, 8(a1)
    ld s0, 16(a1)
    ld s1, 24(a1)
    ld s2, 32(a1)
    ld s3, 40(a1)
    ld s4, 48(a1)
    ld s5, 56(a1)
    ld s6, 64(a1)
    ld s7, 72(a1)
    ld s8, 80(a1)
    ld s9, 88(a1)
    ld s10, 96(a1)
    ld s11, 104(a1)

	/* return to ra */
	ret
```

---
# 2.Using threads
- 第二个实验是要求我们在真实的Linux或者MacOS系统上使用Pthread线程库。而由官网上的提示可知，我们需要在ph.c文件现有的代码之上加上对应锁，从而避免因竞态而导致的错误以及提高程序的并发性。

**实现代码：**

**1.数据结构**

```
#define NBUCKET 5
#define NKEYS 100000

struct entry {
  int key;
  int value;
  struct entry *next;
};

// 每个bucket对应一个锁，程序中采用的是链地址法来处理
// 因此一个bucket可能会有多个entry结构体(用链表进行链接)
pthread_mutex_t locks[NBUCKET];
struct entry *table[NBUCKET];
int keys[NKEYS];
int nthread = 1;
```

**2. put函数**

```
static void put(int key, int value)
{
  int i = key % NBUCKET;

  // is the key already present?
  struct entry *e = 0;

  pthread_mutex_lock(&locks[i]);
  for (e = table[i]; e != 0; e = e->next) 
  {
    if (e->key == key)   
      break;        
  }
  pthread_mutex_unlock(&locks[i]);

  pthread_mutex_lock(&locks[i]);
  if(e)
  { 
    // update the existing key.
    e->value = value;
  } 
  else 
  {
    // the new is new.
    insert(key, value, &table[i], table[i]);
  }
  pthread_mutex_unlock(&locks[i]);

}
```
**3. get函数**

```
static struct entry* get(int key)
{
  int i = key % NBUCKET;
  struct entry *e = 0;

  pthread_mutex_lock(&locks[i]);
  for (e = table[i]; e != 0; e = e->next) {
    if (e->key == key)    
      break;   
  }
  pthread_mutex_unlock(&locks[i]);

  return e;
}
```
**4. main函数**

```
int main(int argc, char *argv[])
{
  pthread_t *tha;
  void *value;
  double t1, t0;
    
 
  for(int i = 0; i < NBUCKET; i++)
  {
    // initialize the lock
    pthread_mutex_init(&locks[i], NULL); 
  }
  ...
  ...
}

```

---

# 3.Barrier
- 第三个实验同样也是要求我们在真实的Linux或者MacOS系统上使用Pthread线程库，并通过Pthread实现Barrier。Barrier是指：所有运行的线程必须在某一个点等待，直到所有运行线程均到达该点才能之后，该线程才能继续往下运行。

**实现代码：**

**1. barrier函数**
```
static void barrier()
{
  pthread_mutex_lock(&bstate.barrier_mutex);
  bstate.nthread ++;

  if(bstate.nthread < nthread)
    pthread_cond_wait(&bstate.barrier_cond, &bstate.barrier_mutex);
  else
  {
    bstate.nthread = 0;
    bstate.round ++;
    pthread_cond_broadcast(&bstate.barrier_cond);
  }
  
  pthread_mutex_unlock(&bstate.barrier_mutex);
}
```
**2. thread函数**

```
static void * thread(void *xa)
{
  long n = (long) xa;
  long delay;
  int i;

  for (i = 0; i < 20000; i++) {
    pthread_mutex_lock(&bstate.barrier_mutex);
    int t = bstate.round;
    pthread_mutex_unlock(&bstate.barrier_mutex);
    assert (i == t);
    barrier();
    usleep(random() % 100);
  }

  return 0;
}
```



