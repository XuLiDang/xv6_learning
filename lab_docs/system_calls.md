## 1.System call tracing
需要修改的文件包括:`user/user.h, user/usys.pl, kernel/syscall.h, kernel/syscall.c, kernel/proc.c, kernel/pror.h, kernel/sysproc.c`

1. `user/user.h`中需要添加: `int trace(int); `

2. `user/usys.pl`中需要添加: `entry("trace");`

3. `kernel/syscall.h`中需要添加: `#define SYS_trace   22`

4. `kernel/syscall.c`修改内容:

```
extern uint64 sys_chdir(void);
....
extern uint64 sys_trace(void);

// 系统调用表
static uint64 (*syscalls[])(void) = {
[SYS_fork]    sys_fork,
...
[SYS_trace]   sys_trace
};

// 系统调用名称表
static char *syscall_list[23] = {
  "none",  "fork",  "exit",   "wait",   "pipe",  "read",  "kill",   "exec",
  "fstat", "chdir", "dup",    "getpid", "sbrk",  "sleep", "uptime", "open",
  "write", "mknod", "unlink", "link",   "mkdir", "close", "trace"
};

/ 内核处理分发系统调用，并返回执行结果
void syscall(void)
{
  ...
  // 通过a7寄存器取出系统调用号
  num = p->trapframe->a7;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num])
  {
    // 根据系统调用号，找到对应的系统调用实现并执行，执行结果通过a0寄存器返回给用户程序
    p->trapframe->a0 = syscalls[num]();
    
    if(p->mask == (1 << num) || p->mask == 2147483647)  
      printf("%d: syscall %s -> %d\n",p->pid,syscall_list[num],p->trapframe->a0);
    
  } 
  ...
}

```
5. proc.c修改内容:

```
int fork(void)
{
  ...
  // 子进程继承父进程的Tracing mask
  acquire(&wait_lock);
  np->mask = p->mask;
  release(&wait_lock);

  return pid;
}
```

6. proc.h修改内容:

```
// Per-process state
// 相当于linux中的task_struct结构体
struct proc 
{
  ...
  // wait_lock must be held when using this:
  struct proc *parent;         // Parent process
  int mask;                    // Tracing mask
  ...
};
```
7.sysproc.c修改内容:

```
uint64 sys_trace(void)
{
  int mask;
  struct proc *p = myproc();

  if(argint(0, &mask) < 0)
    return -1;

  p->mask = mask;

  return 0; 
}
```
## 2.Sysinfo
需要修改的文件包括:`user/user.h, user/usys.pl, kernel/syscall.h, kernel/syscall.c, kernel/proc.c, kernel/sysproc.c, kernel/kalloc.c, kernel/defs.h`

1. `user/user.h`中需要添加: `int sysinfo(struct sysinfo *); `

2. `user/usys.pl`中需要添加: `entry("sysinfo");`

3. `kernel/syscall.h`中需要添加: `#define SYS_sysinfo 23`

4. `kernel/syscall.c`修改内容:
```
...
extern uint64 sys_info(void);

// 系统调用表
static uint64 (*syscalls[])(void) = {
...
[SYS_sysinfo] sys_info
};
```
5.sysproc.c修改内容:

```
....
#include "sysinfo.h" // struct sysinfo
....
uint64 sys_info(void)
{
  uint64 u_addr;
  struct proc *p = myproc();
  struct sysinfo si;

  if(argaddr(0, &u_addr) < 0)
    return -1;

  si.freemem = collect_freemem_nr();
  si.nproc = collect_proc_nr();
  
  if (copyout(p->pagetable, u_addr, (char *)&si, sizeof(si)) < 0)
    return -1;

  return 0;

}
```
6.proc修改内容:

```
...
#include "sysinfo.h" // struct sysinfo
...
uint64 collect_proc_nr()
{
  uint64 nproc = 0;
  for(int i = 0; i < NPROC; i++)
    if(proc[i].state != UNUSED)
      nproc++;
  
  return nproc;
}
```
7.kalloc.c修改内容:

```
...
#include "sysinfo.h" // struct sysinfo
...
uint64 collect_freemem_nr()
{
  uint64 freemem = 0;
  struct run *r;
  r = kmem.freelist;
  
  while(r)
  {
    freemem += 4096;
    r = r->next;
  }
    
  return freemem;
}
```
8.defs.h修改内容:

```
...
struct sysinfo;
...
// kalloc.c
void*           kalloc(void);
...
uint64            collect_freemem_nr(void);
...
// proc.c
int             cpuid(void);
...
uint64 			collect_proc_nr(void);