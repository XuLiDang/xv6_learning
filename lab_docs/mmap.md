# Lab: mmap
- 这一个实验是要实现最基础的mmap功能。mmap即内存映射文件，将一个文件直接映射到内存当中，之后对文件的读写就可以直接通过对内存进行读写来进行，而对文件的同步则由操作系统来负责完成。使用mmap可以避免对文件大量read和write操作带来的内核缓冲区和用户缓冲区之间的频繁的数据拷贝。官网已给出了大部分的实现细节，那么接下来看看具体如何实现

**1. 相关数据结构**
- 首先在`proc.h`文件中定义vma结构体，该结构体主要用于保存内存映射信息；随后我们还需要在proc结构体中定义一个vma数组(亦可采用链表的方式动态分配vma结构体)：

```
#define MAXVMA 16
struct vma
{
  int used;
  struct file *f;
  uint64 addr;
  uint64 length;
  int prot;
  int flags;
  uint64 offset;
}

struct proc 
{
  ......
  struct vma vmas[MAXVMA];
};

```

**2. sys_mmap函数**
- 正如官网所说，该函数并不会直接分配物理内存以及从文件中读取数据，相反，这些工作将会交给中断处理函数。
```
uint64 sys_mmap(void)
{
  // 按照官网的提示，我们可以假设用户传入的addr以及offset总会是0
  int length, prot, flags, fd;
  struct file *f;
  struct proc *p = myproc();

  // 获取用户进程传递的参数
  if(argint(1, &length) < 0 ||  argint(2, &prot) < 0 || argint(3, &flags) < 0  || argint(4, &fd) < 0 )
    return -1;

  // 通过用户传入的文件描述符获取对应的file结构体
  f = p->ofile[fd];
  // 检查文件权限与用户传入的权限是否一致
  if(!f->writable && prot & PROT_WRITE && !(flags & MAP_PRIVATE))
    return -1;
  if(!f->readable && prot & PROT_READ)
    return -1;

  // 找到第一个未使用的vma结构体，并使用该结构体存放相应的信息
  for(int i = 0; i < MAXVMA; i++)
  {
    if(!p->vmas[i].used)
    {
      p->vmas[i].used = 1;
      p->vmas[i].f = f;
      // 增加文件结构体的引用计数
      filedup(f);
      // 内存映射的起始地址
      p->vmas[i].addr = p->sz;
      // 内存映射的字节数，最小的单位为PGSIZE
      p->vmas[i].length = PGROUNDUP(length);
      // 增加当前进程的堆顶地址
      p->sz += p->vmas[i].length;
      // 权限
      p->vmas[i].prot = prot;
      // 标志
      p->vmas[i].flags = flags;
      // 偏移量
      p->vmas[i].offset = 0;
      // 返回内存映射的起始地址
      return p->vmas[i].addr;
    }
  }
  // 未找到对应的vma结构体
  return -1;
}    
```

**3. usertrap函数**
- 当页面故障发生在内存映射区域中的某个地址时，该函数的主要处理流程：分配一页物理内存 ---> 将相关文件的4096字节数据读入该物理页中 ---> 创建出错地址与物理页的映射关系。

```
void usertrap(void)
{
  ...
  else if(r_scause() == 15 || r_scause() == 13)
  { 
    uint64 fault_addr = r_stval();
    // 检查地址的合法性
    if(fault_addr >= MAXVA || fault_addr < 0 || fault_addr >= p->sz || (fault_addr <= PGROUNDDOWN(p->trapframe->sp) && fault_addr >= PGROUNDDOWN(p->trapframe->sp) - PGSIZE))
    { 
      printf("usertrap(): wrong address!\n");
      p->killed = 1;
      goto end;
    }

    // 查找出错的地址是否位于当前进程任一vma储存的地址区域中
    for(int i = 0; i < MAXVMA; i++)
    { 
      struct vma *v = &p->vmas[i];

      if(!v->used || fault_addr < v->addr || fault_addr >= v->addr + v->length)
        continue;

      // 获取用户调用mmap时传入的权限参数，并转化为对应页面的权限
      // flag记得要将用户访问位置为1
      int flag = PTE_U;
      if(v->prot & PROT_READ)
        flag |= PTE_R;
      if(v->prot & PROT_WRITE)
        flag |= PTE_W;
      if(v->prot & PROT_EXEC)
        flag |= PTE_X;

      // 申请分配内存空间
      char *mem = kalloc();
      if(mem == 0)
      {
        printf("usertrap(): there is no free memory!\n");
        p->killed = 1;
        goto end;
      }
      memset(mem, 0, PGSIZE);

      // 锁定inode
      ilock(v->f->ip);
      // 出错地址所属页面的基地址
      uint64 va = PGROUNDDOWN(fault_addr);
      // 在offset为0的前提下，v->addr既是内存映射中的起始位置，也对应于文件的起始位置
      // 因此，"va - v->addr" 既是内存映射中的偏移量，也是对应映射文件中的偏移量
      uint64 offset = va - v->addr + v->offset;
      // 将从offset位置开始的4096个字节的文件数据拷贝到新分配的内存空间中
      readi(v->f->ip, 0, (uint64)mem, offset, PGSIZE);  
      // 释放对inode的锁定
      iunlock(v->f->ip);

      // 在进程页表中将虚拟地址va映射到新页面, 并赋予对应的权限
      if(mappages(p->pagetable, va, PGSIZE, (uint64)mem, flag) != 0)
      { 
        printf("usertrap(): mappages errors!\n");
        kfree(mem);
        p->killed = 1;
        goto end;
      }              
      // 处理成功，直接跳到end处进行处理，注意此时p->killed = 0
      goto end;
    }
    // 出错的地址没有位于当前进程任一vma储存的地址区域中
    p->killed = 1;

  } // r_scause() == 15 || r_scause() == 13
  ...

end:
  // p->killed则终止当前进程
  if(p->killed)
    exit(-1);

  // give up the CPU if this is a timer interrupt.
  if(which_dev == 2)
    yield();

  // 调用usertrapret()函数
  usertrapret();
}
```

**4. munmap函数**
- 正如官网所说，该函数的作用是：根据用户传入的地址，找到对应的VMA，然后解除对应页面的映射关系。如果munmap解除了先前mmap所有页面的映射关系，则还需要减少相应结构文件的引用计数。 如果当前被解除映射的页面已被修改并且文件被映射为`MAP_SHARED`(个人觉得还需加入一个条件：用户权限为`PROT_WIRTE`)，则将该页面写回到文件中。
- 官网简化了实现该函数的要求，即不需要判断被解除映射的页面是否已被修改，只要文件被映射为`MAP_SHARED`，则将该页面写回到文件中。


```
uint64 sys_munmap(void)
{
  uint64 addr;
  int length;
  int npages, unmap_addr, offset, close;

  // 获取用户进程传递的参数
  if(argaddr(0, &addr) < 0 ||  argint(1, &length) < 0 )
    return -1;

  // 通过解除映射地址查找对应的VMA
  for(int i = 0; i < MAXVMA; i++)
  { 
    struct vma *v = &p->vmas[i];
    // 解除映射地址不属于当前VMA则继续往下查找
    if(!v->used || addr < v->addr || addr > v->addr + v->length )
      continue;
    // close = 1时代表要减少对应file结构体的引用计数
    close = 0;
    // 从映射区域头部开始解除映射
    if(addr == v->addr)
    { 
      length = PGROUNDUP(length);
      // unmap a portion of an mmap-ed region
      if(length < v->length)
      { 
        // 解除映射的页面数
        npages = length / PGSIZE;
        // 解除映射的起始地址
        unmap_addr = addr;
        // 被映射文件区域的偏移量
        offset = v->offset;
        // 更新映射区域的大小
        v->addr += length;
        v->length -= length;
        // 更新被映射文件区域的偏移量
        v->offset += length;
      }
      // unmap the whole region
      else
      {
        // in case length > v->length
        length = v->length;
        // 解除映射的页面数
        npages = length / PGSIZE;
        // 解除映射的起始地址
        unmap_addr = addr;
        // 被映射文件区域的偏移量
        offset = v->offset;     
        // 解除整个映射空间的映射时，需要减少对应file结构体的引用计数
        // 同时，还需要将当前的vma标记为未使用
        close = 1;
        v->used = 0;
      }
    }
    // 从映射区域的尾部开始解除映射, addr = v->addr + v->length
    else
    {
      length = PGROUNDUP(length);
      // unmap a portion of an mmap-ed region
      if(length < v->length)
      {
        // 解除映射的页面数
        npages = length/PGSIZE;
        // 解除映射的起始地址
        unmap_addr = addr - length;
        // 被映射文件区域的偏移量
        offset = v->offset + v->length - length;
        // 修改映射区域的大小
        v->length -= length;
      }
      // unmap the whole region
      else
      {
        // in case length > v->length
        length = v->length;
        // 解除映射的页面数
        npages = length/PGSIZE;
        // 解除映射的起始地址
        unmap_addr = v->addr;
        // 被映射文件区域的偏移量
        offset = v->offset;
        close = 1;
        v->used = 0;
      }
    }

    // 将数据写回到文件中
    if(v->flags & MAP_SHARED && v->prot & PROT_WRITE)
    {
      if(write_back(v->f, unmap_addr, npages*PGSIZE, offset) == -1)
        return -1;
    }

    // 解除映射
    uvmunmap(p->pagetable, unmap_addr, npages, 1);
    // close = 1 则减少对应file结构体的引用计数
    if(close == 1)
      fileclose(v->f);

    return 0;
  }

  return -1;
}    
```

执行写回操作时要到的write_back函数定义如下：

```
// 写回函数
int write_back(struct file *f, uint64 addr,int n ,uint off)
{
  int r = 0;
  if(f->writable == 0) return -1;

  int max= ((MAXOPBLOCKS-1-1-2) / 2)* BSIZE;
  int i = 0;

  while(i < n)
  {
    int n1 = n - i;
    if(n1 > max) n1 = max;

    begin_op();
    ilock(f->ip);
    // 因为mmap系统调用默认从文件的开始处进行映射
    // 因此, 被映射文件区域的偏移量便等于文件的偏移量
    if ((r = writei(f->ip, 1, addr + i, off, n1)) > 0)
      off += r;
    iunlock(f->ip);
    end_op();

    if(r != n1) break;

    i += r;
  }
  
  return 0;

}
```
**5. 修改exit以及fork函数**

```
//proc.c
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait().
void exit(int status)
{
  struct proc *p = myproc();

  if(p == initproc)
    panic("init exiting");

  for(int i = 0; i < MAXVMA; i++)
  {
    struct vma *v = &p->vmas[i];
    if(!v->used)
      continue;

    v->used = 0;
    if(v->flags & MAP_SHARED)
      if(write_back(v->f, v->addr, v->length, v->offset) == -1)
        printf("write_back returns -1\n");
               
    uvmunmap(p->pagetable, v->addr, v->length/PGSIZE, 1);
    fileclose(v->f); 
  }
  ...
}

// Create a new process, copying the parent.
// Sets up child kernel stack to return as if from fork() system call.
int fork(void)
{
  int i, pid;
  struct proc *np;
  struct proc *p = myproc();
  ...

  for(int i = 0; i < MAXVMA; i++)
  {
    struct vma *v = &p->vmas[i];
    struct vma *nv = &np->vmas[i];
    if(v->used)
    { 
      memmove(nv,v,sizeof(struct vma));
      filedup(nv->f);
    }
  }

  return pid;
}
```
**6. 修改uvmunmap以及uvmcopy函数**

```
void uvmunmap(pagetable_t pagetable, uint64 va, uint64 npages, int do_free)
{
  uint64 a;
  pte_t *pte;

  if((va % PGSIZE) != 0)
    panic("uvmunmap: not aligned");

  for(a = va; a < va + npages*PGSIZE; a += PGSIZE){
    if((pte = walk(pagetable, a, 0)) == 0)
      //panic("uvmunmap: walk");
      contiune; // 忽略未分配物理内存的虚拟地址
      
    if((*pte & PTE_V) == 0)
      //panic("uvmunmap: not mapped");
      contiune; // 忽略未分配物理内存的虚拟地址
    ...
}


int uvmcopy(pagetable_t old, pagetable_t new, uint64 sz)
{
  pte_t *pte;
  uint64 pa, i;
  uint flags;
  char *mem;

  for(i = 0; i < sz; i += PGSIZE){
    // 寻找父进程虚拟地址i对应的PTE, 并用指针变量pte存放PTE的地址
    if((pte = walk(old, i, 0)) == 0)
      //panic("uvmcopy: pte should exist");
      continue; // 忽略父进程中未分配物理内存的虚拟地址
      
    // PTE不存在则报错
    if((*pte & PTE_V) == 0)
      //panic("uvmcopy: page not present");
      continue; // 忽略父进程中未分配物理内存的虚拟地址
    ...
}
```
**7. 注意事项**
- 在使用实验中提到的结构体或者函数时，需要先包含对应的头文件。例如，需要在`trap.c`文件中包含`fcntl.h`以及`file.h`等头文件。
